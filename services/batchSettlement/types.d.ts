export interface BatchSettlementListType {
  id: number;
  file_path: string;
  file_title: string;
  status: boolean;
  insert_date: string;
  description: string;
  total_amount: number;
  total_count: number;
}
