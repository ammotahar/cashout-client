import Api from "services";

export const getBatchSettlementListApi = (query?: {
  per_page?: number;
  page_number?: number;
  file_title?: string;
}) => Api.GET({ url: "batch_settlement", query });

export const downloadExcelBulkSettlementApi = (data: { file_id: number }) =>
  Api.POST({ url: "settlement/download_excel_bulk_settlement", data });

export const downloadSampleExcelBulkSettlementApi = () =>
  Api.GET({ url: "settlement/download_sample_excel_bulk_settlement" });

export const deleteBatchSettlementApi = (id: number) =>
  Api.DELETE({ url: `batch_settlement/${id}` });
