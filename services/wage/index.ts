import Api from "services";

export const getIbanInquiryWageApi = () =>
  Api.GET({ url: "company_wage/get_iban_inquiry_wage" });
