import { toast } from "react-toastify";

export const ServerErrorHandling = (
  err: any,
  hasToast = true,
  status: number = 0,
  type: string = "noServerData"
) => {
  if (type === "withServerData") {
    switch (status) {
      case 403:
        toast.error(err?.message);
        // window.localStorage.clear();
        break;
      case 401:
        toast.error(err?.message);
        window.localStorage.clear();
        window.location.href = "/authentication/login";
        throw err?.message;

        break;
      default:
        if (hasToast) toast.error(err?.message);
        console.log("error::::::::::>", err?.message);
        return;
    }
  } else {
    if (err.response) {
      switch (err.response.status) {
        case 403:
          // window.localStorage.clear();
          break;
        case 401:
          window.localStorage.clear();

          break;
        default:
          if (hasToast) toast.error(err.response.data?.message);
          console.log("error::::::::::>", err.response.data?.message);
          return;
      }
    } else {
      toast.dismiss();
      toast.error(
        "مشکل در اتصال به اینترنت در صورت استفاده از فیلتر شکن آن را خاموش کنید"
      );
      // toast.error("تتتتتت")
      return;
    }
  }
};
