export interface ChargeBalanceReportListType {
  id: number;
  amount: number;
  insert_date: string;
  bank_title: null | string;
  status: number;
}

export interface ChargeBalanceReportDetailsType {
  id: number;
  bank_title: null | string;
  confirmer_name: null | string;
  user_name: string;
  type_title: string;
  status_title: string;
  type: number;
  amount: number;
  confirmer_user_id: null | number;
  description: string | null;
  from_iban: string;
  to_iban: string;
  reject_result: string;
  insert_date: string;
  confirm_date: null | string;
  track_number: number;
  status: number;
  user: number;
  bank: null | number;
}
