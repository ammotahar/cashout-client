import Api from "services";

export const increaseBalanceViaReceiptApi = (data: FormData) =>
  Api.POST({
    url: "charge_balance/increase_balance_via_receipt",
    data,
    isFormData: true,
  });

export const increaseBalanceViaIpgApi = (data: {
  amount: number;
  description?: string;
  call_back_url: string;
}) =>
  Api.POST({
    url: "charge_balance/increase_balance_via_ipg",
    data,
  });

export const getChargeBalanceReportListApi = (query?: {
  amount?: number;
  bank_name?: string;
  from_date_insert_date?: string;
  to_date_insert_date?: string;
  from_iban?: string;
  to_iban?: string;
  ordering?: string;
  page_number?: number;
  per_page?: number;
  search?: string;
  status?: number;
  track_number?: number;
  type?: number;
}) => Api.GET({ url: "charge_balance", query });

export const getChargeBalanceReportDetailsApi = (id: number) =>
  Api.GET({ url: `charge_balance/${id}` });

export const getChargeBalanceListXlsxApi = () =>
  Api.GET({
    url: "charge_balance/get_charge_balance_list_xlsx",
    isBinary: true,
  });
