export interface SettlementFileRecordType {
  first_name: string;
  last_name: string;
  amount: number;
  iban: string;
}

export interface ErrorRecordsType {
  first_name: string;
  last_name: string;
  amount: number;
  iban: string;
  error_fields: "iban" | "first_name" | "last_name" | "amount"[];
}

export interface SettlementDetailsType {
  id: number;
  bank_name: string;
  iban: string;
  transaction_type: null | sring;
  track_number: number;
  create_date: string;
  settlement_date: null | string;
  settlement_delay_time: null | number;
  amount: number;
  first_name: string;
  last_name: string;
  wage_amount: null | string;
  bank_wage_amount: null | string;
  description: string;
  response_code: null | number;
  response: string;
  bank_track_id: string;
  settlement_from: 1 | 2;
  settlement_type: 0 | 1;
  settlement_status: 0 | 1 | 2 | 3 | 4;
  wage_status: boolean;
  track_bank_guid: string;
  bank: number;
  account: number;
}

export interface IbanListType {
  first_name: string;
  last_name: string;
  iban: string;
}

export interface BulkSettlementInfoType {
  total_count: number;
  total_amount: number;
  total_wage: number;
  total_bank_wage: number;
}
