import Api from "services";

export const uploadExcelBulkSettlementApi = (data: any) =>
  Api.POST({
    url: "settlement/upload_excel_bulk_settlement",
    data,
    isFormData: true,
  });

export const getSettlementFileRecordsApi = (
  data: { file_id: number },
  query?: { page_number?: number; per_page?: number }
) =>
  Api.POST({
    url: "settlement/get_settlement_file_records",
    data,
    query,
  });

export const doBulkSettlementApi = (data: {
  file_id: number;
  settlement_delay_time?: number | null;
  settlement_from: number;
  account?: number;
}) =>
  Api.POST({
    url: "settlement/do_bulk_settlement",
    data,
  });

export const addBulkSettlementToListApi = (data: {
  file_id: number;
  settlement_delay_time?: number | null;
  settlement_from: number;
  account?: number;
}) =>
  Api.POST({
    url: "settlement/add_bulk_settlement_to_list",
    data,
  });

export const addSingleSettlementToListApi = (data: {
  iban: string;
  amount: number;
  first_name: string;
  last_name: string;
  bank?: number;
  account?: number;
  // account_identity?: number;
  settlement_delay_time?: number | null;
  settlement_from: number | string | null;
  description?: string;
}) => Api.POST({ url: "settlement/add_single_settlement_to_list", data });

export const getSettlementDetailsApi = (id: number) =>
  Api.GET({ url: `settlement/${id}` });

export const doSettlementByIdApi = (data: { settlement_id: number }) =>
  Api.POST({
    url: "settlement/do_settlement_by_id",
    data,
  });

export const updateSingleSettlementByIdApi = (data: {
  settlement_id: number;
  iban: string;
  amount: number;
  first_name: string;
  last_name: string;
  bank?: number;
  account?: number;
  settlement_delay_time?: number | null;
  settlement_from: number | string | null;
  description?: string;
}) => Api.POST({ url: "settlement/update_single_settlement_by_id", data });

export const deleteSettlementApi = (id: number) =>
  Api.DELETE({ url: `settlement/${id}` });

export const getIbanListApi = (query?: {
  page_number?: number;
  per_page?: number;
  search?: string;
}) => Api.GET({ url: "settlement/get_iban_list", query });

export const getBulkSettlementInfoApi = (data: {
  file_id: number;
  settlement_from: 1 | 2;
}) => Api.POST({ url: "settlement/get_bulk_settlement_info", data });
