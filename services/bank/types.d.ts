export interface BankListType {
  id: number;
  name: string;
  is_active: boolean;
}
