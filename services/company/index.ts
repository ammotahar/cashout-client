import Api from "services";

export const getBalanceAndMinDepositApi = () =>
  Api.GET({ url: "company/get_balance_and_min_deposit" });
