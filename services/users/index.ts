import Api from "services";

export const getProfileApi = () => Api.GET({ url: "users/get_profile" });

export const editProfileApi = (
  id: number,
  data: {
    company?: number;
    status?: boolean;
    first_name?: string;
    last_name?: string;
    mobile?: string;
    phone?: string;
    birthdate?: string;
    national_code?: string;
    user_name?: string;
    password?: string;
    bank?: number;
    job_position?: string;
    max_amount?: string;
    max_amount_satna?: string;
    max_amount_paya?: string;
  }
) => Api.PATCH({ url: `users/${id}`, data });

export const createUserApi = (data: {
  company?: number;
  status?: boolean;
  first_name: string;
  last_name: string;
  mobile: string;
  phone?: string;
  birthdate?: string;
  national_code: string;
  user_name: string;
  password: string;
  bank?: number;
  job_position?: string;
  max_amount?: string;
  max_amount_satna?: string;
  max_amount_paya?: string;
  role?: number;
  permission_code_list?: string[];
}) => Api.POST({ url: "users", data });

export const getUsersListApi = (query?: {
  company_id?: number;
  first_name?: string;
  last_name?: string;
  mobile?: string;
  national_code?: string;
  page_number?: number;
  per_page?: number;
  permission_code?: string;
  phone?: string;
  role_id?: number;
  search?: string;
  status?: boolean;
  user_name?: string;
}) => Api.GET({ url: "users", query });

export const getUsersListExcelApi = (query?: {
  company_id?: number;
  first_name?: string;
  last_name?: string;
  mobile?: string;
  national_code?: string;
  permission_code?: string;
  phone?: string;
  role_id?: number;
  search?: string;
  status?: boolean;
  user_name?: string;
}) => Api.GET({ url: "users/get_user_list_xlsx", query, isBinary: true });

export const getUserDetailsApi = (id: number) =>
  Api.GET({ url: `users/${id}` });

export const getPermissionRoleUserApi = () =>
  Api.GET({ url: "users/get_permission_role_user" });
