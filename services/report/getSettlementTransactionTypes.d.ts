export interface GetSettlementTransactionDataItemType {
  id: number;
  company_name: string;
  settlement_status_title: string;
  iban: string;
  transaction_type: number;
  track_number: number;
  create_date: string;
  settlement_date: string;
  settlement_delay_time: null | string;
  response: string;
  amount: number;
  first_name: string;
  last_name: string;
  wage_amount: string;
  description: string;
  response_code: null | number | string;
  bank_track_id: string;
  company_id: number;
  settlement_from: number;
  settlement_status: number;
  wage_status: boolean;
  track_bank_guid: string;
  user: number;
  cut_batch_settlement: null | number;
  company_wage: number;
  bank: number;
  account: null | number;
}

export interface GetSettlementTransactionType {
  count: number;
  page_count: number;
  page_number: number;
  successful_transactions_count: number;
  failed_transactions_count: number;
  pending_transactions_count: number;
  checking_queue_transactions_count: number;
  pending_user_transactions_count: number;
  successful_transactions_percent: number;
  failed_transactions_percent: number;
  pending_transactions_percent: number;
  checking_queue_transactions_percent: number;
  pending_user_transactions_percent: number;
  data: GetSettlementTransactionDataItemType[];
}
