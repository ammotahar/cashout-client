import Api from "services";

export const getAccountsListApi = (query?: {
  is_active?: boolean;
  is_deleted?: boolean;
}) => Api.GET({ url: "account", query });

export const getAccountBalanceApi = (data: { account_id: number }) =>
  Api.POST({ url: "account/get_account_balance", data });

export const getIbanInfoApi = (data: { iban: string }) =>
  Api.POST({ url: "account/get_iban_info", data });

export const createAccountApi = (data: {
  description?: string;
  user_name: string;
  password: string;
  account_name?: string;
  full_name: string;
  is_active?: boolean;
  account_number: string;
  iban: string;
  customer_code?: string;
  view_dashboard?: boolean;
  account_number_inq?: string;
  first_name_inq?: string;
  account_status?: string;
  bank_inq?: string;
  channel_name: string;
  secret_key: string;
  bank: number;
}) => Api.POST({ url: "account", data });

export const getAccountDetailsApi = (id: number) =>
  Api.GET({ url: `account/${id}` });

export const editAccountApi = (
  data: {
    description?: string;
    user_name: string;
    password: string;
    account_name?: string;
    full_name: string;
    is_active?: boolean;
    account_number: string;
    iban: string;
    customer_code?: string;
    view_dashboard?: boolean;
    account_number_inq?: string;
    first_name_inq?: string;
    account_status?: string;
    bank_inq?: string;
    channel_name: string;
    secret_key: string;
    bank: number;
  },
  id: number
) => Api.PATCH({ url: `account/${id}`, data });

export const deleteAccountApi = (query: { id: number }, ott_token: string) =>
  Api.DELETE({ url: "account/delete_account", query, ott_token });

export const deactiveAccountApi = (
  data: { account_id: number },
  ott_token: string
) => Api.POST({ url: "account/inactive_account", data, ott_token });

export const getAccountListApi = (query?: {
  account_name?: string;
  account_number?: string;
  bank_name?: string;
  iban?: string;
  page_number?: number;
  per_page?: number;
  search?: string;
  is_deleted?: boolean;
}) => Api.GET({ url: "account", query });

export const getAccountListExcelApi = (query?: {
  account_name?: string;
  account_number?: string;
  bank_name?: string;
  iban?: string;
  search?: string;
}) => Api.GET({ url: "account/get_account_list_xlsx", query, isBinary: true });
