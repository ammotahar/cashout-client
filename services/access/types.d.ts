export interface RolesListType {
  id: number;
  title: string;
  is_active: boolean;
  role_code: string;
}

export interface PermissionsListType {
  code: string;
  english_title: string;
  persian_title: string;
}
