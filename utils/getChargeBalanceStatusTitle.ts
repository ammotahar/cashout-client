import {
  NOT_CONFIRMED,
  NOT_CONFIRMED_TITLE,
  CONFIRMED,
  CONFIRMED_TITLE,
  AWAITING_CONFIRMATION,
  AWAITING_CONFIRMATION_TITLE,
  SENDING_TO_IPG,
  SENDING_TO_IPG_TITLE,
  TRANSFER_TO_IPG_FAILED,
  TRANSFER_TO_IPG_FAILED_TITLE,
} from "@/enums/chargeBalanceStatusEnums";

export function getChargeBalanceStatusTitle(status: number) {
  switch (status) {
    case NOT_CONFIRMED:
      return NOT_CONFIRMED_TITLE;

    case CONFIRMED:
      return CONFIRMED_TITLE;

    case AWAITING_CONFIRMATION:
      return AWAITING_CONFIRMATION_TITLE;

    case SENDING_TO_IPG:
      return SENDING_TO_IPG_TITLE;

    case TRANSFER_TO_IPG_FAILED:
      return TRANSFER_TO_IPG_FAILED_TITLE;

    default:
      return "نا مشخص";
  }
}
