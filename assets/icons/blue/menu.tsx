export default function MenuIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
    >
      <path
        d="M21.75 5C21.75 4.586 21.414 4.25 21 4.25H8C7.586 4.25 7.25 4.586 7.25 5C7.25 5.414 7.586 5.75 8 5.75H21C21.414 5.75 21.75 5.414 21.75 5ZM3 11.25H21C21.414 11.25 21.75 11.586 21.75 12C21.75 12.414 21.414 12.75 21 12.75H3C2.586 12.75 2.25 12.414 2.25 12C2.25 11.586 2.586 11.25 3 11.25ZM12 18.25H21C21.414 18.25 21.75 18.586 21.75 19C21.75 19.414 21.414 19.75 21 19.75H12C11.586 19.75 11.25 19.414 11.25 19C11.25 18.586 11.586 18.25 12 18.25Z"
        fill="#4640E1"
      />
    </svg>
  );
}
