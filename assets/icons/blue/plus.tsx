export default function PlusIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="11"
      viewBox="0 0 12 11"
      fill="none"
    >
      <path
        d="M5.83203 1L5.83203 9.58042"
        stroke="#4640E1"
        strokeWidth="1.90676"
        strokeLinecap="round"
      />
      <path
        d="M1.54199 5.29004L10.1224 5.29004"
        stroke="#4640E1"
        strokeWidth="1.90676"
        strokeLinecap="round"
      />
    </svg>
  );
}
