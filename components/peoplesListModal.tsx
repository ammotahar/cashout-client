"use client";

import { MouseEventHandler, useEffect, useState } from "react";
import Modal from "@mui/material/Modal";
import SearchField from "@/components/searchField";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import Button from "./button";
import MuiRadio from "@mui/material/Radio";
import { getIbanListApi } from "@/services/settlement";
import { IbanListType } from "@/services/settlement/types";

export default function PeoplesListModal({
  open,
  onClose,
  onSelectClick,
  onCancelClick,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onSelectClick: (data: IbanListType) => void;
  onCancelClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  const [data, setData] = useState<IbanListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState<IbanListType>({
    first_name: "",
    last_name: "",
    iban: "",
  });
  const [search, setSearch] = useState("");

  async function getIbanList(searchText?: string, page?: number) {
    setLoading(true);

    const res = await getIbanListApi({
      per_page: 10,
      page_number: page,
      search: searchText,
    });

    if (res) {
      setCount(res?.data?.count);
      setData(res?.data?.data);
    }

    setLoading(false);
  }

  function pageHandler(event: any, page: number) {
    setPage(page);
    getIbanList(search, page);
  }

  useEffect(() => {
    getIbanList();
  }, []);

  useEffect(() => {
    setPage(1);
    getIbanList(search, 1);
  }, [search]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="text-center mb-2">
          <span>لیست افراد</span>
        </div>

        <hr className="w-full border-primary mb-4" />

        <div className="w-full max-w-sm self-start">
          <SearchField
            placeholder="بر اساس نام و نام خانوادگی، شماره شبا"
            onChange={(value) => setSearch(value)}
          />
        </div>

        <div className="mt-4 w-full overflow-auto">
          <Table
            heads={["انتخاب", "نام و نام خانوادگی", "شماره شبا"]}
            data={data?.map((item) => ({
              select: (
                <MuiRadio
                  checked={selected?.iban === item.iban}
                  onChange={() => setSelected(item)}
                />
              ),
              name: `${item.first_name} ${item.last_name}`,
              iban: item.iban,
            }))}
            loading={loading}
          />
        </div>

        <div className="mt-4 flex justify-end">
          <Pagination
            page={page}
            count={Math.ceil(count / 10)}
            onChange={pageHandler}
          />
        </div>

        <div className="grid grid-cols-2 gap-4 mt-4 self-end">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={() => onSelectClick(selected)}
          >
            <span>انتخاب</span>
          </Button>
          <Button
            fullRounded={false}
            variant="cancel"
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={onCancelClick}
          >
            <span>انصراف</span>
          </Button>
        </div>
      </div>
    </Modal>
  );
}
