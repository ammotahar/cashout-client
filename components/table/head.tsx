import { TableHead, TableRow, TableCell } from "@mui/material";

export default function Head({ titles }: { titles: string[] }) {
  return (
    <TableHead className="">
      <TableRow className="shadow-tableHeader">
        {titles.map((title) => (
          <TableCell
            key={title}
            align="center"
            className="!text-black !text-xs !bg-primary"
          >
            {title}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}
