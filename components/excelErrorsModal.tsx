"use client";

import { MouseEventHandler, useState } from "react";
import Modal from "@mui/material/Modal";
import Table from "@/components/table";
import Button from "./button";
import { rialSeparator } from "@/utils/rialSeperator";
import { deleteBatchSettlementApi } from "@/services/batchSettlement";
import { ErrorRecordsType } from "@/services/settlement/types";

export default function ExcelErrorsModal({
  open,
  errorRecords,
  fileId,
  onSubmitClick,
  onClose,
  onDeleteFinished,
}: {
  open: boolean;
  errorRecords: ErrorRecordsType[];
  fileId: number;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onDeleteFinished?: () => void;
}) {
  const [deleteLoading, setDeleteLoading] = useState(false);

  async function deleteBatchSettlement() {
    setDeleteLoading(true);

    const res = await deleteBatchSettlementApi(fileId);

    setDeleteLoading(false);

    onDeleteFinished && onDeleteFinished();
  }
  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="mt-4 w-full overflow-auto">
          <Table
            heads={["نام و نام خانوادگی", "شماره شبا", "مبلغ"]}
            data={errorRecords.map((item) => {
              return {
                name: `${item.first_name} ${item.last_name}`,
                iban: item.iban,
                amount: rialSeparator(item.amount.toString()),
                hasError: true,
              };
            })}
          />
        </div>

        <div className="grid grid-cols-2 gap-4 mt-4 self-end">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            padding="!p-0"
            onClick={onSubmitClick}
          >
            <span>مرحله بعد</span>
          </Button>
          <Button
            fullRounded={false}
            variant="cancel"
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={deleteBatchSettlement}
            loading={deleteLoading}
          >
            <span>حذف فایل و انصراف</span>
          </Button>
        </div>
      </div>
    </Modal>
  );
}
