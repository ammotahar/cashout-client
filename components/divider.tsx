export default function Divider({ className }: { className?: string }) {
  return <hr className={`${className} border-divider border`} />;
}
