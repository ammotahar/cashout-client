import React from "react";
import { Drawer } from "@mui/material";
import { CheckCircleOutline, HighlightOff } from "@mui/icons-material";
import Button from "./button";

export default function FilterContainer({
  open,
  onClose,
  children,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  children: React.ReactNode;
  onSubmitClick?: React.MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: React.MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={onClose}
        PaperProps={{ className: "!rounded-t-xl" }}
        ModalProps={{ className: "lg:!hidden" }}
      >
        <div className="rounded-t-xl p-4">
          <div className="text-center text-black mb-4">
            <span>فیلتر</span>
          </div>

          <hr className="border-table mb-6" />

          {children}

          <div className="grid grid-cols-2 gap-4 mt-8">
            <Button
              fullRounded={false}
              className="!col-span-1 !flex !items-center !justify-center gap-4"
              padding="!px-2"
              onClick={onSubmitClick}
            >
              <span className="text-xs">اعمال فیلتر</span>
              <CheckCircleOutline />
            </Button>

            <Button
              variant="cancel"
              className="!col-span-1 !flex !items-center !justify-center gap-4"
              onClick={onDeleteClick}
            >
              <span className="text-xs">حذف فیلتر</span>
              <HighlightOff />
            </Button>
          </div>
        </div>
      </Drawer>

      {open && (
        <div className="hidden lg:block bg-filter p-4 rounded-md">

          {children}

          <div className="flex items-center justify-end gap-4 mt-6">
            <Button
              fullRounded={false}
              className="!col-span-1 !flex !items-center !justify-center gap-4"
              padding="!px-2"
              onClick={onSubmitClick}
            >
              <span className="text-xs">اعمال فیلتر</span>
              <CheckCircleOutline />
            </Button>

            <Button
              variant="cancel"
              className="!col-span-1 !flex !items-center !justify-center gap-4"
              onClick={onDeleteClick}
            >
              <span className="text-xs">حذف فیلتر</span>
              <HighlightOff />
            </Button>
          </div>
        </div>
      )}
    </>
  );
}
