"use client";

import { MouseEventHandler, useEffect, useState, useCallback } from "react";
import { useRouter } from "next/navigation";
import Modal from "@mui/material/Modal";
import Button from "../button";
import SettlementDetailItem from "./settlementDetailItem";
import Loading from "./loading";
import { getSettlementDetailsApi } from "@/services/settlement";
import { jalaliConverter } from "@/utils/jalaliConverter";
import { rialSeparator } from "@/utils/rialSeperator";
import { SettlementDetailsType } from "@/services/settlement/types";
import {
  SUCCESSFULL_SETTLEMENT,
  SUCCESSFULL_SETTLEMENT_TITLE,
  FAILED_SETTLEMENT,
  FAILED_SETTLEMENT_TITLE,
  DELAY_PENDING_SETTLEMENT,
  DELAY_PENDING_SETTLEMENT_TITLE,
  QUEUE_PENDING_SETTLEMENT,
  QUEUE_PENDING_SETTLEMENT_TITLE,
  USER_PENDING_SETTLEMENT,
  USER_PENDING_SETTLEMENT_TITLE,
} from "@/enums/settlementStatusEnums";
import {
  GROUP_SETTLEMENT,
  SINGLE_SETTLEMENT,
} from "@/enums/settlementTypeEnums";

export default function SettlementDetailsModal({
  open,
  settlementId,
  isRetrySettlement = false,
  onClose,
  onCancelClick,
}: {
  open: boolean;
  settlementId: number;
  isRetrySettlement?: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onCancelClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  const [data, setData] = useState<SettlementDetailsType | null>(null);
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const getSettlementDetails = useCallback(async () => {
    setLoading(true);

    const res = await getSettlementDetailsApi(settlementId);

    if (res) {
      setData(res?.data);
    }

    setLoading(false);
  }, [settlementId]);

  useEffect(() => {
    if (open) {
      getSettlementDetails();
    }
  }, [getSettlementDetails, open]);

  function getSettlementStatusTitle(status?: number) {
    switch (status) {
      case SUCCESSFULL_SETTLEMENT:
        return SUCCESSFULL_SETTLEMENT_TITLE;

      case FAILED_SETTLEMENT:
        return FAILED_SETTLEMENT_TITLE;

      case DELAY_PENDING_SETTLEMENT:
        return DELAY_PENDING_SETTLEMENT_TITLE;

      case QUEUE_PENDING_SETTLEMENT:
        return QUEUE_PENDING_SETTLEMENT_TITLE;

      case USER_PENDING_SETTLEMENT:
        return USER_PENDING_SETTLEMENT_TITLE;

      default:
        break;
    }
  }

  function getSettlementTypeTitle(type?: number) {
    switch (type) {
      case GROUP_SETTLEMENT:
        return "گروهی";

      case SINGLE_SETTLEMENT:
        return "تکی";

      default:
        break;
    }
  }

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="text-center mb-2">
          <span>نمایش جزئیات تراکنش</span>
        </div>

        <hr className="w-full border-primary mb-4" />

        <div className="w-full overflow-auto bg-gray rounded-lg px-6 py-4">
          {/* <div className="mb-8">
            <span className="text-black">
              کاربر ثبت کننده درخواست: علیرضا هاشمی فاخر
            </span>
          </div> */}

          {loading ? (
            <Loading />
          ) : (
            <div className="grid grid-cols-1 lg:grid-cols-3 gap-4">
              <SettlementDetailItem
                title="نام و نام خانوادگی"
                value={`${data?.first_name} ${data?.last_name}`}
              />
              <SettlementDetailItem title="شماره شبا" value={data?.iban} />
              <SettlementDetailItem
                title="نوع تراکنش"
                value={getSettlementTypeTitle(data?.settlement_type)}
              />
              <SettlementDetailItem title="نام بانک" value={data?.bank_name} />
              <SettlementDetailItem
                title="کد پیگیری"
                value={data?.track_number.toString()}
              />
              <SettlementDetailItem
                title="تاریخ ثبت"
                value={
                  data?.create_date ? jalaliConverter(data?.create_date) : ""
                }
              />
              <SettlementDetailItem
                title="نتیجه تسویه"
                value={getSettlementStatusTitle(data?.settlement_status)}
              />
              <SettlementDetailItem
                title="مبلغ"
                value={rialSeparator(data?.amount.toString())}
              />
              <SettlementDetailItem
                title="زمان تسویه"
                value={
                  data?.settlement_date
                    ? jalaliConverter(data?.settlement_date)
                    : ""
                }
              />
              <SettlementDetailItem
                title="شناسه حساب"
                value={data?.bank_track_id}
              />
              <SettlementDetailItem
                title="مبلغ کارمزد"
                value={
                  data?.wage_amount
                    ? //TODO: Check "." in wage amount value from backend (100.000)
                      rialSeparator(data?.wage_amount)
                    : ""
                }
              />
              <SettlementDetailItem
                title="مبلغ کارمزد بانکی"
                value={
                  data?.bank_wage_amount
                    ? //TODO: Check "." in wage amount value from backend (100.000)
                      rialSeparator(data?.bank_wage_amount)
                    : ""
                }
              />
              <SettlementDetailItem
                title="زمان تاخیر تا تسویه"
                value={
                  data?.settlement_delay_time
                    ? `${data?.settlement_delay_time.toString()} ساعت`
                    : "در لحظه"
                }
              />
              <SettlementDetailItem
                title="توضیحات"
                className="lg:col-span-2"
                value={data?.description}
              />
              {data?.settlement_status === FAILED_SETTLEMENT && (
                <SettlementDetailItem
                  title="علت خطا"
                  value={data?.response}
                  isErrorMessage
                />
              )}
            </div>
          )}
        </div>

        <div className="grid grid-cols-2 gap-4 mt-4 self-end">
          {isRetrySettlement ? (
            <Button
              padding="!p-0"
              fullRounded={false}
              className="!col-span-1 lg:!w-36 lg:!h-8"
              onClick={() =>
                router.push(`/panel/checkout/single?id=${settlementId}`)
              }
            >
              درخواست مجدد
            </Button>
          ) : (
            <Button
              fullRounded={false}
              className="!col-span-1 lg:!w-36 lg:!h-8"
            >
              چاپ
            </Button>
          )}
          <Button
            fullRounded={false}
            variant="cancel"
            className="!col-span-1 lg:!w-36 lg:!h-8"
            onClick={onCancelClick}
          >
            بستن
          </Button>
        </div>
      </div>
    </Modal>
  );
}
