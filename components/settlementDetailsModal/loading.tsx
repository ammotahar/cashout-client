import { Skeleton } from "@mui/material";

const n = 15;

const Loading = () => {
  return (
    <div className="grid grid-cols-1 lg:grid-cols-3 gap-4">
      {[...Array(n)].map((item, index) => (
        <Skeleton
          key={index}
          variant="rounded"
          className="!col-span-1 !min-w-72 !h-12"
        />
      ))}
    </div>
  );
};

export default Loading;
