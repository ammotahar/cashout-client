const SettlementDetailItem = ({
  title,
  value,
  className,
  isErrorMessage = false,
}: {
  title: string;
  value?: string;
  className?: string;
  isErrorMessage?: boolean;
}) => {
  return (
    <div
      className={`flex items-center justify-between bg-white gap-4 p-4 rounded-lg col-span-1 text-sm min-w-72 ${
        isErrorMessage && "border border-error"
      } ${className}`}
    >
      <div>
        <span className={isErrorMessage ? "text-error" : "text-black"}>
          {title}
        </span>
      </div>

      <div>
        <span>{value}</span>
      </div>
    </div>
  );
};

export default SettlementDetailItem;
