export default function FormTitle({
  title,
  className,
}: {
  title: string;
  className?: string;
}) {
  return (
    <div className={`text-center lg:text-start ${className}`}>
      <div>
        <span className="text-black">{title}</span>
      </div>

      <hr className="border-table my-4" />
    </div>
  );
}
