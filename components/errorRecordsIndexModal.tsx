"use client";

import { MouseEventHandler } from "react";
import Modal from "@mui/material/Modal";
import Button from "./button";

export default function ErrorRecordsIndexModal({
  open,
  onClose,
  errorRecordsIndex,
  onCloseClick,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  errorRecordsIndex: number[];
  onCloseClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="flex flex-col items-center w-full border border-table rounded">
          <div className="border-b border-table text-center w-full py-2">
            شماره ردیف
          </div>

          <div className="w-full h-96 overflow-auto flex flex-col items-center">
            {errorRecordsIndex.map((item) => (
              <div
                className="w-full text-center border-b border-table py-2"
                key={item}
              >
                {item}
              </div>
            ))}
          </div>
        </div>

        <div className="grid grid-cols-2 gap-4 mt-4 self-end">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            padding="!p-0"
          >
            <span>چاپ</span>
          </Button>
          <Button
            fullRounded={false}
            variant="cancel"
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={onCloseClick}
          >
            <span>بستن</span>
          </Button>
        </div>
      </div>
    </Modal>
  );
}
