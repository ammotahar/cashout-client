import React from "react";
import {
  DatePicker as MuiDatePicker,
  LocalizationProvider,
} from "@mui/x-date-pickers";
import { Typography } from "@mui/material";
import { AdapterDateFnsJalali } from "@mui/x-date-pickers/AdapterDateFnsJalali";

export default function Datepicker({
  value,
  onChange,
  label,
  error,
  name,
  minDate = false,
  size = "small",
  containerClassNames,
  disabled = false,
}: {
  value: any;
  onChange?: (value: any) => void;
  label?: string;
  error?: any;
  name?: string;
  minDate?: any;
  size?: "small" | "medium";
  containerClassNames?: string;
  disabled?: boolean;
}) {
  return (
    <div className={containerClassNames}>
      <LocalizationProvider dateAdapter={AdapterDateFnsJalali}>
        <MuiDatePicker
          value={value}
          onChange={(newValue) => onChange && onChange(newValue)}
          className="bg-white w-full"
          label={label}
          disabled={disabled}
          slotProps={{
            textField: {
              sx: {
                boxShadow: "none",
                ".MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline":
                  {
                    borderColor: "#4B17A4",
                  },
                ".MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline":
                  {
                    borderColor: "#4B17A4",
                  },
              },
              size,
            },
          }}
          {...(minDate && { minDate })}
        />
        {name && (
          <Typography>{error?.[name] && error[name].message}</Typography>
        )}
      </LocalizationProvider>
    </div>
  );
}
