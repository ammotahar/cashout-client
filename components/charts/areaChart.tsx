"use client";

import dynamic from "next/dynamic";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });
import { jalaliConverter, getJalaliDay } from "@/utils/jalaliConverter";

export default function AreaChart({ data }: { data: any }) {
  const series = [
    {
      name: "واریزی",
      data: data?.map((item: any) => item?.amount),
    },
  ];

  const options = {
    chart: {
      height: 350,
      type: "area",
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth",
    },
    xaxis: {
      type: "datetime",
      categories: data?.map((item: any) => jalaliConverter(item?.confirm_date)),
      labels: {
        datetimeUTC: false,
        datetimeFormatter: {
          year: "yyyy",
          month: "MMM",
          day: "dd",
          hour: "HH:mm",
        },
        formatter: function (value: any, timestamp: any, opts: any) {
          return getJalaliDay(new Date(timestamp).toISOString());
        },
      },
    },
    yaxis: {
      show: false,
    },
    tooltip: {
      x: {
        format: "yyyy/MM/dd HH:mm",
        formatter: function (value: any) {
          return `${new Date(value).toISOString().split("T")[0]} ${
            new Date(value).toISOString().split("T")[1]
          }`;
        },
      },
    },
    legend: {
      show: false,
    },
    colors: ["#6C64A0"],
  };

  return (
    <Chart
      // @ts-ignore
      options={options}
      series={series}
      type="area"
      height={350}
      width="100%"
    />
  );
}
