"use client";

import dynamic from "next/dynamic";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });

export default function PieChart({
  data,
}: {
  data: {
    successful_transactions_percent: number;
    pending_transactions_percent: number;
    failed_transactions_percent: number;
    checking_queue_transactions_percent: number;
    pending_user_transactions_percent: number;
  };
}) {
  const series = [
    data?.successful_transactions_percent,
    data?.pending_transactions_percent,
    data?.failed_transactions_percent,
    data?.checking_queue_transactions_percent,
    data?.pending_user_transactions_percent,
  ];

  const options = {
    chart: {
      width: 380,
      type: "pie",
    },
    legend: {
      show: false,
    },
    colors: ["#5BC2A5", "#FBBD19", "#FE185C", "#AE3FE0", "#4640E1"],
    dataLabels: {
      style: {
        fontSize: "14px",
        fontFamily: "vazirFont",
        fontWeight: "",
        colors: ["#FFF"],
      },
    },
    // labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 280,
            height: 280,
          },
          legend: {
            position: "bottom",
          },
        },
      },
      {
        breakpoint: 1024,
        options: {
          chart: {
            width: 280,
            height: 280,
          },
        },
      },
      {
        breakpoint: 1490,
        options: {
          chart: {
            width: 210,
            height: 210,
          },
        },
      },
    ],
  };

  return (
    <div>
      <Chart
        // @ts-ignore
        options={options}
        series={series}
        type="pie"
        width={280}
        height={280}
      />
    </div>
  );
}
