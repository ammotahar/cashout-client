import { MouseEventHandler } from "react";
import Modal from "@mui/material/Modal";
import Button from "./button";
import { SuccessIcon } from "@/assets/icons/success";
import { ErrorIcon } from "@/assets/icons/error";

export default function MessageModal({
  open,
  onClose,
  title,
  variant,
  message,
  content,
  onSubmitClick,
  showCancelButton = false,
  onCancelClick,
  showCustomButton = false,
  customButtonTitle,
  onCustomButtonClick,
  loading = false,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  title: string;
  message: string;
  variant: "success" | "error";
  content?: React.ReactNode;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  showCancelButton?: boolean;
  onCancelClick?: MouseEventHandler<HTMLButtonElement>;
  showCustomButton?: boolean;
  customButtonTitle?: string;
  onCustomButtonClick?: MouseEventHandler<HTMLButtonElement>;
  loading?: boolean;
}) {
  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 rounded-xl border-2 border-success p-4 mx-4">
        <div className="text-center mb-2">
          <span className={`text-sm ${variant === "error" && "text-error"}`}>
            {title}
          </span>
        </div>

        <hr className="w-full border-primary" />

        <div className="mt-6">
          {variant === "error" ? <ErrorIcon /> : <SuccessIcon />}
        </div>

        {content}

        <div className="text-center mt-2">
          <span className="text-sm">{message}</span>
        </div>

        <div
          className={`grid ${
            showCancelButton || showCustomButton ? "grid-cols-2" : "grid-cols-1"
          } gap-4 mt-4`}
        >
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={onSubmitClick}
            loading={loading}
          >
            تایید
          </Button>
          {showCustomButton && (
            <Button
              variant="secondary"
              className="!col-span-1 lg:!h-10"
              onClick={onCustomButtonClick}
            >
              {customButtonTitle}
            </Button>
          )}
          {showCancelButton && (
            <Button
              fullRounded={false}
              variant="cancel"
              className="!col-span-1 lg:!w-36 lg:!h-10"
              onClick={onCancelClick}
            >
              انصراف
            </Button>
          )}
        </div>
      </div>
    </Modal>
  );
}
