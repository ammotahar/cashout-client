"use client";

import { useState, useEffect } from "react";
import { usePathname } from "next/navigation";
import Drawer from "./drawer";
import Footer from "@/components/footer";
import ExpandMoreIcon from "@/assets/icons/white/expandMore";

export default function PanelContainer({
  children,
}: {
  children: React.ReactNode;
}) {
  const [open, setOpen] = useState(false);

  const pathname = usePathname();

  useEffect(() => {
    if (open) {
      setOpen(false);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  return (
    <div className="flex flex-col justify-between bg-panel bg-cover bg-no-repeat bg-center p-4 pb-0 relative content-area">
      <div>
        <div className="flex items-center justify-between mb-4 lg:hidden">
          <Drawer
            open={open}
            onMenuClick={() => setOpen(true)}
            onClose={() => setOpen(false)}
          />

          <div className="flex items-center pr-2 bg-blue drop-shadow-expand w-8 h-8 absolute left-0 rounded-r-full">
            <ExpandMoreIcon className="w-5 h-5" />
          </div>
        </div>

        {children}
      </div>

      <Footer />
    </div>
  );
}
