"use client";

import { useState, useEffect } from "react";
import { useRouter, usePathname } from "next/navigation";
import MenuItem from "./menuItem";
import DashboardIcon from "@/assets/icons/white/dashboard";
import CheckoutIcon from "@/assets/icons/white/checkout";
import ChargeAccountIcon from "@/assets/icons/white/chargeAccount";
import AddAndEditUserIcon from "@/assets/icons/white/addAndEditUser";
import CompaniesFeeReportIcon from "@/assets/icons/white/companiesFeeReport";

export default function Menu() {
  const pathname = usePathname();
  const [activeMenu, setActiveMenu] = useState<string>(pathname);

  const router = useRouter();

  const handleClickMenu = (route: string) => {
    setActiveMenu(route);
    router.push(route);
  };

  useEffect(() => {
    setActiveMenu(pathname);
  }, [pathname]);

  return (
    <div className="flex flex-col gap-6 mt-10">
      <MenuItem
        title="داشبورد"
        icon={<DashboardIcon />}
        isActive={activeMenu === "/panel"}
        onClick={() => handleClickMenu("/panel")}
      />
      <MenuItem
        title="تسویه حساب"
        icon={<CheckoutIcon />}
        isActive={activeMenu.startsWith("/panel/checkout")}
        onClick={() => handleClickMenu("/panel/checkout/single")}
      />
      <MenuItem
        title="مدیریت کیف پول"
        icon={<ChargeAccountIcon />}
        isActive={activeMenu.startsWith("/panel/charge")}
        onClick={() => handleClickMenu("/panel/charge/receipt")}
      />
      <MenuItem
        title="مدیریت کاربران"
        icon={<AddAndEditUserIcon />}
        isActive={activeMenu.startsWith("/panel/users")}
        onClick={() => handleClickMenu("/panel/users/create")}
      />
      {/* <MenuItem
        title="گزارش کارمزد شرکت‌ها"
        icon={<CompaniesFeeReportIcon />}
        isActive={activeMenu.startsWith("/panel/report")}
        onClick={() => handleClickMenu("/panel/report")}
      /> */}
      <MenuItem
        title="مدیریت حساب‌ها"
        icon={<AddAndEditUserIcon />}
        isActive={activeMenu.startsWith("/panel/account")}
        onClick={() => handleClickMenu("/panel/account/create")}
      />
    </div>
  );
}
