import { MouseEventHandler } from "react";
import Button from "../button";

export default function MenuItem({
  title,
  icon,
  isActive,
  onClick,
}: {
  title: string;
  icon: React.ReactNode;
  isActive: boolean;
  onClick: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <Button variant="text" onClick={onClick}>
      <div
        className={`flex items-center w-full ${
          isActive && "bg-sidebar shadow-sidebar rounded-lg"
        } p-3 gap-6`}
      >
        {icon}
        <span
          className={`${isActive ? "text-white" : "text-paleCyan"} text-sm`}
        >
          {title}
        </span>
      </div>
    </Button>
  );
}
