import Image from "next/image";
import Menu from "./menu";
import CashoutLogo from "assets/images/cashout-logo.svg";

export default function Sidebar({
  type = "sidebar",
}: {
  type?: "sidebar" | "drawer";
}) {
  return (
    <div
      className={`${
        type === "sidebar" ? "hidden lg:block w-1/5" : "w-full"
      } min-w-max bg-blue rounded-l-2xl p-4 min-h-screen h-full lg:h-auto`}
    >
      <div className="flex items-center justify-center">
        <Image src={CashoutLogo} alt="cashout" className="w-14 h-14" />
      </div>

      <hr className="border-1 border-white mt-2" />

      <Menu />
    </div>
  );
}
