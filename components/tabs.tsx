import { SyntheticEvent } from "react";
import MuiTabs from "@mui/material/Tabs";
import MuiTab from "@mui/material/Tab";

export default function Tabs({
  value,
  onChange,
  tabs,
}: {
  value: any;
  onChange: (event: SyntheticEvent<Element, Event>, value: any) => void;
  tabs?: string[];
}) {
  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  return (
    <div>
      <MuiTabs
        value={value}
        onChange={onChange}
        aria-label="basic tabs example"
        className="!mb-2 !px-0 !min-w-0 !min-h-0"
      >
        {tabs?.map((item, index) => (
          <MuiTab
            key={index}
            label={item}
            {...a11yProps(index)}
            className="!pb-2 !pt-0 !px-0 !ml-4 !min-w-0 !min-h-0"
          />
        ))}
      </MuiTabs>
    </div>
  );
}
