"use client";

import { MouseEventHandler, useState, useEffect } from "react";
import Modal from "@mui/material/Modal";
import MaterialTextField from "./materialTextField";
import Button from "./button";

export default function FileNameModal({
  open,
  onClose,
  onSubmit,
  onCancelClick,
  loading,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onSubmit?: (fileName: string) => void;
  onCancelClick?: MouseEventHandler<HTMLButtonElement>;
  loading?: boolean;
}) {
  const [fileName, setFileName] = useState("");
  const [error, setError] = useState(false);

  useEffect(() => {
    if (!open) {
      setFileName("");
    }
  }, [open]);

  useEffect(() => {
    if (fileName !== "") {
      setError(false);
    }
  }, [fileName]);

  function onSubmitClick() {
    if (fileName !== "") {
      onSubmit && onSubmit(fileName);
    } else {
      setError(true);
    }
  }

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="text-center mb-2">
          <span>نام گذاری فایل</span>
        </div>

        <hr className="w-full border-primary mb-4" />

        <div className="mb-4">
          <span>لطفا نام فایل خود را انتخاب کنید.</span>
        </div>

        <div>
          <MaterialTextField
            value={fileName}
            onChange={(event) => setFileName(event.target.value)}
            label="نام فایل"
            error={error}
            helperText="لطفا نام فایل را وارد کنید."
          />
        </div>

        <div className="grid grid-cols-2 gap-4 mt-4">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={onSubmitClick}
            loading={loading}
          >
            <span>تایید</span>
          </Button>
          <Button
            fullRounded={false}
            variant="cancel"
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={onCancelClick}
          >
            <span>انصراف</span>
          </Button>
        </div>
      </div>
    </Modal>
  );
}
