"use client";

import { useState } from "react";
import Button from "./button";
import { downloadExcelBulkSettlementApi } from "@/services/batchSettlement";

export default function ExcelDownloadButton({ file_id }: { file_id: number }) {
  const [loading, setLoading] = useState(false);

  async function downloadExcelBulkSettlement() {
    setLoading(true);

    const res = await downloadExcelBulkSettlementApi({ file_id });

    if (res) {
      window.open(res?.data?.file_path);
    }

    setLoading(false);
  }

  return (
    <Button
      variant="secondary"
      className="!text-xs !rounded-full"
      loading={loading}
      onClick={downloadExcelBulkSettlement}
    >
      دانلود
    </Button>
  );
}
