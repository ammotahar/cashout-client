import Image from "next/image";
import EniacLogo from "assets/images/eniac.svg";

export default function Footer() {
  return (
    <div className="flex items-center justify-between py-2 border-t border-footer mt-4">
      <div>
        <span className="text-footer text-xs">
          کلیه حقوق این پنل متعلق به شرکت فناوران انیاک رایانه می باشد.
        </span>
      </div>

      <div>
        <Image src={EniacLogo} alt="eniac" />
      </div>
    </div>
  );
}
