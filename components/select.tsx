"use client";

import { ReactNode } from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import MuiSelect, { SelectChangeEvent } from "@mui/material/Select";

export default function Select({
  label,
  className,
  value,
  onChange,
  items,
  error = false,
  helperText,
  disabled = false,
}: {
  label?: string;
  className?: string;
  value?: any;
  onChange?: (event: SelectChangeEvent<any>, child: ReactNode) => void;
  items: { id: number | string; name: string }[];
  error?: boolean;
  helperText?: string;
  disabled?: boolean;
}) {
  return (
    <Box sx={{ minWidth: 120 }} className={className}>
      <FormControl fullWidth size="small">
        <InputLabel id="demo-simple-select-label" size="small">
          {label}
        </InputLabel>
        <MuiSelect
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          label={label}
          onChange={onChange}
          size="small"
          error={error}
          disabled={disabled}
        >
          {items.map((item) => (
            <MenuItem key={item.id} value={item.id}>
              {item?.name}
            </MenuItem>
          ))}
        </MuiSelect>
      </FormControl>
    </Box>
  );
}
