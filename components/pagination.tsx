"use client";

import { ChangeEvent } from "react";
import MuiPagination from "@mui/material/Pagination";

export default function Pagination({
  page,
  count,
  onChange,
}: {
  page: number;
  count: number;
  onChange: ((event: ChangeEvent<unknown>, page: number) => void) | undefined;
}) {
  return (
    <MuiPagination
      page={page}
      count={count}
      color="secondary"
      onChange={onChange}
      sx={{
        "& .Mui-selected": { color: "#FFF !important" },
        "& .MuiButtonBase-root": {
          borderRadius: "4px",
          minWidth: "1.7rem",
          width: "1.7rem",
          height: "1.7rem",
        },
      }}
    />
  );
}
