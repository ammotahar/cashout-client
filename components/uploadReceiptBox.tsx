"use client";

import { MouseEventHandler, useRef, useState } from "react";
import Image from "next/image";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Button from "./button";
import UploadReceiptImage from "assets/images/upload-receipt.svg";

export default function UploadReceiptBox({
  value,
  onChange,
  onDeleteFileClick,
  error = false,
  helperText,
}: {
  value?: File;
  onChange?: (file: File) => void;
  onDeleteFileClick?: MouseEventHandler<HTMLButtonElement>;
  error?: boolean;
  helperText?: string;
}) {
  const [dragActive, setDragActive] = useState(false);

  const inputRef = useRef<HTMLInputElement>(null);

  const handleDrag = function (e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.type === "dragenter" || e.type === "dragover") {
      setDragActive(true);
    } else if (e.type === "dragleave") {
      setDragActive(false);
    }
  };

  const handleDrop = function (e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      onChange && onChange(e.dataTransfer.files[0]);
    }
  };

  const handleValue = (value: any) => {
    onChange && onChange(value.target.files[0]);
  };

  return (
    <div
      className=" w-full rounded border border-dashed border-gray p-4"
      onDragEnter={handleDrag}
      onDragLeave={handleDrag}
      onDragOver={handleDrag}
      onDrop={handleDrop}
    >
      <input
        className="hidden"
        id="contained-button-file"
        name="file"
        accept="*"
        type="file"
        ref={inputRef}
        onChange={handleValue}
      />

      <label
        htmlFor="contained-button-file"
        className="w-full flex flex-col items-center gap-4 cursor-pointer"
      >
        <Image src={UploadReceiptImage} alt="upload-receipt" />

        <div className="text-center">
          {dragActive ? (
            <span className="text-black">رها کنید.</span>
          ) : (
            <span className="text-black">
              لطفا عکس فیش واریزی خود را در{" "}
              <span className="text-primary">اینجا</span> آپلود کنید.
            </span>
          )}
        </div>

        <div className="w-full flex justify-between text-xs text-subtitle">
          <div>
            <span>حجم فایل: 17 مگابایت</span>
          </div>

          <div>
            <span>فرمت فایل: PNG، JPG</span>
          </div>
        </div>
      </label>

      {value && (
        <div className="w-full flex flex-col items-center justify-center mt-4 gap-4">
          <Image
            src={URL.createObjectURL(value)}
            alt="selected-image"
            width={100}
            height={100}
          />

          <Button
            variant="text"
            className="!text-error !flex !items-center"
            onClick={onDeleteFileClick}
          >
            <DeleteOutlineIcon />
            <div>
              <span>حذف</span>
            </div>
          </Button>
        </div>
      )}

      {error && (
        <div className="w-full text-center mt-4 text-xs text-error">
          <span>{helperText}</span>
        </div>
      )}
    </div>
  );
}
