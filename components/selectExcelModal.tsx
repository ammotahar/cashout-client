"use client";

import { MouseEventHandler, useEffect, useState } from "react";
import Modal from "@mui/material/Modal";
import MuiRadio from "@mui/material/Radio";
import SearchField from "@/components/searchField";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import Button from "./button";
import ExcelDownloadButton from "./excelDownloadButton";
import { getBatchSettlementListApi } from "@/services/batchSettlement";
import { BatchSettlementListType } from "@/services/batchSettlement/types";

export default function SelectExcelModal({
  open,
  onClose,
  onSelectClick,
  onCancelClick,
  selected,
  setSelected,
}: {
  open: boolean;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onSelectClick: () => void;
  onCancelClick?: MouseEventHandler<HTMLButtonElement>;
  selected: number;
  setSelected: (fileId: number) => void;
}) {
  const [data, setData] = useState<BatchSettlementListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");

  async function getBatchSettlementList(page?: number, search?: string) {
    setLoading(true);

    const res = await getBatchSettlementListApi({
      per_page: 10,
      page_number: page,
      file_title: search,
    });

    if (res) {
      setCount(res?.data?.count);
      setData(res?.data?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    if (open) {
      getBatchSettlementList(1, "");
    }
  }, [open]);

  useEffect(() => {
    setPage(1);
    getBatchSettlementList(1, search);
  }, [search]);

  function pageHandler(event: any, page: number) {
    setPage(page);
    getBatchSettlementList(page, search);
  }

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="text-center mb-2">
          <span>انتخاب فایل</span>
        </div>

        <hr className="w-full border-primary mb-4" />

        <div className="w-full max-w-sm self-start">
          <SearchField
            placeholder="بر اساس نام فایل"
            onChange={(value) => setSearch(value)}
          />
        </div>

        <div className="mt-4 w-full overflow-auto">
          <Table
            heads={[
              "انتخاب",
              "نام فایل",
              "مبلغ کل ",
              "تعداد کل رکوردها",
              "عملیات",
            ]}
            data={data?.map((item) => ({
              select: (
                <MuiRadio
                  checked={selected === item.id}
                  onChange={() => {
                    setSelected(item.id);
                  }}
                />
              ),
              file_name: item?.file_title,
              total_amount: item?.total_amount,
              total_records: item?.total_count,
              operation: <ExcelDownloadButton file_id={item?.id} />,
            }))}
            loading={loading}
          />
        </div>

        <div className="mt-4 flex justify-end">
          <Pagination
            page={page}
            count={Math.ceil(count / 10)}
            onChange={pageHandler}
          />
        </div>

        <div className="grid grid-cols-2 gap-4 mt-4 self-end">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={onSelectClick}
          >
            <span>انتخاب</span>
          </Button>
          <Button
            fullRounded={false}
            variant="cancel"
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={onCancelClick}
          >
            <span>انصراف</span>
          </Button>
        </div>
      </div>
    </Modal>
  );
}
