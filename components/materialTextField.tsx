"use client";

import { ChangeEventHandler, HTMLInputTypeAttribute, forwardRef } from "react";
import { InputAdornment, TextField } from "@mui/material";
import { NumericFormat, NumericFormatProps } from "react-number-format";

interface CustomProps {
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

const NumericFormatCustom = forwardRef<NumericFormatProps, CustomProps>(
  function NumericFormatCustom(props, ref) {
    const { onChange, ...other } = props;

    return (
      <NumericFormat
        {...other}
        getInputRef={ref}
        onValueChange={(values) => {
          onChange({
            target: {
              name: props.name,
              value: values.value,
            },
          });
        }}
        thousandSeparator
        valueIsNumericString
        // prefix="ریال"
      />
    );
  }
);

export default function MaterialTextField({
  label,
  placeholder,
  multiline = false,
  className,
  value,
  onChange,
  error = false,
  helperText,
  startAdornments,
  disabled = false,
  isFormatNumber = false,
  type,
}: {
  label?: string;
  placeholder?: string;
  multiline?: boolean;
  className?: string;
  value?: any;
  onChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  error?: boolean;
  helperText?: string;
  startAdornments?: string;
  disabled?: boolean;
  isFormatNumber?: boolean;
  type?: HTMLInputTypeAttribute;
}) {
  return (
    <TextField
      id="outlined-basic"
      label={label}
      placeholder={placeholder}
      variant="outlined"
      size="small"
      className={`!w-full ${className}`}
      multiline={multiline}
      rows={4}
      value={value}
      onChange={onChange}
      disabled={disabled}
      error={error}
      type={type}
      InputProps={{
        ...(isFormatNumber && { inputComponent: NumericFormatCustom as any }),
        ...(startAdornments && {
          endAdornment: (
            <InputAdornment position="start">{startAdornments}</InputAdornment>
          ),
          className: "!pl-0",
        }),
      }}
    />
  );
}
