import { SyntheticEvent } from "react";
import { Tabs, Tab } from "@mui/material";
import { Favorite } from "@mui/icons-material";

export default function FilterTabs({
  value,
  onChange,
  tabs,
}: {
  value: any;
  onChange: (event: SyntheticEvent<Element, Event>, value: any) => void;
  tabs?: string[];
}) {
  function a11yProps(index: number) {
    return {
      id: `full-width-tab-${index}`,
      "aria-controls": `full-width-tabpanel-${index}`,
    };
  }

  return (
    <Tabs
      value={value}
      onChange={onChange}
      variant="scrollable"
      scrollButtons="auto"
    >
      {tabs?.map((item, index) => (
        <Tab
          key={index}
          label={item}
          {...a11yProps(index)}
          icon={
            <span
              className={`${
                value === index
                  ? "bg-blue text-white"
                  : "border border-gray bg-white"
              } rounded-full px-2 py-1`}
            >
              429
            </span>
          }
          iconPosition="end"
          className="!min-h-0"
        />
      ))}
    </Tabs>
  );
}
