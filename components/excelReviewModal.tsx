"use client";

import { useEffect, useState, useCallback } from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Modal from "@mui/material/Modal";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import Button from "./button";
import { rialSeparator } from "@/utils/rialSeperator";
import Select from "./select";
import PriceText from "./priceText";
import {
  getSettlementFileRecordsApi,
  doBulkSettlementApi,
  addBulkSettlementToListApi,
  getBulkSettlementInfoApi,
} from "@/services/settlement";
import { deleteBatchSettlementApi } from "@/services/batchSettlement";
import { getAccountsListApi } from "@/services/account";
import { AccountsListType } from "@/services/account/types";
import {
  SettlementFileRecordType,
  BulkSettlementInfoType,
} from "@/services/settlement/types";
import {
  ENIAC_ACCOUNT,
  ENIAC_ACCOUNT_TITLE,
  COMPANY_ACCOUNT,
} from "@/enums/settlementFromEnums";
import { settlementDelayItems } from "@/utils/settlementDelayItems";

export default function ExcelReviewModal({
  open,
  fileId,
  onClose,
  onCancelClick,
  onInstantSettlement,
  onAddSettlementToList,
}: {
  open: boolean;
  fileId: number;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onCancelClick?: () => void;
  onInstantSettlement?: (fileId: number, fileName: string) => void;
  onAddSettlementToList?: (fileId: number, fileName: string) => void;
}) {
  const [data, setData] = useState<SettlementFileRecordType[]>([]);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState<number>(0);
  const [page, setPage] = useState<number>(1);
  const [totalAmount, setTotalAmount] = useState<number>(0);
  const [settlementDelayTime, setSettlementDelayTime] = useState("");
  const [error, setError] = useState(false);
  const [doSettlementLoading, setDoSettlementLoading] = useState(false);
  const [addSettlementLoading, setAddSettlementLoading] = useState(false);
  const [deleteSettlementLoading, setDeleteSettlementLoading] = useState(false);
  const [accountList, setAccountList] = useState<AccountsListType[]>([]);
  const [account, setAccount] = useState("");
  const [bulkSettlementInfo, setBulkSettlementInfo] = useState<
    BulkSettlementInfoType | undefined
  >();
  const [bulkSettlementInfoLoading, setBulkSettlementInfoLoading] =
    useState(false);

  const getSettlementFileRecords = useCallback(async () => {
    setLoading(true);

    const res = await getSettlementFileRecordsApi(
      { file_id: fileId },
      { page_number: page, per_page: 10 }
    );

    if (res) {
      setData(res?.data?.data);
      setCount(res?.data?.count);
      setTotalAmount(res?.data?.total_amount);
    } else {
      setData([]);
      setCount(0);
      setTotalAmount(0);
    }

    setLoading(false);
  }, [fileId, page]);

  async function getAccountsList() {
    const res = await getAccountsListApi();
    if (res) {
      setAccountList(res?.data?.data);
    }
  }

  useEffect(() => {
    getAccountsList();
  }, []);

  useEffect(() => {
    getSettlementFileRecords();
  }, [getSettlementFileRecords]);

  useEffect(() => {
    if (account !== "") {
      setError(false);
    }
  }, [account]);

  function pageHandler(event: any, page: number) {
    setPage(page);
  }

  async function doBulkSettlement() {
    if (account !== "") {
      setDoSettlementLoading(true);
      const res = await doBulkSettlementApi({
        file_id: fileId,
        settlement_from:
          typeof account !== "string" && account === 0
            ? ENIAC_ACCOUNT
            : COMPANY_ACCOUNT,
        settlement_delay_time: settlementDelayTime
          ? parseInt(settlementDelayTime)
          : null,
        account:
          typeof account !== "string" && account > 0 ? account : undefined,
      });

      if (res) {
        if (res?.data?.file_id > 0) {
          setSettlementDelayTime("");
          setAccount("");
          onInstantSettlement &&
            onInstantSettlement(res?.data?.file_id, res?.data?.file_name);
        }
      }
    } else {
      setError(true);
    }

    setDoSettlementLoading(false);
  }

  async function addBulkSettlementToList() {
    if (account !== "") {
      setAddSettlementLoading(true);
      const res = await addBulkSettlementToListApi({
        file_id: fileId,
        settlement_from:
          typeof account !== "string" && account === 0
            ? ENIAC_ACCOUNT
            : COMPANY_ACCOUNT,
        settlement_delay_time: settlementDelayTime
          ? parseInt(settlementDelayTime)
          : null,
        account:
          typeof account !== "string" && account > 0 ? account : undefined,
      });

      if (res) {
        setSettlementDelayTime("");
        setAccount("");
        onAddSettlementToList &&
          onAddSettlementToList(res?.data?.file_id, res?.data?.file_name);
      }
    } else {
      setError(true);
    }

    setAddSettlementLoading(false);
  }

  async function getBulkSettlementInfo(accountId: number) {
    setBulkSettlementInfoLoading(true);

    const res = await getBulkSettlementInfoApi({
      file_id: fileId,
      settlement_from:
        typeof accountId !== "string" && accountId === 0
          ? ENIAC_ACCOUNT
          : COMPANY_ACCOUNT,
    });

    if (res) {
      setBulkSettlementInfo(res?.data);
    }

    setBulkSettlementInfoLoading(false);
  }

  async function deleteBatchSettlement() {
    setDeleteSettlementLoading(true);

    const res = await deleteBatchSettlementApi(fileId);

    setDeleteSettlementLoading(false);

    onCancelClick && onCancelClick();
  }

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="text-center mb-2">
          <span>مشاهده لیست فایل اکسل</span>
        </div>

        <hr className="w-full border-primary mb-4" />

        <div className="mt-4 w-full overflow-auto">
          <Table
            heads={["نام و نام خانوادگی", "شماره شبا", "مبلغ"]}
            data={data?.map((item) => ({
              name: `${item.first_name} ${item.last_name}`,
              iban: item.iban,
              amount: rialSeparator(item.amount.toString()),
              // hasError: true,
            }))}
            loading={loading}
          />
        </div>

        <div className="mt-4 flex justify-end">
          <Pagination
            page={page}
            count={Math.ceil(count / 10)}
            onChange={pageHandler}
          />
        </div>

        <hr className="border-table border-dashed w-full my-4" />

        <div className="grid grid-cols-2 w-full gap-4">
          <Select
            label="زمان تاخیر تا تسویه"
            onChange={(event) => setSettlementDelayTime(event.target.value)}
            value={settlementDelayTime}
            items={settlementDelayItems}
            className="!col-span-1"
          />

          <Select
            label="تسویه از *"
            onChange={(event) => {
              getBulkSettlementInfo(event.target.value);
              setAccount(event.target.value);
            }}
            value={account}
            items={[
              { id: 0, name: ENIAC_ACCOUNT_TITLE },
              ...accountList.map((item) => {
                return { id: item.id, name: item.account_name };
              }),
            ]}
            className="!col-span-1"
            error={error}
          />
        </div>

        <div className="w-full bg-gray border border-table rounded-xl mt-4 p-4 text-sm">
          <div className="flex items-center justify-between">
            <div>
              <span className="text-gray">مبلغ کل رکوردها:</span>
            </div>

            <div>
              <PriceText
                price={totalAmount ? totalAmount.toString() : ""}
                priceTextColor="text-black"
                unitTextColor="text-black"
                priceFontSize="text-lg"
                unitFontSize="text-xs"
              />
            </div>
          </div>

          <hr className="border-table border-dashed w-full my-4" />

          <div className="flex items-center justify-between">
            <div>
              <span className="text-gray">تعداد کل رکوردها:</span>
            </div>

            <div>
              <span className="text-black">{count} عدد</span>
            </div>
          </div>

          <hr className="border-table border-dashed w-full my-4" />

          <div className="flex items-center justify-between">
            <div>
              <span className="text-gray">مبلغ کل کارمزد:</span>
            </div>

            <div>
              {bulkSettlementInfoLoading ? (
                <CircularProgress size={22} />
              ) : (
                <PriceText
                  price={
                    bulkSettlementInfo
                      ? bulkSettlementInfo?.total_wage.toString()
                      : ""
                  }
                  priceTextColor="text-black"
                  unitTextColor="text-black"
                  priceFontSize="text-lg"
                  unitFontSize="text-xs"
                />
              )}
            </div>
          </div>

          <hr className="border-table border-dashed w-full my-4" />

          <div className="flex items-center justify-between">
            <div>
              <span className="text-gray">مبلغ کل کارمزد بانکی:</span>
            </div>

            <div>
              {bulkSettlementInfoLoading ? (
                <CircularProgress size={22} />
              ) : (
                <PriceText
                  price={
                    bulkSettlementInfo
                      ? bulkSettlementInfo?.total_bank_wage.toString()
                      : ""
                  }
                  priceTextColor="text-black"
                  unitTextColor="text-black"
                  priceFontSize="text-lg"
                  unitFontSize="text-xs"
                />
              )}
            </div>
          </div>
        </div>

        <div className="grid grid-cols-3 gap-4 mt-4 self-end">
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            padding="!p-0"
            onClick={addBulkSettlementToList}
            loading={addSettlementLoading}
          >
            <span>افزودن به لیست</span>
          </Button>
          <Button
            fullRounded={false}
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={doBulkSettlement}
            loading={doSettlementLoading}
          >
            <span>تسویه حساب</span>
          </Button>
          <Button
            fullRounded={false}
            variant="cancel"
            className="!col-span-1 lg:!w-36 lg:!h-10"
            onClick={deleteBatchSettlement}
            loading={deleteSettlementLoading}
          >
            <span>حذف فایل و انصراف</span>
          </Button>
        </div>
      </div>
    </Modal>
  );
}
