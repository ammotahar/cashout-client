import Modal from "@mui/material/Modal";
import Button from "./button";
import { SuccessIcon } from "@/assets/icons/success";

export default function SuccessModal({
  open,
  onClose,
  title,
  message,
  body,
  buttons,
}: {
  open: boolean;
  onClose?: () => void;
  title?: string;
  message?: string;
  body?: React.ReactNode;
  buttons?: React.ReactNode;
}) {
  return (
    <Modal
      open={open}
      onClose={onClose}
      className="flex items-center justify-center"
    >
      <div className="bg-white flex flex-col items-center w-80 p-4 border border-success rounded-lg mx-4">
        <div>
          <span className="text-sm">{title}</span>
        </div>

        <hr className="w-full border-success my-4" />

        <div className="flex flex-col items-center">
          <div>
            <SuccessIcon />
          </div>

          <div className="my-4">
            {message && <span className="text-sm">{message}</span>}
            {body}
          </div>

          {!buttons && <Button onClick={onClose}>تایید</Button>}
          {buttons}
        </div>
      </div>
    </Modal>
  );
}
