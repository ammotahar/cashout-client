"use client";

import { useState, useEffect } from "react";
import Skeleton from "@mui/material/Skeleton";
import { ChevronLeft } from "@mui/icons-material";
import PieChart from "./charts/pieChart";
import { getSettlementTransactionApi } from "@/services/report";
import { GetSettlementTransactionType } from "@/services/report/getSettlementTransactionTypes";

export default function CheckoutsStatusBox() {
  const [data, setData] = useState<GetSettlementTransactionType | null>(null);
  const [loading, setLoading] = useState(false);

  async function getSettlementTransaction() {
    setLoading(true);

    const res = await getSettlementTransactionApi();

    if (res) {
      setData(res?.data);
    }

    setLoading(false);
  }

  useEffect(() => {
    getSettlementTransaction();
  }, []);

  return (
    <div className="flex flex-col items-center">
      <div>
        <span>نمایش کلی وضعیت تسویه‌ها</span>
      </div>

      <hr className="border-table w-full mt-6" />

      <div className="w-full grid grid-rows-3 gap-4 my-6">
        {loading ? (
          <Skeleton variant="rounded" className="!row-span-1 !py-8" />
        ) : (
          <div className="grid grid-cols-10 row-span-1 border border-table rounded-xl px-3 py-6">
            <div className="col-span-5 flex items-center gap-2">
              <div className="bg-success min-w-3 w-3 h-3 rounded" />
              <span className="text-black">تسویه شده</span>
            </div>

            <div className="col-span-3 flex items-center justify-center">
              <span className="text-black">{`${
                data ? data?.successful_transactions_count : 0
              } نفر`}</span>
            </div>

            <div className="col-span-2 flex items-center justify-end">
              <span className="text-success">
                {`${data ? data?.successful_transactions_percent : 0}%`}
              </span>
            </div>

            {/* <div className="col-span-4 flex items-center justify-end text-primary">
              <span>نمایش لیست</span>
              <ChevronLeft className="!text-base" />
            </div> */}
          </div>
        )}

        {loading ? (
          <Skeleton variant="rounded" className="!row-span-1 !py-8" />
        ) : (
          <div className="grid grid-cols-10 row-span-1 border border-table rounded-xl px-3 py-6">
            <div className="col-span-5 flex items-center gap-2">
              <div className="bg-error min-w-3 w-3 h-3 rounded" />
              <span className="text-black">تسویه نشده</span>
            </div>

            <div className="col-span-3 flex items-center justify-center">
              <span className="text-black">{`${
                data ? data?.failed_transactions_count : 0
              } نفر`}</span>
            </div>

            <div className="col-span-2 flex items-center justify-end">
              <span className="text-error">
                {`${data ? data?.failed_transactions_percent : 0}%`}
              </span>
            </div>

            {/* <div className="col-span-4 flex items-center justify-end text-primary">
              <span>نمایش لیست</span>
              <ChevronLeft className="!text-base" />
            </div> */}
          </div>
        )}

        {loading ? (
          <Skeleton variant="rounded" className="!row-span-1 !py-8" />
        ) : (
          <div className="grid grid-cols-10 row-span-1 border border-table rounded-xl px-3 py-6">
            <div className="col-span-5 flex items-center gap-2">
              <div className="bg-yellow min-w-3 w-3 h-3 rounded" />
              <span className="text-black">در انتظار تسویه</span>
            </div>

            <div className="col-span-3 flex items-center justify-center">
              <span className="text-black">{`${
                data ? data?.pending_transactions_count : 0
              } نفر`}</span>
            </div>

            <div className="col-span-2 flex items-center justify-end">
              <span className="text-yellow">
                {`${data ? data?.pending_transactions_percent : 0}%`}
              </span>
            </div>

            {/* <div className="col-span-4 flex items-center justify-end text-primary">
              <span>نمایش لیست</span>
              <ChevronLeft className="!text-base" />
            </div> */}
          </div>
        )}

        {loading ? (
          <Skeleton variant="rounded" className="!row-span-1 !py-8" />
        ) : (
          <div className="grid grid-cols-10 row-span-1 border border-table rounded-xl px-3 py-6">
            <div className="col-span-5 flex items-center gap-2">
              <div className="bg-purple min-w-3 w-3 h-3 rounded" />
              <span className="text-black">در انتظار تایید کاربر</span>
            </div>

            <div className="col-span-3 flex items-center justify-center">
              <span className="text-black">{`${
                data ? data?.pending_user_transactions_count : 0
              } نفر`}</span>
            </div>

            <div className="col-span-2 flex items-center justify-end">
              <span className="text-purple">
                {`${data ? data?.pending_user_transactions_percent : 0}%`}
              </span>
            </div>

            {/* <div className="col-span-4 flex items-center justify-end text-primary">
              <span>نمایش لیست</span>
              <ChevronLeft className="!text-base" />
            </div> */}
          </div>
        )}

        {loading ? (
          <Skeleton variant="rounded" className="!row-span-1 !py-8" />
        ) : (
          <div className="grid grid-cols-10 row-span-1 border border-table rounded-xl px-3 py-6">
            <div className="col-span-5 flex items-center gap-2">
              <div className="bg-blue min-w-3 w-3 h-3 rounded" />
              <span className="text-black">در صف</span>
            </div>

            <div className="col-span-3 flex items-center justify-center">
              <span className="text-black">{`${
                data ? data?.checking_queue_transactions_count : 0
              } نفر`}</span>
            </div>

            <div className="col-span-2 flex items-center justify-end">
              <span className="text-blue">
                {`${data ? data?.checking_queue_transactions_percent : 0}%`}
              </span>
            </div>

            {/* <div className="col-span-4 flex items-center justify-end text-primary">
              <span>نمایش لیست</span>
              <ChevronLeft className="!text-base" />
            </div> */}
          </div>
        )}
      </div>

      {loading ? (
        <Skeleton variant="circular" className="!w-64 !h-64" />
      ) : (
        <PieChart
          data={{
            successful_transactions_percent: data
              ? data?.successful_transactions_percent
              : 0,
            pending_transactions_percent: data
              ? data?.pending_transactions_percent
              : 0,
            failed_transactions_percent: data
              ? data?.failed_transactions_percent
              : 0,
            checking_queue_transactions_percent: data
              ? data?.checking_queue_transactions_percent
              : 0,
            pending_user_transactions_percent: data
              ? data?.pending_user_transactions_percent
              : 0,
          }}
        />
      )}
    </div>
  );
}
