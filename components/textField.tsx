"use client";

import { useState, HTMLInputTypeAttribute, ChangeEventHandler } from "react";
import Button from "@mui/material/Button";
import { VisibilityOff, Visibility } from "@mui/icons-material";

export default function TextField({
  title,
  icon,
  placeholder,
  className,
  inputClassName,
  type,
  value,
  onChange,
  error = false,
  helperText = "",
  maxLength,
}: {
  title?: string;
  icon?: React.ReactNode;
  placeholder?: string;
  className?: string;
  inputClassName?: string;
  type?: HTMLInputTypeAttribute;
  value?: string | number | readonly string[];
  onChange?: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
  error?: boolean;
  helperText?: string;
  maxLength?: number;
}) {
  const [visibility, setVisibility] = useState(false);

  return (
    <div className={`${className}`}>
      {title && (
        <div className="mb-1">
          <span className="text-black text-xs">{title}</span>
        </div>
      )}

      <div className="flex items-center border border-gray focus-within:border-primary rounded-md">
        {icon && (
          <div className="w-7 h-7 bg-gray rounded-md flex items-center justify-center ml-2">
            {icon}
          </div>
        )}

        <div className="textfield-input-container">
          <input
            placeholder={placeholder}
            className={`outline-none w-full rounded-md ${inputClassName}`}
            type={visibility ? "text" : type}
            value={value}
            onChange={onChange}
            maxLength={maxLength}
          />
        </div>

        {type === "password" && (
          <Button
            onClick={() => setVisibility(!visibility)}
            className="!m-0 !p-0 !min-w-0 !text-black"
          >
            {visibility ? (
              <Visibility className="!w-4 !h-4" />
            ) : (
              <VisibilityOff className="!w-4 !h-4" />
            )}
          </Button>
        )}
      </div>

      {error && (
        <div className="mt-2">
          <span className="text-error text-sm">{helperText}</span>
        </div>
      )}
    </div>
  );
}
