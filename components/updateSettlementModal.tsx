"use client";

import { MouseEventHandler, useEffect, useState, useCallback } from "react";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import Modal from "@mui/material/Modal";
import MaterialTextField from "./materialTextField";
import Select from "./select";
import Button from "./button";
import {
  getSettlementDetailsApi,
  updateSingleSettlementByIdApi,
} from "@/services/settlement";
import { getBankListApi } from "@/services/bank";
import { SettlementDetailsType } from "@/services/settlement/types";
import { BankListType } from "@/services/bank/types";
import { getAccountsListApi } from "@/services/account";
import { AccountsListType } from "@/services/account/types";
import {
  ENIAC_ACCOUNT,
  ENIAC_ACCOUNT_TITLE,
  COMPANY_ACCOUNT,
} from "@/enums/settlementFromEnums";
import { settlementDelayItems } from "@/utils/settlementDelayItems";

interface IFormInput {
  first_name: string;
  last_name: string;
  amount: string;
  iban: string;
  settlement_delay_time?: string;
  settlement_from: number | string;
  description?: string;
  bank?: string | number;
  account?: string | number;
}

export default function UpdateSettlementModal({
  open,
  settlementId,
  onFinishUpdateSettlement,
  onClose,
  onCancelClick,
}: {
  open: boolean;
  settlementId: number;
  onFinishUpdateSettlement?: () => void;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onCancelClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  const [loading, setLoading] = useState(false);
  const [bankList, setBankList] = useState<BankListType[]>([]);
  const [accountList, setAccountList] = useState<AccountsListType[]>([]);

  const {
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      first_name: "",
      last_name: "",
      amount: "",
      iban: "",
      bank: "",
      account: "",
      settlement_delay_time: "",
      settlement_from: "",
      description: "",
    },
  });

  const getSettlementDetails = useCallback(async () => {
    setLoading(true);

    const res = await getSettlementDetailsApi(settlementId);
    const data: SettlementDetailsType = res?.data;

    if (res) {
      setValue("first_name", data.first_name);
      setValue("last_name", data.last_name);
      setValue("iban", data.iban.split("IR")[1]);
      setValue("amount", data.amount.toString());
      setValue("bank", data.bank);
      setValue("account", data.account || 0);
      setValue("settlement_delay_time", data.settlement_delay_time?.toString());
      setValue("settlement_from", data.settlement_from);
      setValue("description", data.description);
    }

    setLoading(false);
  }, [setValue, settlementId]);

  useEffect(() => {
    if (open) {
      getSettlementDetails();
    }
  }, [getSettlementDetails, open]);

  async function getBankList() {
    const res = await getBankListApi();
    if (res) {
      setBankList(res?.data);
    }
  }

  async function getAccountsList() {
    const res = await getAccountsListApi();
    if (res) {
      setAccountList(res?.data?.data);
    }
  }

  useEffect(() => {
    if (open) {
      getAccountsList();
      getBankList();
    }
  }, [open]);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const res = await updateSingleSettlementByIdApi({
      settlement_id: settlementId,
      ...data,
      amount: parseInt(data.amount),
      iban: `IR${data?.iban}`,
      bank:
        typeof data?.bank !== "string" && data?.bank ? data?.bank : undefined,
      account:
        typeof data?.account !== "string" && data?.account && data?.account > 0
          ? data?.account
          : undefined,
      settlement_from: data?.account === 0 ? ENIAC_ACCOUNT : COMPANY_ACCOUNT,
      settlement_delay_time: data.settlement_delay_time
        ? parseInt(data.settlement_delay_time)
        : null,
    });

    if (res) {
      onFinishUpdateSettlement && onFinishUpdateSettlement();
    }

    setLoading(false);
  };

  return (
    <Modal
      open={open}
      onClose={onClose}
      className="!flex !items-center !justify-center"
    >
      <div className="bg-white flex flex-col items-center min-w-72 min-h-56 max-h-screen rounded-xl border-2 border-primary p-4 mx-4">
        <div className="text-center mb-2">
          <span>ویرایش درخواست</span>
        </div>

        <hr className="w-full border-primary mb-4" />

        <form
          className="w-full flex flex-col"
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className="w-full overflow-auto py-2">
            <div className="flex flex-col w-full gap-4 lg:grid lg:grid-cols-2">
              <Controller
                name="first_name"
                control={control}
                rules={{
                  required: "نام را وارد نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <MaterialTextField
                    label="نام *"
                    className="!col-span-1"
                    onChange={onChange}
                    value={value}
                    error={!!errors?.first_name}
                    helperText={errors?.first_name?.message}
                    disabled={loading}
                  />
                )}
              />

              <Controller
                name="last_name"
                control={control}
                rules={{
                  required: "نام خانوادگی را وارد نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <MaterialTextField
                    label="نام خانوادگی *"
                    className="!col-span-1"
                    onChange={onChange}
                    value={value}
                    error={!!errors?.first_name}
                    helperText={errors?.first_name?.message}
                    disabled={loading}
                  />
                )}
              />

              <Controller
                name="amount"
                control={control}
                rules={{
                  required: "مبلغ را وارد نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <MaterialTextField
                    label="مبلغ *"
                    className="!col-span-1"
                    onChange={onChange}
                    value={value}
                    error={!!errors?.amount}
                    helperText={errors?.amount?.message}
                    disabled={loading}
                  />
                )}
              />

              <Controller
                name="iban"
                control={control}
                rules={{
                  required: "شماره شبا را وارد نمایید.",
                  pattern: {
                    value: /^(?=.{24}$)[0-9]*$/gi,
                    message: "شماره شبا وارد شده صحیح نیست",
                  },
                }}
                render={({ field: { onChange, value } }) => (
                  <MaterialTextField
                    label="شماره شبا *"
                    startAdornments="IR"
                    className="!col-span-1"
                    onChange={onChange}
                    value={value}
                    error={!!errors?.iban}
                    helperText={errors?.iban?.message}
                    disabled={loading}
                  />
                )}
              />

              <Controller
                name="bank"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <Select
                    label="نام بانک"
                    className="!col-span-1"
                    onChange={onChange}
                    value={value}
                    items={bankList}
                    disabled={loading}
                  />
                )}
              />

              <Controller
                name="settlement_delay_time"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <Select
                    label="زمان تاخیر تا تسویه"
                    className="!col-span-1"
                    onChange={onChange}
                    value={value}
                    items={settlementDelayItems}
                    disabled={loading}
                  />
                )}
              />

              <Controller
                name="account"
                control={control}
                rules={{
                  validate: (v) =>
                    v !== "" || "حساب مورد نظر را انتخاب نمایید.",
                }}
                render={({ field: { onChange, value } }) => (
                  <Select
                    label="تسویه از *"
                    className="!col-span-1"
                    onChange={onChange}
                    value={value}
                    items={[
                      { id: 0, name: ENIAC_ACCOUNT_TITLE },
                      ...accountList.map((item) => {
                        return { id: item.id, name: item.account_name };
                      }),
                    ]}
                    error={!!errors?.account}
                    helperText={errors?.account?.message}
                    disabled={loading}
                  />
                )}
              />

              <Controller
                name="description"
                control={control}
                render={({ field: { onChange, value } }) => (
                  <MaterialTextField
                    label="توضیحات"
                    className="!col-span-2"
                    onChange={onChange}
                    value={value}
                    multiline
                    disabled={loading}
                  />
                )}
              />
            </div>
          </div>

          <div className="grid grid-cols-2 gap-4 mt-4 self-end">
            <Button
              padding="!p-1"
              fullRounded={false}
              className="!col-span-1 lg:!w-36 lg:!h-8"
              loading={loading}
              type="submit"
            >
              تایید و تسویه
            </Button>
            <Button
              fullRounded={false}
              variant="cancel"
              className="!col-span-1 lg:!w-36 lg:!h-8"
              onClick={onCancelClick}
            >
              بستن
            </Button>
          </div>
        </form>
      </div>
    </Modal>
  );
}
