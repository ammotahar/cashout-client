"use client";

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import Image from "next/image";
import Skeleton from "@mui/material/Skeleton";
import Menu from "@mui/material/Menu";
import MenuList from "@mui/material/MenuList";
import MenuItem from "@mui/material/MenuItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import { ExpandMore } from "@mui/icons-material";
import Button from "./button";
import { getProfileApi } from "@/services/users";
import ProfileIcon from "assets/icons/profile.svg";
import GroupIcon from "assets/icons/group.svg";
import SettingsIcon from "assets/icons/settings.svg";
import LogoutIcon from "assets/icons/logout.svg";

export default function Header() {
  const [username, setUsername] = useState("");
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = Boolean(anchorEl);

  const router = useRouter();

  async function getUserName() {
    const res = await getProfileApi();

    if (res) {
      if (res?.data?.first_name && res?.data?.last_name)
        setUsername(`${res?.data?.first_name} ${res?.data?.last_name}`);
    } else {
      setUsername(res?.data?.user_name);
    }
  }

  useEffect(() => {
    getUserName();
  }, []);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="bg-white flex items-center justify-between px-4 py-2 shadow-header sticky top-0 z-50">
      <Button
        variant="text"
        className="!flex !items-center !gap-2"
        onClick={handleClick}
      >
        <Image src={ProfileIcon} alt="profile" className="w-8 h-8" />

        {username && username.length === 0 ? (
          <Skeleton variant="text" className="!text-sm !w-32" />
        ) : (
          <span className="text-black text-sm">{username}</span>
        )}

        <ExpandMore className="!text-black !text-xl" />
      </Button>

      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <MenuList>
          <MenuItem
            onClick={() => {
              router.push("/panel/profile");
              handleClose();
            }}
          >
            <ListItemIcon>
              <PersonOutlineIcon />
            </ListItemIcon>
            <ListItemText>مشاهده مشخصات کاربر</ListItemText>
          </MenuItem>
        </MenuList>
      </Menu>

      <div className="flex items-center gap-2">
        {/* <Image src={GroupIcon} alt="profile" className="w-8" />
        <Image src={SettingsIcon} alt="settings" className="w-8" /> */}
        <Button
          variant="icon"
          onClick={() => {
            window.localStorage.clear();
            router.replace("/authentication/login");
          }}
        >
          <Image src={LogoutIcon} alt="logout" className="w-8" />
        </Button>
      </div>
    </div>
  );
}
