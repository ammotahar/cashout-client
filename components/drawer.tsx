import { MouseEventHandler } from "react";
import MuiDrawer from "@mui/material/Drawer";
import Button from "./button";
import Sidebar from "./sidebar";
import MenuIcon from "@/assets/icons/blue/menu";

export default function Drawer({
  open,
  onMenuClick,
  onClose,
}: {
  open: boolean;
  onMenuClick?: MouseEventHandler<HTMLButtonElement>;
  onClose?: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
}) {
  return (
    <>
      <Button onClick={onMenuClick} variant="icon">
        <MenuIcon />
      </Button>

      <MuiDrawer
        open={open}
        onClose={onClose}
        PaperProps={{ className: "!rounded-l-2xl" }}
      >
        <Sidebar type="drawer" />
      </MuiDrawer>
    </>
  );
}
