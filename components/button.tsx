import { MouseEventHandler } from "react";
import MuiButton from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";

export default function Button({
  children,
  variant = "primary",
  className,
  onClick,
  type,
  loading = false,
  fullRounded = true,
  padding,
}: {
  children: React.ReactNode;
  variant?:
    | "primary"
    | "secondary"
    | "text"
    | "icon"
    | "cancel"
    | "success"
    | "delete";
  className?: string;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  type?: "submit";
  loading?: boolean;
  fullRounded?: boolean;
  padding?: string;
}) {
  switch (variant) {
    case "primary":
      return (
        <MuiButton
          variant="contained"
          className={`!shadow-none ${
            fullRounded ? "!rounded-full" : "!rounded"
          } ${padding || "!px-8"} ${className}`}
          onClick={onClick}
          type={type}
          disabled={loading}
        >
          {loading ? <CircularProgress size={25} /> : children}
        </MuiButton>
      );

    case "secondary":
      return (
        <MuiButton
          variant="outlined"
          className={`${className} !text-primary`}
          onClick={onClick}
          type={type}
          disabled={loading}
        >
          {loading ? <CircularProgress size={25} /> : children}
        </MuiButton>
      );

    case "text":
      return (
        <MuiButton
          className={`${className}`}
          onClick={onClick}
          type={type}
          disabled={loading}
        >
          {loading ? <CircularProgress size={25} /> : children}
        </MuiButton>
      );

    case "icon":
      return (
        <MuiButton
          className={`${className} !p-0 !min-w-0`}
          onClick={onClick}
          type={type}
          disabled={loading}
        >
          {loading ? <CircularProgress size={25} /> : children}
        </MuiButton>
      );

    case "cancel":
      return (
        <MuiButton
          variant="outlined"
          className={`${className}`}
          onClick={onClick}
          type={type}
          disabled={loading}
          color="error"
        >
          {loading ? <CircularProgress size={25} /> : children}
        </MuiButton>
      );

    case "success":
      return (
        <MuiButton
          variant="contained"
          className={`!shadow-none !rounded-lg !px-2 !bg-success ${className}`}
          onClick={onClick}
          type={type}
          disabled={loading}
        >
          {loading ? <CircularProgress size={25} /> : children}
        </MuiButton>
      );

    case "delete":
      return (
        <MuiButton
          variant="contained"
          className={`!shadow-none !rounded-lg !bg-error !px-2 ${className}`}
          onClick={onClick}
          type={type}
          disabled={loading}
        >
          {loading ? <CircularProgress size={25} /> : children}
        </MuiButton>
      );

    default:
      break;
  }
}
