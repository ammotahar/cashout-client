import { rialSeparator } from "@/utils/rialSeperator";

export default function PriceText({
  price,
  containerClassName,
  priceFontSize = "text-2xl",
  unitFontSize = "text-xs",
  priceTextColor = "text-white",
  unitTextColor = "text-white",
}: {
  price: string;
  containerClassName?: string;
  priceFontSize?: string;
  unitFontSize?: string;
  priceTextColor?: string;
  unitTextColor?: string;
}) {
  return (
    <div className={`flex items-end ${containerClassName}`}>
      <span className={`${priceFontSize} ${priceTextColor} ml-1`}>
        {rialSeparator(price)}
      </span>
      <span className={`${unitFontSize} ${unitTextColor} mb-1`}>ریال</span>
    </div>
  );
}
