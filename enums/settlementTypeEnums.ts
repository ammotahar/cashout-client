export const SINGLE_SETTLEMENT = 0;
export const GROUP_SETTLEMENT = 1;

export const SINGLE_SETTLEMENT_TITLE = "تسویه تکی";
export const GROUP_SETTLEMENT_TITLE = "تسویه گروهی";
