export const FAILED_SETTLEMENT = 0;
export const SUCCESSFULL_SETTLEMENT = 1;
export const DELAY_PENDING_SETTLEMENT = 2;
export const QUEUE_PENDING_SETTLEMENT = 3;
export const USER_PENDING_SETTLEMENT = 4;

export const FAILED_SETTLEMENT_TITLE = "تسویه نشده";
export const SUCCESSFULL_SETTLEMENT_TITLE = "تسویه شده";
export const DELAY_PENDING_SETTLEMENT_TITLE = "در انتظار تسویه";
export const QUEUE_PENDING_SETTLEMENT_TITLE = "در حال بررسی (صف)";
export const USER_PENDING_SETTLEMENT_TITLE = "در انتظار بررسی کاربر";
