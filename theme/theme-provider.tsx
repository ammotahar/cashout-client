"use client";

import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { prefixer } from "stylis";
import { ThemeProvider, createTheme } from "@mui/material/styles";
// import { faIR } from "@mui/x-date-pickers/locales";

export const theme = createTheme(
  {
    direction: "rtl",
    typography: {
      fontFamily: "vazirFont",
    },
    palette: {
      primary: {
        main: "#4640E1",
      },
      secondary: {
        main: "#FFA827",
      },
    },
  }
  // faIR
);

// Create rtl cache
const cacheRtl = createCache({
  key: "muirtl",
  stylisPlugins: [prefixer, rtlPlugin],
});

export default function CThemeProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <ThemeProvider theme={theme}>
      <CacheProvider value={cacheRtl}>{children}</CacheProvider>
    </ThemeProvider>
  );
}
