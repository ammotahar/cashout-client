## [2.3.5](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.3.4...v2.3.5) (2024-01-16)


### Bug Fixes

* changetag2 ([5431642](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/5431642061e9e0943a2283d529646a9f8a228781))

## [2.3.4](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.3.3...v2.3.4) (2024-01-16)


### Bug Fixes

* update role and permission in edi user page ([01eb5ca](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/01eb5ca6a18133a3a465ed47b813af7ccbb5e3c3))

## [2.3.3](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.3.2...v2.3.3) (2024-01-10)


### Bug Fixes

* fix export excel and edit user password required field bug ([db89bf7](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/db89bf7d83e307a1511d412ad5cc4c6ca141674d))

## [2.3.2](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.3.1...v2.3.2) (2024-01-09)


### Bug Fixes

* update role and permission in create user page ([f00630b](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/f00630b7df149dd97ff23fb179c75c12bc608dfe))

## [2.3.1](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.3.0...v2.3.1) (2024-01-08)


### Bug Fixes

* fix Dockerfile.develop ([0f7a709](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/0f7a709a0f66c67c82dfc3c83aabf30073fe9870))

## [2.2.10](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.9...v2.2.10) (2023-12-06)


### Bug Fixes

* fix edit user page crash ([0765d05](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/0765d0559a32fa45e44598382d43aab9984b68e6))

## [2.2.9](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.8...v2.2.9) (2023-12-04)


### Bug Fixes

* fixed stage configs ([86ab2a0](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/86ab2a05baa1019a24481d5aa0c00f3b479576a0))

## [2.2.8](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.7...v2.2.8) (2023-12-04)


### Bug Fixes

* added stage environment configs ([e1510bf](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/e1510bfbbf0a456ba5ffe274c8c635e04976827d))
* fix 403 status logout error handling ([5a8e83c](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/5a8e83cd7edf4e9ea5edb1927bbf5275dae6cc96))

## [2.2.7](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.6...v2.2.7) (2023-12-02)


### Bug Fixes

* add filename modal to select excel file component ([b4dadd8](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/b4dadd8eca144fd7c944e7b706ee334c4ab252d1))

## [2.2.6](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.5...v2.2.6) (2023-12-02)


### Bug Fixes

* fixed group settlement bugs ([0b5d252](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/0b5d25249f6ecd3175dc9aea61deafdbbbe3f6a0))

## [2.2.5](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.4...v2.2.5) (2023-11-28)


### Bug Fixes

* add max amount satna and paya in create user form ([4019b8a](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/4019b8a57c93a9d15554c03ffbcecab6419fdb7f))

## [2.2.4](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.3...v2.2.4) (2023-11-28)


### Bug Fixes

* fix some bugs ([822efe9](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/822efe9a2da7bf68f11f5618861737fe05ca2a0a))
* fixed open select excel input twice in chrome =| ([9d059f4](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/9d059f47836cfd9e8f9afddd2517261359443556))

## [2.2.3](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.2...v2.2.3) (2023-11-27)


### Bug Fixes

* fix a little design bug ([2f84fbc](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/2f84fbc042bb110221b6a6942ba7e0a84ce25b90))
* fix a little design bug ([b2247aa](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/b2247aae17d1040865f0626b4633b16746bac4be))
* fix some bugs ([d93f51f](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/d93f51fbd64ab6faa55195605f4a414eb2a5aa10))
* remove search field from pages ([c61776a](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/c61776aba72e40a117793966afb26f47d4a7134a))

## [2.2.2](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.1...v2.2.2) (2023-11-27)


### Bug Fixes

* fixed excel review modal crash ([78b15e8](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/78b15e827331f552d0095c3f419df44b08ac7006))

## [2.2.1](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.2.0...v2.2.1) (2023-11-26)


### Bug Fixes

* error count in upload excel ([a9b4600](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/a9b4600a41954fd38bb3a27707b526bd94579db8))

# [2.2.0](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.1.4...v2.2.0) (2023-11-26)


### Features

* added wage calculation in group settlement before do settlement ([c70d09b](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/c70d09b0b226c3227002aa2149e57ef77a0e39aa))

## [2.1.4](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.1.3...v2.1.4) (2023-11-26)


### Bug Fixes

* added getIbanListApi instead of getSettlementListApi for people ([1108e19](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/1108e19f5051d67414203d972bacbac6fe421d37))

## [2.1.3](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.1.2...v2.1.3) (2023-11-25)


### Bug Fixes

* fixed little bugs ([020e6d7](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/020e6d7e151399905c6716eec184bda060f813e5))

## [2.1.2](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.1.1...v2.1.2) (2023-11-25)


### Bug Fixes

* fixed little bugs ([553887d](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/553887dd2654367868e9413035fa4eb0fc70abda))

## [2.1.1](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.1.0...v2.1.1) (2023-11-25)


### Bug Fixes

* a little design bug ([e873eca](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/e873eca1d39ec8f450c2107880dc52c9cc2b92cf))
* finished dashboard chart ([f15655a](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/f15655a2ae1d301581a059710f5c83d25f3cb387))
* fix a little design bug ([df86704](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/df867045bbefc2c429174731de0c2f2f95e0a5e7))

# [2.1.0](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.0.6...v2.1.0) (2023-11-21)


### Features

* added account management pages and functionality ([c2c9842](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/c2c98426254a3f53eb88095be2cf8eff9c895743))

## [2.0.6](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.0.5...v2.0.6) (2023-11-21)


### Bug Fixes

* added resend otp functionality ([482d6db](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/482d6db370c111c65dee2e44975a1a3abe4e6e43))
* fixed balance box in dashboard page design bug ([4c2b344](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/4c2b3447227647377bc7d1091184a73f72a1d6e7))

## [2.0.5](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.0.4...v2.0.5) (2023-11-19)


### Bug Fixes

* added download settlement list excel functionality ([e442aaa](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/e442aaa0eb063c337127bc85b7478859d24524b9))
* added image in auth layout ([610649c](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/610649c56214504badc49a3ca48ee3ed5030254a))
* little design bug ([5297c28](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/5297c28ea34e8a9abfdc14eb92b33ef39d2bf145))
* working on balance box in dashboard ([f314cb8](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/f314cb88aa6dd8a71497efec91f3e139b0e81508))

## [2.0.4](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.0.3...v2.0.4) (2023-11-19)


### Bug Fixes

* change versioning commit message ([4941267](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/49412675d7b8fe597a8fbbefc0d239547c5a7141))

## [2.0.3](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.0.2...v2.0.3) (2023-11-19)


### Bug Fixes

* change versioning commit message ([fadc1d6](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/fadc1d64bd72f1c7c75d1995b874ec9e026cba21))

## [2.0.2](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.0.1...v2.0.2) (2023-11-19)


### Bug Fixes

* exclude versioning pipeline from deployment ([2a55587](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/2a555875eca74ea6b5e90986f015bf6e7e746fbc))

## [2.0.1](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v2.0.0...v2.0.1) (2023-11-15)

# [2.0.0](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.14...v2.0.0) (2023-11-15)

## [1.0.14](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.13...v1.0.14) (2023-11-15)

## [1.0.13](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.12...v1.0.13) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([a9bcce2](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/a9bcce2fdf190f9ec053a97eda1efd7e6645cb1e))

## [1.0.12](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.11...v1.0.12) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([21d6317](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/21d6317f7ae24606f977c23010992ef4b2e9cf54))

## [1.0.11](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.10...v1.0.11) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([b8f3629](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/b8f362990d5af77b0698beec9dcc162b25d6f64e))

## [1.0.10](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.9...v1.0.10) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([b0a6b23](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/b0a6b23e4caaa4543cb7f99e57984b51d9aff3bf))

## [1.0.9](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.8...v1.0.9) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([032c1a2](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/032c1a26ec486de484933a2852cfbc3d6a6d0ded))

## [1.0.8](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.7...v1.0.8) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([d6e13b9](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/d6e13b9899f9a12132d92e4aa2c5c5b2cdca12c9))

## [1.0.7](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.6...v1.0.7) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([cbbd76d](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/cbbd76d6d94b51d89c9019be5184fa02bab9bc59))

## [1.0.6](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.5...v1.0.6) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([e8ec0ed](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/e8ec0edb88f747949d11eeb836d432080cea9e91))

## [1.0.5](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.4...v1.0.5) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([102ac42](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/102ac42dd9c8127d305f426ba7b4cc48b9103dc5))
* fix versioning pipeline ([59f6d67](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/59f6d67dd60d526146829466dbb5f88a78c81e2c))

## [1.0.4](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.3...v1.0.4) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([2ebf0c8](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/2ebf0c8157c7c179e13bf30542647081bd23385c))

## [1.0.3](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.2...v1.0.3) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([24e484e](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/24e484efc9a83a5313dfa5aeb7ef2660519f67a7))

## [1.0.2](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.1...v1.0.2) (2023-11-15)


### Bug Fixes

* fix versioning pipeline ([96ce262](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/96ce262d7f0ceaeb612c1b797ddc111cc60bfaee))
* fix versioning pipeline ([270220d](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/270220ddcfa490d0227068f0595f4ca0b273f243))

## [1.0.1](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/compare/v1.0.0...v1.0.1) (2023-11-15)


### Bug Fixes

* added pipeline for versioning ([f9d95a3](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/f9d95a3a4a8ef872508436634c58ebb7e6852fb9))
* fix versioning pipeline bug ([ad8098c](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/ad8098ce68069d5e211579b26db32db5b384dfb4))

# 1.0.0 (2023-11-15)


### Bug Fixes

* semver ([4e83b80](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/4e83b80805ac65ebad8fbdb617e3ef2f7e80fdba))


### Features

* semver ([0c9bd69](https://git.eniac-tech.com/kamal.habibi/cashoutv2-front-merchant/commit/0c9bd696cc400f5f555ac4829578ef9c214d5eff))
