import "./globals.css";
import { ToastContainer } from "react-toastify";
import CThemeProvider from "theme/theme-provider";
import ContextProvider from "@/context/provider";
import "react-toastify/dist/ReactToastify.css";

export const metadata = {
  title: "CashoutV2 Merchant",
  description: "CashoutV2",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="fa">
      <body>
        <ContextProvider>
          <CThemeProvider>{children}</CThemeProvider>
          <ToastContainer rtl />
        </ContextProvider>
      </body>
    </html>
  );
}
