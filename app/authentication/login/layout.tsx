import Divider from "@/components/divider";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div className="bg-white m-4 border-gradient rounded-lg">
      <div className="p-2 xl:p-8 xl:h-full">
        <div>
          <span className="text-title xl:text-3xl">ورود</span>
        </div>

        <Divider className="my-4" />

        {children}
      </div>
    </div>
  );
}
