"use client";

import { useRouter } from "next/navigation";
import TextField from "@/components/textField";
import Button from "@/components/button";
import { MobileIcon } from "@/assets/icons/black/mobile";

export default function ForgotPasswordPage() {
  const router = useRouter();

  return (
    <>
      <div className="mt-4">
        <div>
          <span className="text-subtitle text-xs">
            لطفا شماره موبایل خود را وارد کنید.
          </span>
        </div>

        <TextField
          title="شماره موبایل"
          icon={<MobileIcon />}
          className="mb-4 mt-8"
          placeholder="09"
        />
      </div>

      <Button
        className="!self-end !mt-10 !mb-4"
        onClick={() => router.push("/authentication/forgot-password/enter-otp")}
      >
        مرحله بعد
      </Button>
    </>
  );
}
