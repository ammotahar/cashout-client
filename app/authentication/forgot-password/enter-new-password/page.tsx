"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import TextField from "@/components/textField";
import Button from "@/components/button";
import SuccessModal from "@/components/successModal";
import { PasswordIcon } from "@/assets/icons/black/password";

export default function EnterNewPasswordPage() {
  const [open, setOpen] = useState(false);

  const router = useRouter();

  return (
    <>
      <div className="mt-4">
        <div>
          <span className="text-subtitle text-xs">
            لطفا رمز عبور جدید خود را وارد کنید.
          </span>
        </div>

        <TextField
          title="ورود رمز عبور"
          icon={<PasswordIcon />}
          className="my-4"
          type="password"
        />

        <div className="mt-8">
          <span className="text-subtitle text-xs">
            لطفا رمز عبور جدید خود را تکرار کنید.
          </span>
        </div>

        <TextField
          title="تکرار رمز عبور"
          icon={<PasswordIcon />}
          className="my-4"
          type="password"
        />
      </div>

      <Button className="!self-end !mt-10 !mb-4" onClick={() => setOpen(true)}>
        تایید
      </Button>

      <SuccessModal
        open={open}
        title="فراموشی رمز عبور"
        onClose={() => setOpen(false)}
        message="رمز عبور شما با موفقیت تغییر کرد!"
      />
    </>
  );
}
