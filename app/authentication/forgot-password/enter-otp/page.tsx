"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";
import Button from "@/components/button";
import Otp from "@/components/otp";
import { EditIcon } from "@/assets/icons/green/edit";

export default function EnterOtpPage() {
  const [showTimer, setShowTimer] = useState(false);
  const [otpCode, setOtpCode] = useState("");

  const router = useRouter();

  return (
    <>
      <div className="flex flex-col mt-4">
        <div>
          <span className="text-subtitle text-xs">
            کد یکبار مصرف به شماره موبایل ارسال شده است.
          </span>
        </div>

        <Button variant="text" className="self-end">
          <Link href="/authentication/forgot-password">
            <div className="flex items-center">
              <span className="text-xs text-green">ویرایش شماره موبایل</span>

              <div className="flex items-center justify-center bg-softGreen h-5 w-5 border border-green rounded mr-2">
                <EditIcon />
              </div>
            </div>
          </Link>
        </Button>

        <Otp
          onResendOtp={() => {}}
          showTimer={showTimer}
          setShowTimer={setShowTimer}
          otpCode={otpCode}
          setOtpCode={setOtpCode}
        />
      </div>

      <Button
        className="!self-end !mt-10 !mb-4"
        onClick={() =>
          router.push("/authentication/forgot-password/enter-new-password")
        }
      >
        تایید
      </Button>
    </>
  );
}
