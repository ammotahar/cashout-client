import Image from "next/image";
import EniacLogo from "assets/images/eniac-text.svg";
import AuthImage from "assets/images/auth-image.svg";
import packageJson from "package.json";

export default function AuthenticationLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="bg-login bg-cover h-screen flex flex-col">
      <Image src={EniacLogo} alt="eniac-text" className="self-center mt-4" />

      <div className="h-full w-full flex flex-col items-center justify-between lg:flex-row">
        <div className="my-auto md:w-4/5 md:m-auto lg:w-1/2 lg:m-auto xl:w-1/3 xl:mx-10">
          {children}
        </div>

        <div className="max-w-form m-10 hidden lg:block">
          <Image src={AuthImage} alt="auth" />
        </div>
      </div>

      <div className="text-center mb-4 text-black">
        <span>نسخه {packageJson.version}</span>
      </div>
    </div>
  );
}
