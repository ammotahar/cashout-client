"use client";

import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import PanelBox from "@/components/panelBox";
import MaterialTextField from "@/components/materialTextField";
import Button from "@/components/button";
import MessageModal from "@/components/messageModal";
import { rialSeparator } from "@/utils/rialSeperator";
import { increaseBalanceViaIpgApi } from "@/services/charge";
import { getBalanceAndMinDepositApi } from "@/services/company";

interface IFormInput {
  amount: string;
  description?: string;
}

export default function PortalPage() {
  const [loading, setLoading] = useState(false);
  const [successOpen, setSuccessOpen] = useState(false);
  const [referenceId, setReferenceId] = useState("");
  const [balance, setBalance] = useState(0);

  const router = useRouter();

  const {
    handleSubmit,
    reset,
    control,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      amount: "",
      description: "",
    },
  });

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const res = await increaseBalanceViaIpgApi({
      amount: parseInt(data.amount),
      description: data.description,
      call_back_url: `${process.env.SERVER_BASE_URL}panel/charge/report` || "",
    });

    if (res) {
      if (res?.data?.reference_id > 0) {
        setReferenceId(res?.data?.reference_id);
        window.open(res?.data?.ipg_link, "_blank");
      }
    }

    setLoading(false);

    reset();
  };

  async function getBalance() {
    setLoading(true);

    const res = await getBalanceAndMinDepositApi();

    if (res) {
      setBalance(res?.data?.balance);
    }

    setLoading(false);
  }

  useEffect(() => {
    getBalance();
  }, []);

  return (
    <PanelBox className="md:max-w-form">
      <div className="text-center text-black text-sm shadow-box py-4 rounded">
        <span>
          موجودی کیف پول شما: {rialSeparator(balance.toString())} ریال
        </span>
      </div>

      <div className="flex flex-col items-center justify-center text-center text-black text-sm w-full relative my-10">
        <div className="absolute px-4 bg-white">
          <span>شارژ کیف پول</span>
        </div>

        <hr className="w-full border-dashed border-divider" />
      </div>

      <form
        className="flex flex-col w-full gap-4 2xl:px-20 2xl:py-4"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Controller
          name="amount"
          control={control}
          rules={{
            required: "مبلغ را وارد نمایید.",
            pattern: {
              value: /^\d+$/gi,
              message: "مبلغ وارد شده صحیح نیست",
            },
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="مبلغ *"
              startAdornments="ریال"
              isFormatNumber
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.amount}
              helperText={errors?.amount?.message}
            />
          )}
        />

        <Controller
          name="description"
          control={control}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="توضیحات"
              className="!col-span-2"
              onChange={onChange}
              value={value}
              multiline
            />
          )}
        />

        <div className="grid grid-cols-2 gap-2 mt-4">
          <Button
            fullRounded={false}
            className="!py-2 !text-xs !col-span-1"
            type="submit"
            loading={loading}
          >
            شارژ کیف پول
          </Button>
          <Button
            variant="cancel"
            className="!py-2 !text-xs !col-span-1"
            onClick={() => router.push("/panel")}
          >
            انصراف
          </Button>
        </div>
      </form>

      <MessageModal
        open={successOpen}
        onClose={() => setSuccessOpen(false)}
        variant="success"
        title="ارسال درخواست افزایش موجودی کیف پول"
        message="پس از بررسی فیش بانکی توسط ادمین سیستم،
        در صورت تایید ادمین، شارژ موجودی کیف پول انجام خواهد شد."
        content={
          <div className="text-center mt-2">
            <span className="text-sm text-primary">
              کد پیگیری: {referenceId}
            </span>
          </div>
        }
        onSubmitClick={() => setSuccessOpen(false)}
      />
    </PanelBox>
  );
}
