import ChargeMenu from "./components/menu";

export default function CheckoutLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div>
      <ChargeMenu />

      <div className="mt-4 flex flex-col items-center">{children}</div>
    </div>
  );
}
