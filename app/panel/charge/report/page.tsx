"use client";

import { useState, useEffect, useMemo } from "react";
import Image from "next/image";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import PanelBox from "@/components/panelBox";
import SearchField from "@/components/searchField";
import Button from "@/components/button";
import Filter from "./components/filter";
import Loading from "./components/loading";
import {
  getChargeBalanceReportListApi,
  getChargeBalanceListXlsxApi,
} from "@/services/charge";
import Pagination from "@/components/pagination";
import ReportItem from "./components/reportItem";
import FilterIcon from "assets/icons/filter.svg";
import ExportIcon from "assets/icons/export.svg";
import { ChargeBalanceReportListType } from "@/services/charge/types";

export default function ReportPage() {
  const [filterOpen, setFilterOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<ChargeBalanceReportListType[]>([]);
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");
  const [bankName, setBankName] = useState("");
  const [fromDate, setFromDate] = useState<Date | undefined>();
  const [toDate, setToDate] = useState<Date | undefined>();
  const [trackNumber, setTrackNumber] = useState("");
  const [amount, setAmount] = useState("");
  const [fromIban, setFromIban] = useState("");
  const [toIban, setToIban] = useState("");
  const [chargeStatus, setChargeStatus] = useState<number | string>("");
  const [chargeType, setChargeType] = useState<number | string>("");
  const [excelDownloadLoading, setExcelDownloadLoading] = useState(false);

  async function getChargeBalanceReportList(
    page?: number,
    search?: string,
    amount?: number,
    bankName?: string,
    fromDate?: string,
    toDate?: string,
    fromIban?: string,
    toIban?: string,
    status?: number,
    trackNumber?: number,
    type?: number
  ) {
    setLoading(true);

    const res = await getChargeBalanceReportListApi({
      per_page: 5,
      page_number: page,
      search,
      ...(amount && { amount }),
      bank_name: bankName,
      from_date_insert_date: fromDate,
      to_date_insert_date: toDate,
      from_iban: fromIban,
      to_iban: toIban,
      status,
      ...(trackNumber && { track_number: trackNumber }),
      type,
    });

    if (res) {
      setData(res?.data?.data);
      setCount(res?.data?.count);
    }

    setLoading(false);
  }

  function deleteFilters() {
    setPage(1);
    setBankName("");
    setFromDate(undefined);
    setToDate(undefined);
    setTrackNumber("");
    setAmount("");
    setFromIban("");
    setToIban("");
    setChargeStatus("");
    setChargeType("");
    getChargeBalanceReportList(
      1,
      "",
      undefined,
      "",
      "",
      "",
      "",
      "",
      undefined,
      undefined,
      undefined
    );
  }

  function pageHandler(event: any, page: number) {
    setPage(page);
    getChargeBalanceReportList(
      page,
      search,
      parseInt(amount),
      bankName,
      typeof fromDate !== "undefined" ? fromDate.toISOString() : "",
      typeof toDate !== "undefined" ? toDate.toISOString() : "",
      fromIban,
      toIban,
      typeof chargeStatus !== "string" ? chargeStatus : undefined,
      parseInt(trackNumber),
      typeof chargeType !== "string" ? chargeType : undefined
    );
  }

  useEffect(() => {
    getChargeBalanceReportList(
      1,
      "",
      undefined,
      "",
      "",
      "",
      "",
      "",
      undefined,
      undefined,
      undefined
    );
  }, []);

  useEffect(() => {
    setPage(1);
    getChargeBalanceReportList(
      1,
      search,
      parseInt(amount),
      bankName,
      typeof fromDate !== "undefined" ? fromDate.toISOString() : "",
      typeof toDate !== "undefined" ? toDate.toISOString() : "",
      fromIban,
      toIban,
      typeof chargeStatus !== "string" ? chargeStatus : undefined,
      parseInt(trackNumber),
      typeof chargeType !== "string" ? chargeType : undefined
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  async function getChargeBalanceListXlsx() {
    setExcelDownloadLoading(true);

    const res = await getChargeBalanceListXlsxApi();
    const blob = new Blob([res], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = "charge_report.xlsx";
    a.click();
    setExcelDownloadLoading(false);
  }

  return (
    <PanelBox>
      <div className="w-full flex flex-col items-end gap-4 lg:flex-row lg:justify-between lg:items-center mb-4">
        <div className="flex items-center gap-4 w-full lg:w-1/2">
          {/* <SearchField
            placeholder="شماره کارت / حساب / شبا مبدا"
            onChange={(value) => setSearch(value)}
          /> */}

          <Button
            className="!flex !items-center !justify-center lg:!justify-between lg:!px-2 !bg-primary !rounded-md !min-w-10 !min-h-10 lg:!w-32"
            variant="text"
            onClick={() => setFilterOpen(!filterOpen)}
          >
            <div className="flex items-center gap-1">
              <Image src={FilterIcon} alt="filter" />
              <span className="hidden lg:block text-xs text-black">فیلتر</span>
            </div>

            <div className="hidden lg:block">
              {filterOpen ? (
                <ExpandLess className="!text-xl !text-black" />
              ) : (
                <ExpandMore className="!text-xl !text-black" />
              )}
            </div>
          </Button>
        </div>

        <Button
          fullRounded={false}
          className="!col-span-1 !flex !items-center !justify-center !gap-2 !min-h-10"
          padding="!px-2"
          onClick={() => getChargeBalanceListXlsx()}
          loading={excelDownloadLoading}
        >
          <span className="text-xs">خروجی اکسل</span>
          <Image src={ExportIcon} alt="export" />
        </Button>
      </div>

      <Filter
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        onBankNameChange={(event) => setBankName(event.target.value)}
        bankName={bankName}
        fromDateChange={(value) => setFromDate(value)}
        fromDate={fromDate}
        toDateChange={(value) => setToDate(value)}
        toDate={toDate}
        onTrackNumberChange={(event) => setTrackNumber(event.target.value)}
        trackNumber={trackNumber}
        onAmountChange={(event) => setAmount(event.target.value)}
        amount={amount}
        onFromIbanChange={(event) => setFromIban(event.target.value)}
        fromIban={fromIban}
        onToIbanChange={(event) => setToIban(event.target.value)}
        toIban={toIban}
        onChargeStatusChange={(event) => setChargeStatus(event.target.value)}
        chargeStatus={chargeStatus}
        onChargeTypeChange={(event) => setChargeType(event.target.value)}
        chargeType={chargeType}
        onSubmitClick={() => {
          setPage(1);
          getChargeBalanceReportList(
            1,
            search,
            parseInt(amount),
            bankName,
            typeof fromDate !== "undefined" ? fromDate.toISOString() : "",
            typeof toDate !== "undefined" ? toDate.toISOString() : "",
            fromIban,
            toIban,
            typeof chargeStatus !== "string" ? chargeStatus : undefined,
            parseInt(trackNumber),
            typeof chargeType !== "string" ? chargeType : undefined
          );
        }}
        onDeleteClick={deleteFilters}
      />

      {loading ? (
        <Loading />
      ) : (
        <div className="mt-4 flex flex-col gap-6">
          {data.length > 0 ? (
            data.map((item) => <ReportItem key={item.id} data={item} />)
          ) : (
            <div className="w-full text-center">
              <span>موردی یافت نشد.</span>
            </div>
          )}
        </div>
      )}

      <div className="mt-4 flex justify-end">
        <Pagination
          page={page}
          count={Math.ceil(count / 5)}
          onChange={pageHandler}
        />
      </div>
    </PanelBox>
  );
}
