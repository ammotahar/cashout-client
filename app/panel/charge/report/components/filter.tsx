import { ChangeEventHandler, MouseEventHandler, ReactNode } from "react";
import { SelectChangeEvent } from "@mui/material";
import FilterContainer from "@/components/filterContainer";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";
import Datepicker from "@/components/datepicker";
import {
  NOT_CONFIRMED,
  NOT_CONFIRMED_TITLE,
  CONFIRMED,
  CONFIRMED_TITLE,
  AWAITING_CONFIRMATION,
  AWAITING_CONFIRMATION_TITLE,
  SENDING_TO_IPG,
  SENDING_TO_IPG_TITLE,
  TRANSFER_TO_IPG_FAILED,
  TRANSFER_TO_IPG_FAILED_TITLE,
} from "@/enums/chargeBalanceStatusEnums";
import {
  MANUAL,
  MANUAL_TITLE,
  IPG,
  IPG_TITLE,
} from "@/enums/chargeBalanceTypeEnums";

export default function Filter({
  open,
  onClose,
  onBankNameChange,
  bankName,
  fromDateChange,
  fromDate,
  toDateChange,
  toDate,
  onTrackNumberChange,
  trackNumber,
  onAmountChange,
  amount,
  onFromIbanChange,
  fromIban,
  onToIbanChange,
  toIban,
  onChargeStatusChange,
  chargeStatus,
  onChargeTypeChange,
  chargeType,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onBankNameChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  bankName?: any;
  fromDateChange?: (value: any) => void;
  fromDate?: any;
  toDateChange?: (value: any) => void;
  toDate?: any;
  onTrackNumberChange?: ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  >;
  trackNumber?: any;
  onAmountChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  amount?: any;
  onFromIbanChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  fromIban?: any;
  onToIbanChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  toIban?: any;
  onChargeStatusChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  chargeStatus?: any;
  onChargeTypeChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  chargeType?: any;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <FilterContainer
      open={open}
      onClose={onClose}
      onSubmitClick={onSubmitClick}
      onDeleteClick={onDeleteClick}
    >
      <div className="grid grid-cols-1 lg:grid-cols-5 gap-4">
        <MaterialTextField
          label="نام بانک"
          onChange={onBankNameChange}
          value={bankName}
          className="!col-span-1 !bg-white"
        />

        <Datepicker
          value={fromDate}
          onChange={(value) => fromDateChange && fromDateChange(value)}
          label="از تاریخ ثبت"
          containerClassNames="!col-span-1 !bg-white"
        />

        <Datepicker
          value={toDate}
          onChange={(value) => toDateChange && toDateChange(value)}
          label="تا تاریخ ثبت"
          containerClassNames="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="کد پیگیری"
          onChange={onTrackNumberChange}
          value={trackNumber}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="مبلغ"
          startAdornments="ریال"
          onChange={onAmountChange}
          value={amount}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="شماره حساب / شبا مبدا"
          startAdornments="IR"
          onChange={onFromIbanChange}
          value={fromIban}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="شماره حساب / شبا مقصد"
          startAdornments="IR"
          onChange={onToIbanChange}
          value={toIban}
          className="!col-span-1 !bg-white"
        />

        <Select
          label="وضعیت"
          onChange={onChargeStatusChange}
          value={chargeStatus}
          className="!col-span-1 !bg-white"
          items={[
            {
              id: NOT_CONFIRMED,
              name: NOT_CONFIRMED_TITLE,
            },
            {
              id: CONFIRMED,
              name: CONFIRMED_TITLE,
            },
            {
              id: AWAITING_CONFIRMATION,
              name: AWAITING_CONFIRMATION_TITLE,
            },
            {
              id: SENDING_TO_IPG,
              name: SENDING_TO_IPG_TITLE,
            },
            {
              id: TRANSFER_TO_IPG_FAILED,
              name: TRANSFER_TO_IPG_FAILED_TITLE,
            },
          ]}
        />

        <Select
          label="نوع"
          onChange={onChargeTypeChange}
          value={chargeType}
          className="!col-span-1 !bg-white"
          items={[
            {
              id: MANUAL,
              name: MANUAL_TITLE,
            },
            {
              id: IPG,
              name: IPG_TITLE,
            },
          ]}
        />
      </div>
    </FilterContainer>
  );
}
