"use client";

import { useState, useEffect, useCallback, memo } from "react";
import Collapse from "@mui/material/Collapse";
import { Skeleton } from "@mui/material";
import MaterialTextField from "@/components/materialTextField";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { getChargeBalanceStatusTitle } from "@/utils/getChargeBalanceStatusTitle";
import { rialSeparator } from "@/utils/rialSeperator";
import { miladiToJalali } from "@/utils/jalaliConverter";
import { getChargeBalanceReportDetailsApi } from "@/services/charge";
import {
  ChargeBalanceReportListType,
  ChargeBalanceReportDetailsType,
} from "@/services/charge/types";

const n = 14;

const Loading = () => {
  return (
    <div className="grid grid-cols-2 gap-4 pb-6 lg:grid-cols-4 lg:ml-20">
      {[...Array(n)].map((item, index) => (
        <Skeleton
          key={index}
          variant="rounded"
          className="!col-span-2 lg:!col-span-1 !h-10"
        />
      ))}
    </div>
  );
};

const ReportItem = memo(function ReportItem({
  data,
}: {
  data: ChargeBalanceReportListType;
}) {
  const [checked, setChecked] = useState(false);
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState<ChargeBalanceReportDetailsType | null>(
    null
  );

  const getChargeBalanceReportDetails = useCallback(async () => {
    setLoading(true);

    const res = await getChargeBalanceReportDetailsApi(data.id);

    if (res) {
      setDetails(res?.data);
    }

    setLoading(false);
  }, [data.id]);

  useEffect(() => {
    if (checked) {
      getChargeBalanceReportDetails();
    }
  }, [checked, getChargeBalanceReportDetails]);

  return (
    <div className="bg-primary rounded p-4 pb-6 relative lg:pb-0">
      <div className="w-full flex flex-col items-center lg:flex-row lg:items-start">
        <div className="grid gird-cols-1 w-full lg:grid-cols-4 lg:gap-4 lg:ml-20">
          <MaterialTextField
            label="مبلغ"
            startAdornments="ریال"
            className="!col-span-1 !bg-white"
            disabled
            value={rialSeparator(data?.amount.toString())}
          />

          <MaterialTextField
            label="تاریخ ثبت"
            className="!col-span-1 !bg-white !hidden lg:!grid"
            value={miladiToJalali(data?.insert_date)}
            disabled
          />

          <MaterialTextField
            label="نام بانک"
            className="!col-span-1 !bg-white !hidden lg:!grid"
            disabled
            value={data?.bank_title || ""}
          />

          <MaterialTextField
            label="وضعیت"
            className="!col-span-1 !bg-white !hidden lg:!grid"
            disabled
            value={getChargeBalanceStatusTitle(data?.status)}
          />
        </div>

        <div
          className="flex items-center gap-2 text-primary cursor-pointer absolute bottom-2 lg:top-6 lg:left-2 lg:bottom-auto"
          onClick={() => setChecked(!checked)}
        >
          <div>
            <span>{checked ? "کمتر" : "بیشتر"}</span>
          </div>

          <div>{checked ? <ExpandLess /> : <ExpandMore />}</div>
        </div>
      </div>

      <Collapse in={checked} className="!mt-4">
        {loading ? (
          <Loading />
        ) : (
          <div className="grid grid-cols-2 gap-4 pb-6 lg:grid-cols-4 lg:ml-20">
            <MaterialTextField
              label="تاریخ ثبت"
              className="!col-span-1 !bg-white lg:!hidden"
              value={miladiToJalali(data?.insert_date)}
              disabled
            />

            <MaterialTextField
              label="تاریخ تایید"
              className="!col-span-1 !bg-white"
              value={miladiToJalali(details?.confirm_date || "")}
              disabled
            />

            <MaterialTextField
              label="نام بانک"
              className="!col-span-2 !bg-white lg:!hidden"
              disabled
              value={data?.bank_title || ""}
            />

            <MaterialTextField
              label="وضعیت"
              className="!col-span-2 !bg-white lg:!hidden"
              disabled
              value={getChargeBalanceStatusTitle(data?.status)}
            />

            <MaterialTextField
              label="طریقه افزایش موجودی"
              className="!col-span-2 !bg-white lg:!col-span-1"
              value={details?.type_title || ""}
              disabled
            />

            <MaterialTextField
              label="کد پیگیری"
              className="!col-span-2 !bg-white lg:!col-span-1"
              value={details?.track_number || ""}
              disabled
            />

            <MaterialTextField
              label="شناسه شخص تایید کننده"
              className="!col-span-2 !bg-white lg:!col-span-1"
              value={details?.confirmer_user_id || ""}
              disabled
            />

            <MaterialTextField
              label="شماره حساب / شبا مبدا"
              className="!col-span-2 !bg-white"
              startAdornments="IR"
              value={details?.from_iban || ""}
              disabled
            />

            <MaterialTextField
              label="شماره حساب / شبا مقصد"
              startAdornments="IR"
              className="!col-span-2 !bg-white"
              value={details?.to_iban || ""}
              disabled
            />

            <MaterialTextField
              label="کد پیگیری فیش بارگذاری شده"
              className="!col-span-2 !bg-white lg:!col-span-1"
              value={details?.track_number || ""}
              disabled
            />

            <MaterialTextField
              label="زمان ثبت با قابلیت انتخاب محدوده"
              className="!col-span-2 !bg-white"
              value={miladiToJalali(details?.insert_date || "")}
              disabled
            />

            <MaterialTextField
              label="شناسه کاربر بارگذاری کننده فیش بانکی"
              className="!col-span-2 !bg-white lg:!col-span-1"
              value={details?.user || ""}
              disabled
            />

            <MaterialTextField
              label="علت عدم تایید"
              className="!col-span-2 !bg-white"
              value={details?.reject_result || ""}
              disabled
              multiline
            />

            <MaterialTextField
              label="توضیحات"
              className="!col-span-2 !bg-white"
              value={details?.description || ""}
              disabled
              multiline
            />
          </div>
        )}
      </Collapse>
    </div>
  );
});

export default ReportItem;
