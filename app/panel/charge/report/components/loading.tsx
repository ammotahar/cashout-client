import { Skeleton } from "@mui/material";

const n = 5;

const Loading = () => {
  return (
    <div className="mt-4 flex flex-col gap-6">
      {[...Array(n)].map((item, index) => (
        <Skeleton key={index} variant="rounded" className="!w-full !h-16" />
      ))}
    </div>
  );
};

export default Loading;
