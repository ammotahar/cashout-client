"use client";

import { usePathname } from "next/navigation";
import Link from "next/link";
import Image from "next/image";
import SingleCheckoutIcon from "assets/icons/single-checkout.svg";
import GroupCheckoutIcon from "assets/icons/group-checkout.svg";
import ListCheckoutIcon from "assets/icons/list.svg";

export default function ChargeMenu() {
  const pathname = usePathname();

  return (
    <div className="flex items-center justify-between gap-4 overflow-auto">
      <Link
        href="/panel/charge/receipt"
        className={`flex items-center justify-center bg-white rounded-lg w-1/3 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/charge/receipt")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={SingleCheckoutIcon}
          alt="single-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/charge/receipt")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          شارژ کیف پول با فیش بانکی
        </span>
      </Link>

      <Link
        href="/panel/charge/portal"
        className={`flex items-center justify-center bg-white rounded-lg w-1/3 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/charge/portal")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={GroupCheckoutIcon}
          alt="group-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/charge/portal")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          شارژ کیف پول با درگاه الکترونیک
        </span>
      </Link>

      <Link
        href="/panel/charge/report"
        className={`flex items-center justify-center bg-white rounded-lg w-1/3 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/charge/report")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image src={ListCheckoutIcon} alt="list-checkout" className="w-4 h-4" />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/charge/report")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          گزارشات واریزی
        </span>
      </Link>
    </div>
  );
}
