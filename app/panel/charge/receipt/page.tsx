"use client";

import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import PanelBox from "@/components/panelBox";
import FormTitle from "@/components/formTitle";
import Select from "@/components/select";
import MaterialTextField from "@/components/materialTextField";
import UploadReceiptBox from "@/components/uploadReceiptBox";
import Button from "@/components/button";
import Datepicker from "@/components/datepicker";
import MessageModal from "@/components/messageModal";
import { increaseBalanceViaReceiptApi } from "@/services/charge";
import { getBankListApi } from "@/services/bank";
import { BankListType } from "@/services/bank/types";

interface IFormInput {
  bank?: string;
  from_iban?: string;
  amount: string;
  insert_date: string | null | Date;
  description?: string;
  receipt_image: File | undefined;
}

export default function ReceiptPage() {
  const [bankList, setBankList] = useState<BankListType[]>([]);
  const [loading, setLoading] = useState(false);
  const [successOpen, setSuccessOpen] = useState(false);
  const [referenceId, setReferenceId] = useState("");

  const router = useRouter();

  const {
    handleSubmit,
    setValue,
    reset,
    control,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      bank: "",
      from_iban: "",
      amount: "",
      insert_date: null,
      description: "",
      receipt_image: undefined,
    },
  });

  async function getBankList() {
    const res = await getBankListApi();
    if (res) {
      setBankList(res?.data);
    }
  }

  useEffect(() => {
    getBankList();
  }, []);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const formData = new FormData();
    data.bank && formData.append("bank", data.bank);
    data.from_iban && formData.append("from_iban", `IR${data.from_iban}`);
    formData.append("amount", data.amount);
    data.insert_date instanceof Date &&
      formData.append("insert_date", data.insert_date.toISOString());
    data.description && formData.append("description", data.description);
    data.receipt_image && formData.append("receipt_image", data.receipt_image);

    const res = await increaseBalanceViaReceiptApi(formData);

    if (res) {
      if (res?.data?.reference_id > 0) {
        setReferenceId(res?.data?.reference_id);
        setSuccessOpen(true);
      }
    }

    setLoading(false);

    reset();
  };

  return (
    <PanelBox className="md:max-w-form">
      <FormTitle title="ثبت و ارسال درخواست افزایش موجودی کیف پول" />

      <form
        className="flex flex-col w-full gap-4 2xl:px-20 2xl:py-4"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Controller
          name="bank"
          control={control}
          render={({ field: { onChange, value } }) => (
            <Select
              label="نام بانک"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              items={bankList}
            />
          )}
        />

        <Controller
          name="from_iban"
          control={control}
          rules={{
            required: "شماره شبا مبدا را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="شماره شبا مبدا *"
              startAdornments="IR"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.from_iban}
              helperText={errors?.from_iban?.message}
            />
          )}
        />

        {/* <MaterialTextField
          label="شماره شبا مقصد"
          startAdornments="IR"
          className="!col-span-1"
          onChange={() => {}}
        /> */}

        <Controller
          name="amount"
          control={control}
          rules={{
            required: "مبلغ را وارد نمایید.",
          }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="مبلغ *"
              startAdornments="ریال"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.amount}
              helperText={errors?.amount?.message}
            />
          )}
        />

        <Controller
          name="insert_date"
          control={control}
          render={({ field: { onChange, value } }) => (
            <Datepicker
              value={value}
              onChange={onChange}
              name="date"
              label="در تاریخ"
              containerClassNames="ml-1"
            />
          )}
        />

        <Controller
          name="description"
          control={control}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="توضیحات"
              className="!col-span-2"
              onChange={onChange}
              value={value}
              multiline
            />
          )}
        />

        <Controller
          name="receipt_image"
          control={control}
          rules={{
            required: "عکس فیش واریزی خودرا انتخاب نمایید",
          }}
          render={({ field: { onChange, value } }) => (
            <UploadReceiptBox
              onChange={onChange}
              value={value}
              onDeleteFileClick={() => setValue("receipt_image", undefined)}
              error={!!errors?.receipt_image}
              helperText={errors?.receipt_image?.message}
            />
          )}
        />

        <div className="grid grid-cols-2 gap-2 mt-4">
          <Button
            fullRounded={false}
            className="!py-2 !text-xs !col-span-1"
            type="submit"
            loading={loading}
          >
            ثبت و ارسال
          </Button>
          <Button
            variant="cancel"
            className="!py-2 !text-xs !col-span-1"
            onClick={() => router.push("/panel")}
          >
            انصراف
          </Button>
        </div>
      </form>

      <MessageModal
        open={successOpen}
        onClose={() => setSuccessOpen(false)}
        variant="success"
        title="ارسال درخواست افزایش موجودی کیف پول"
        message="پس از بررسی فیش بانکی توسط ادمین سیستم،
        در صورت تایید ادمین، شارژ موجودی کیف پول انجام خواهد شد."
        content={
          <div className="text-center mt-2">
            <span className="text-sm text-primary">
              کد پیگیری: {referenceId}
            </span>
          </div>
        }
        onSubmitClick={() => setSuccessOpen(false)}
      />
    </PanelBox>
  );
}
