"use client";

import { Controller } from "react-hook-form";
import MaterialTextField from "@/components/materialTextField";
import FormTitle from "@/components/formTitle";

export default function MaxAmount({
  control,
  errors,
}: {
  control: any;
  errors: any;
}) {
  return (
    <div>
      <FormTitle title="تعیین سقف انتقال وجه" />

      <div className="grid grid-cols-1 gap-4 xl:grid-cols-3">
        <Controller
          name="max_amount"
          control={control}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="سقف انتقال وجه داخلی"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.max_amount}
              helperText={errors?.max_amount?.message}
            />
          )}
        />

        <Controller
          name="max_amount_satna"
          control={control}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="سقف انتقال وجه ساتنا"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.max_amount_satna}
              helperText={errors?.max_amount_satna?.message}
            />
          )}
        />

        <Controller
          name="max_amount_paya"
          control={control}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="سقف انتقال وجه پایا"
              value={value}
              onChange={onChange}
              className="!bg-white !col-span-1"
              error={!!errors?.max_amount_paya}
              helperText={errors?.max_amount_paya?.message}
            />
          )}
        />
      </div>
    </div>
  );
}
