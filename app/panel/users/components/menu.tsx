"use client";

import { usePathname } from "next/navigation";
import Link from "next/link";
import Image from "next/image";
import SingleCheckoutIcon from "assets/icons/single-checkout.svg";
import GroupCheckoutIcon from "assets/icons/group-checkout.svg";

export default function UsersMenu() {
  const pathname = usePathname();

  return (
    <div className="flex items-center justify-between gap-4 overflow-auto">
      <Link
        href="/panel/users/create"
        className={`flex items-center justify-center bg-white rounded-lg w-1/2 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/users/create")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={SingleCheckoutIcon}
          alt="single-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/users/create")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          تعریف کاربر
        </span>
      </Link>

      <Link
        href="/panel/users/list"
        className={`flex items-center justify-center bg-white rounded-lg w-1/2 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/users/list")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={GroupCheckoutIcon}
          alt="group-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/users/list")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          لیست کاربران
        </span>
      </Link>
    </div>
  );
}
