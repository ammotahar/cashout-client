import { ChangeEventHandler, MouseEventHandler, ReactNode } from "react";
import { SelectChangeEvent } from "@mui/material";
import FilterContainer from "@/components/filterContainer";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";

export default function Filter({
  open,
  onClose,
  onAccountNameChange,
  accountName,
  onAccountNumberChange,
  accountNumber,
  onBankNameChange,
  bankName,
  onIbanChange,
  iban,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onAccountNameChange?: ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  >;
  accountName?: any;
  onAccountNumberChange?: ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  >;
  accountNumber?: any;
  onBankNameChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  bankName?: any;
  onIbanChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  iban?: any;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <FilterContainer
      open={open}
      onClose={onClose}
      onSubmitClick={onSubmitClick}
      onDeleteClick={onDeleteClick}
    >
      <div className="grid grid-cols-1 lg:grid-cols-5 gap-4">
        <MaterialTextField
          label="نام حساب"
          onChange={onAccountNameChange}
          value={accountName}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="شماره حساب"
          onChange={onAccountNumberChange}
          value={accountNumber}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="نام بانک"
          onChange={onBankNameChange}
          value={bankName}
          className="!col-span-1 !bg-white"
        />

        <MaterialTextField
          label="شماره شبا"
          startAdornments="IR"
          onChange={onIbanChange}
          value={iban}
          className="!col-span-1 !bg-white"
        />
      </div>
    </FilterContainer>
  );
}
