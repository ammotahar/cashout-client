import AccountMenu from "./components/menu";

export default function UsersLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div>
      <AccountMenu />

      <div className="mt-4 flex flex-col items-center">{children}</div>
    </div>
  );
}
