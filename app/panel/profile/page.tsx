"use client";

import { useState, useEffect, useCallback } from "react";
import { useRouter } from "next/navigation";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import { toast } from "react-toastify";
import PanelBox from "@/components/panelBox";
import FormTitle from "@/components/formTitle";
import MaterialTextField from "@/components/materialTextField";
import Datepicker from "@/components/datepicker";
import Button from "@/components/button";
import { getProfileApi, editProfileApi } from "@/services/users";

interface IFormInput {
  first_name: string;
  last_name: string;
  birthdate: string | null | Date;
  mobile: string;
  phone: string;
  job_position: string;
}

export default function ProfilePage() {
  const [loading, setLoading] = useState(false);
  const [successOpen, setSuccessOpen] = useState(false);
  const [nationalCode, setNationalCode] = useState("");
  const [createTime, setCreateTime] = useState<null | Date | string>(null);
  const [userId, setUserId] = useState<null | number>(null);

  const router = useRouter();

  const {
    handleSubmit,
    reset,
    setValue,
    control,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      first_name: "",
      last_name: "",
      birthdate: null,
      mobile: "",
      phone: "",
      job_position: "",
    },
  });

  const getProfile = useCallback(async () => {
    setLoading(true);

    const res = await getProfileApi();

    if (res) {
      setValue("first_name", res?.data?.first_name);
      setValue("last_name", res?.data?.last_name);
      setValue("birthdate", new Date(res?.data?.birthdate));
      setValue("mobile", res?.data?.mobile);
      setValue("phone", res?.data?.phone);
      setValue("job_position", res?.data?.job_position);
      // setValue("description", res?.data?.description);
      setNationalCode(res?.data?.national_code);
      setCreateTime(new Date(res?.data?.create_time));
      setUserId(res?.data?.id);
    }

    setLoading(false);
  }, [setValue]);

  useEffect(() => {
    getProfile();
  }, [getProfile]);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    if (userId) {
      setLoading(true);

      const res = await editProfileApi(userId, {
        ...(data.first_name && { first_name: data.first_name }),
        ...(data.last_name && { last_name: data.last_name }),
        ...(data.birthdate &&
          typeof data.birthdate !== "string" && {
            birthdate: data.birthdate.toISOString().split("T")[0],
          }),
        ...(data.mobile && { mobile: data.mobile }),
        ...(data.phone && { phone: data.phone }),
        ...(data.job_position && { job_position: data.job_position }),
      });

      if (res) {
        toast.success("اطلاعات کاربری با موفقیت ویرایش شد.");
      }

      setLoading(false);
    }
  };

  return (
    <PanelBox className="md:max-w-form m-auto lg:mt-6">
      <FormTitle title="مشاهده مشخصات کاربر" />

      <form
        onSubmit={handleSubmit(onSubmit)}
        className="grid grid-cols-1 gap-4 xl:grid-cols-2"
      >
        <Controller
          name="first_name"
          control={control}
          // rules={{
          //   required: "نام را وارد نمایید.",
          // }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="نام"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.first_name}
              helperText={errors?.first_name?.message}
            />
          )}
        />

        <Controller
          name="last_name"
          control={control}
          // rules={{
          //   required: "نام خانوادگی را وارد نمایید.",
          // }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="نام خانوادگی"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.last_name}
              helperText={errors?.last_name?.message}
            />
          )}
        />

        <Controller
          name="birthdate"
          control={control}
          // rules={{
          //   required: "تاریخ تولد را وارد نمایید.",
          // }}
          render={({ field: { onChange, value } }) => (
            <Datepicker
              value={value}
              onChange={onChange}
              name="birthdate"
              label="تاریخ تولد"
              containerClassNames="!col-span-1"
              error={errors}
            />
          )}
        />

        <MaterialTextField
          label="کد ملی"
          className="!col-span-1"
          value={nationalCode}
          disabled
        />

        <Controller
          name="mobile"
          control={control}
          // rules={{
          //   required: "شماره موبایل را وارد نمایید.",
          // }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="شماره موبایل"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.mobile}
              helperText={errors?.mobile?.message}
            />
          )}
        />

        <Controller
          name="phone"
          control={control}
          // rules={{
          //   required: "شماره تماس را وارد نمایید.",
          // }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="شماره تماس"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.phone}
              helperText={errors?.phone?.message}
            />
          )}
        />

        <Datepicker
          value={createTime}
          name="create_time"
          label="تاریخ ثبت اطلاعات"
          containerClassNames="!col-span-1"
          disabled
        />

        <Controller
          name="job_position"
          control={control}
          // rules={{
          //   required: "سمت شغلی را وارد نمایید.",
          // }}
          render={({ field: { onChange, value } }) => (
            <MaterialTextField
              label="سمت شغلی"
              className="!col-span-1"
              onChange={onChange}
              value={value}
              error={!!errors?.job_position}
              helperText={errors?.job_position?.message}
            />
          )}
        />

        <div className="cols-span-1" />

        <div className="grid grid-cols-2 gap-2 mt-4 col-span-1">
          <Button
            fullRounded={false}
            className="!py-2 !text-xs !col-span-1"
            type="submit"
            loading={loading}
          >
            ثبت
          </Button>
          <Button
            variant="cancel"
            className="!py-2 !text-xs !col-span-1"
            onClick={() => router.push("/panel")}
          >
            انصراف
          </Button>
        </div>
      </form>
    </PanelBox>
  );
}
