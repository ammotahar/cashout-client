"use client";

import { useState } from "react";
import Button from "@/components/button";
import PeoplesListModal from "@/components/peoplesListModal";
import { FileIcon } from "@/assets/icons/file";
import { IbanListType } from "@/services/settlement/types";

export default function PeoplesList({
  onSelectClick,
}: {
  onSelectClick: (data: IbanListType) => void;
}) {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button
        variant="text"
        className="!bg-orange !flex !items-center !justify-center !h-10 !min-w-10 lg:!min-w-52 !rounded-md lg:!gap-4"
        onClick={() => setOpen(true)}
      >
        <span className="hidden lg:block text-white text-sm">
          مشاهده لیست افراد
        </span>
        <FileIcon className="lg:w-5 lg:h-5" />
      </Button>

      <PeoplesListModal
        open={open}
        onClose={() => setOpen(false)}
        onSelectClick={(data) => {
          onSelectClick(data);
          setOpen(false);
        }}
        onCancelClick={() => setOpen(false)}
      />
    </>
  );
}
