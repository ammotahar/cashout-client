"use client";

import { useState, useEffect, useCallback } from "react";
import { useSearchParams, useRouter } from "next/navigation";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import PanelBox from "@/components/panelBox";
import SearchField from "@/components/searchField";
import MaterialTextField from "@/components/materialTextField";
// import Radio from "@/components/radio";
import Select from "@/components/select";
import Button from "@/components/button";
import MessageModal from "@/components/messageModal";
import PeoplesList from "./components/peoplesList";
import { settlementDelayItems } from "@/utils/settlementDelayItems";
import { doSingleSettlementApi } from "../services";
import {
  addSingleSettlementToListApi,
  getSettlementDetailsApi,
} from "@/services/settlement";
import { getBankListApi } from "@/services/bank";
import { BankListType } from "@/services/bank/types";
import { getAccountsListApi } from "@/services/account";
import { AccountsListType } from "@/services/account/types";
import { SettlementDetailsType } from "@/services/settlement/types";
import {
  ENIAC_ACCOUNT,
  ENIAC_ACCOUNT_TITLE,
  COMPANY_ACCOUNT,
} from "@/enums/settlementFromEnums";

interface IFormInput {
  first_name: string;
  last_name: string;
  amount: string;
  iban: string;
  settlement_delay_time?: string;
  settlement_from: number | string;
  description?: string;
  // account_identity?: string;
  bank?: string | number;
  account?: string | number;
}

export default function CheckoutPage() {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [addToListLoading, setAddToListLoading] = useState(false);
  const [bankList, setBankList] = useState<BankListType[]>([]);
  const [accountList, setAccountList] = useState<AccountsListType[]>([]);
  const [message, setMessage] = useState("");
  const [referenceId, setReferenceId] = useState("");

  const router = useRouter();

  const searchParams = useSearchParams();
  const settlementId = searchParams.get("id");

  const {
    handleSubmit,
    setValue,
    reset,
    control,
    formState: { errors },
  } = useForm<IFormInput>({
    defaultValues: {
      first_name: "",
      last_name: "",
      amount: "",
      iban: "",
      bank: "",
      account: "",
      // account_identity: "",
      settlement_delay_time: "",
      settlement_from: "",
      description: "",
    },
  });

  const getSettlementDetails = useCallback(async () => {
    if (settlementId) {
      setLoading(true);
      setAddToListLoading(true);

      const res = await getSettlementDetailsApi(parseInt(settlementId));
      const data: SettlementDetailsType = res?.data;

      if (res) {
        setValue("first_name", data.first_name);
        setValue("last_name", data.last_name);
        setValue("iban", data.iban.split("IR")[1]);
        setValue("amount", data.amount.toString());
        setValue("bank", data.bank);
        setValue("account", data.account || 0);
        setValue(
          "settlement_delay_time",
          data.settlement_delay_time?.toString()
        );
        setValue("settlement_from", data.settlement_from);
        setValue("description", data.description);
      }

      setLoading(false);
      setAddToListLoading(false);
    }
  }, [setValue, settlementId]);

  useEffect(() => {
    getSettlementDetails();
  }, [getSettlementDetails]);

  async function getBankList() {
    const res = await getBankListApi();
    if (res) {
      setBankList(res?.data);
    }
  }

  async function getAccountsList() {
    const res = await getAccountsListApi();
    if (res) {
      setAccountList(res?.data?.data);
    }
  }

  useEffect(() => {
    getBankList();
    getAccountsList();
  }, []);

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    setLoading(true);

    const res = await doSingleSettlementApi({
      ...data,
      amount: parseInt(data.amount),
      // account_identity: data.account_identity
      //   ? parseInt(data.account_identity)
      //   : undefined,
      iban: `IR${data?.iban}`,
      bank:
        typeof data?.bank !== "string" && data?.bank ? data?.bank : undefined,
      account:
        typeof data?.account !== "string" && data?.account && data?.account > 0
          ? data?.account
          : undefined,
      settlement_from: data?.account === 0 ? ENIAC_ACCOUNT : COMPANY_ACCOUNT,
      settlement_delay_time: data.settlement_delay_time
        ? parseInt(data.settlement_delay_time)
        : null,
    });

    setLoading(false);

    if (res) {
      setMessage(res?.message);
      setReferenceId(res?.data?.reference_id);
      setOpen(true);
    }

    reset();
  };

  async function addSingleSettlementToList(data: IFormInput) {
    setAddToListLoading(true);

    const res = await addSingleSettlementToListApi({
      ...data,
      amount: parseInt(data.amount),
      // account_identity: data.account_identity
      //   ? parseInt(data.account_identity)
      //   : undefined,
      iban: `IR${data?.iban}`,
      bank:
        typeof data?.bank !== "string" && data?.bank ? data?.bank : undefined,
      account:
        typeof data?.account !== "string" && data?.account && data?.account > 0
          ? data?.account
          : undefined,
      settlement_from: data?.account === 0 ? ENIAC_ACCOUNT : COMPANY_ACCOUNT,
      settlement_delay_time: data.settlement_delay_time
        ? parseInt(data.settlement_delay_time)
        : null,
    });

    setAddToListLoading(false);

    if (res) {
      setMessage(res?.message);
      setReferenceId(res?.data?.reference_id);
      setOpen(true);
    }

    reset();
  }

  return (
    <PanelBox className="col-span-10 xl:col-span-7">
      <div className="mb-6 hidden lg:block">
        <span className="text-black">تسویه حساب تکی</span>
        <hr className="mt-6 border-table" />
      </div>

      <form className="w-full" onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col w-full gap-4 lg:grid lg:grid-cols-2">
          <div className="flex items-center justify-between gap-2 lg:col-span-2">
            {/* TODO: WTF is this searchfield for ?!!! */}
            <SearchField placeholder="بر اساس نام و نام خانوادگی / شرکت" />

            <PeoplesList
              onSelectClick={(selected) => {
                setValue("first_name", selected.first_name);
                setValue("last_name", selected.last_name);
                setValue("iban", selected.iban.split("IR")[1]);
              }}
            />
          </div>

          <Controller
            name="first_name"
            control={control}
            rules={{
              required: "نام را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="نام *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                error={!!errors?.first_name}
                helperText={errors?.first_name?.message}
              />
            )}
          />

          <Controller
            name="last_name"
            control={control}
            rules={{
              required: "نام خانوادگی را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="نام خانوادگی *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                error={!!errors?.first_name}
                helperText={errors?.first_name?.message}
              />
            )}
          />

          <Controller
            name="amount"
            control={control}
            rules={{
              required: "مبلغ را وارد نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="مبلغ *"
                className="!col-span-1"
                startAdornments="ریال"
                onChange={onChange}
                value={value}
                isFormatNumber
                error={!!errors?.amount}
                helperText={errors?.amount?.message}
              />
            )}
          />

          <Controller
            name="iban"
            control={control}
            rules={{
              required: "شماره شبا را وارد نمایید.",
              pattern: {
                value: /^(?=.{24}$)[0-9]*$/gi,
                message: "شماره شبا وارد شده صحیح نیست",
              },
            }}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="شماره شبا *"
                startAdornments="IR"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                error={!!errors?.iban}
                helperText={errors?.iban?.message}
              />
            )}
          />

          {/* <div className="bg-form rounded-lg p-2 col-span-2">
            <div className="flex items-center gap-2">
              <Radio label="شماره حساب" />
              <Radio label="شماره شبا" />
            </div>

            <MaterialTextField label="شماره حساب" />
          </div> */}

          <Controller
            name="bank"
            control={control}
            render={({ field: { onChange, value } }) => (
              <Select
                label="نام بانک"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={bankList}
              />
            )}
          />

          {/* <Controller
            name="account_identity"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="شناسه حساب"
                className="!col-span-1"
                onChange={onChange}
                value={value}
              />
            )}
          /> */}

          {/* <MaterialTextField label="در تاریخ" className="!col-span-1" /> */}

          <Controller
            name="settlement_delay_time"
            control={control}
            render={({ field: { onChange, value } }) => (
              <Select
                label="زمان تاخیر تا تسویه"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={settlementDelayItems}
              />
            )}
          />

          <Controller
            name="account"
            control={control}
            rules={{
              validate: (v) => v !== "" || "حساب مورد نظر را انتخاب نمایید.",
            }}
            render={({ field: { onChange, value } }) => (
              <Select
                label="تسویه از *"
                className="!col-span-1"
                onChange={onChange}
                value={value}
                items={[
                  { id: 0, name: ENIAC_ACCOUNT_TITLE },
                  ...accountList.map((item) => {
                    return { id: item.id, name: item.account_name };
                  }),
                ]}
                error={!!errors?.account}
                helperText={errors?.account?.message}
              />
            )}
          />

          <Controller
            name="description"
            control={control}
            render={({ field: { onChange, value } }) => (
              <MaterialTextField
                label="توضیحات"
                className="!col-span-2"
                onChange={onChange}
                value={value}
                multiline
              />
            )}
          />
        </div>

        <div className="grid grid-cols-3 gap-2 mt-8">
          <Button
            fullRounded={false}
            className="!py-2 !text-xs !col-span-1"
            loading={addToListLoading}
            onClick={handleSubmit(addSingleSettlementToList)}
          >
            افزودن به لیست
          </Button>
          <Button
            fullRounded={false}
            className="!py-2 !text-xs !col-span-1"
            type="submit"
            loading={loading}
          >
            تسویه حساب
          </Button>
          <Button
            variant="cancel"
            className="!py-2 !text-xs !col-span-1"
            onClick={() => router.push("/panel")}
          >
            انصراف
          </Button>
        </div>
      </form>

      <MessageModal
        open={open}
        onClose={() => setOpen(false)}
        variant="success"
        title="وضعیت درخواست تسویه حساب"
        message="جهت مشاهده جزئیات بیشتر به فرم لیست تسویه حساب مراجعه فرمائید."
        content={
          <div className="text-center mt-2">
            <span className="text-sm">
              درخواست شما به شماره پیگیری{" "}
              <span className="text-primary">{referenceId}</span> ثبت شد!
            </span>
          </div>
        }
        onSubmitClick={() => setOpen(false)}
        showCustomButton
        onCustomButtonClick={() => router.push("/panel/checkout/list")}
        customButtonTitle="لیست تسویه حساب ها"
      />
    </PanelBox>
  );
}
