"use client";

import { useState, useEffect } from "react";
import Image from "next/image";
import PanelBox from "@/components/panelBox";
import FilterTabs from "@/components/filterTabs";
import SearchField from "@/components/searchField";
import Button from "@/components/button";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import Filter from "./components/filter";
import MoreButton from "./components/moreButton";
import { getSettlementListApi, getSettlementListXlsxApi } from "../services";
import { rialSeparator } from "@/utils/rialSeperator";
import { SettlementListType } from "../services/type";
import FilterIcon from "assets/icons/filter.svg";
import ExportIcon from "assets/icons/export.svg";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import PendingIcon from "@/assets/icons/pending";
import GroupSettlementIcon from "assets/icons/group-settlement.svg";
import SingleSettlementIcon from "assets/icons/single-settlement.svg";
import {
  FAILED_SETTLEMENT,
  SUCCESSFULL_SETTLEMENT,
  DELAY_PENDING_SETTLEMENT,
  QUEUE_PENDING_SETTLEMENT,
  USER_PENDING_SETTLEMENT,
  FAILED_SETTLEMENT_TITLE,
  SUCCESSFULL_SETTLEMENT_TITLE,
  DELAY_PENDING_SETTLEMENT_TITLE,
  QUEUE_PENDING_SETTLEMENT_TITLE,
  USER_PENDING_SETTLEMENT_TITLE,
} from "@/enums/settlementStatusEnums";
import {
  GROUP_SETTLEMENT,
  SINGLE_SETTLEMENT,
} from "@/enums/settlementTypeEnums";

export default function CheckoutListPage() {
  const [value, setValue] = useState(0);
  const [count, setCount] = useState<number>(0);
  const [page, setPage] = useState<number>(1);
  const [data, setData] = useState<SettlementListType[] | null>(null);
  const [loading, setLoading] = useState(false);
  const [filterOpen, setFilterOpen] = useState(false);

  const [trackNumber, setTrackNumber] = useState("");
  const [iban, setIban] = useState("");
  const [settlementStatus, setSettlementStatus] = useState<number | string>("");
  const [settlementType, setSettlementType] = useState("");

  const [search, setSearch] = useState("");

  const [excelDownloadLoading, setExcelDownloadLoading] = useState(false);

  function deleteFilters() {
    setPage(1);
    setTrackNumber("");
    setIban("");
    setSettlementStatus("");
    setSettlementType("");
    getSettlementList(1, "", "", "", "", search);
  }

  async function getSettlementList(
    page_number?: number,
    trackNumber?: string,
    iban?: string,
    settlementStatus?: number | string,
    settlementType?: number | string,
    search?: string
  ) {
    setLoading(true);

    const res = await getSettlementListApi({
      per_page: 10,
      page_number,
      track_number: trackNumber,
      iban: iban && iban,
      settlement_status:
        typeof settlementStatus === "string" ? undefined : settlementStatus,
      settlement_type:
        typeof settlementType === "string" ? undefined : settlementType,
      search,
    });

    if (res) {
      setCount(res?.data?.count);
      setData(res?.data?.data);
    }

    setLoading(false);
  }

  async function getSettlementListXlsx(
    trackNumber?: string,
    iban?: string,
    settlementStatus?: number | string,
    file?: string,
    search?: string
  ) {
    setExcelDownloadLoading(true);

    const res = await getSettlementListXlsxApi({
      track_number: trackNumber,
      iban: iban && iban,
      settlement_status:
        typeof settlementStatus === "string" ? undefined : settlementStatus,
      cut_batch_settlement: file,
      search,
    });
    const blob = new Blob([res], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = "settlement_list.xlsx";
    a.click();
    setExcelDownloadLoading(false);
  }

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  function pageHandler(event: any, page: number) {
    setPage(page);
    getSettlementList(
      page,
      trackNumber,
      iban,
      settlementStatus,
      settlementType,
      search
    );
  }

  function getSettlementStatusTitle(status: number) {
    switch (status) {
      case SUCCESSFULL_SETTLEMENT:
        return (
          <div className="flex items-center text-success gap-2">
            <CheckCircleOutlineIcon />
            <span>{SUCCESSFULL_SETTLEMENT_TITLE}</span>
          </div>
        );

      case FAILED_SETTLEMENT:
        return (
          <div className="flex items-center text-error gap-2">
            <HighlightOffIcon />
            <span>{FAILED_SETTLEMENT_TITLE}</span>
          </div>
        );

      case DELAY_PENDING_SETTLEMENT:
        return (
          <div className="flex items-center text-yellow gap-2">
            <PendingIcon />
            <span>{DELAY_PENDING_SETTLEMENT_TITLE}</span>
          </div>
        );

      case QUEUE_PENDING_SETTLEMENT:
        return (
          <div className="flex items-center text-yellow gap-2">
            <PendingIcon />
            <span>{QUEUE_PENDING_SETTLEMENT_TITLE}</span>
          </div>
        );

      case USER_PENDING_SETTLEMENT:
        return (
          <div className="flex items-center text-yellow gap-2">
            <PendingIcon />
            <span>{USER_PENDING_SETTLEMENT_TITLE}</span>
          </div>
        );

      default:
        break;
    }
  }

  function getSettlementTypeTitle(type: number) {
    switch (type) {
      case GROUP_SETTLEMENT:
        return (
          <div className="flex items-center text-black gap-2">
            <Image src={GroupSettlementIcon} alt="group" />
            <span>گروهی</span>
          </div>
        );

      case SINGLE_SETTLEMENT:
        return (
          <div className="flex items-center text-black gap-2">
            <Image src={SingleSettlementIcon} alt="single" />
            <span>تکی</span>
          </div>
        );

      default:
        break;
    }
  }

  useEffect(() => {
    getSettlementList(1, "", "", "", "", "");
  }, []);

  // useEffect(() => {
  //   setPage(1);
  //   getSettlementList(
  //     1,
  //     trackNumber,
  //     iban,
  //     settlementStatus,
  //     settlementType,
  //     search
  //   );
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [search]);

  return (
    <PanelBox className="col-span-10 xl:col-span-7">
      {/* <FilterTabs
        value={value}
        onChange={handleChange}
        tabs={["همه", "تسویه شده", "تسویه نشده", "در انتظار تسویه"]}
      /> */}

      <div className="lg:flex lg:items-center lg:justify-between">
        <div className="my-6 flex items-center gap-4 w-full lg:w-1/2">
          {/* <SearchField
            placeholder="بر اساس نام و نام خانوادگی"
            onChange={(value) => setSearch(value)}
          /> */}

          <Button
            className="!flex !items-center !justify-center lg:!justify-between lg:!px-2 !bg-primary !rounded-md !min-w-10 !min-h-10 lg:!w-32"
            variant="text"
            onClick={() => setFilterOpen(!filterOpen)}
          >
            <div className="flex items-center gap-1">
              <Image src={FilterIcon} alt="filter" />
              <span className="hidden lg:block text-xs text-black">فیلتر</span>
            </div>

            <div className="hidden lg:block">
              {filterOpen ? (
                <ExpandLess className="!text-xl !text-black" />
              ) : (
                <ExpandMore className="!text-xl !text-black" />
              )}
            </div>
          </Button>
        </div>

        <Button
          fullRounded={false}
          className="!flex !items-center !justify-center !gap-2"
          padding="!px-2 !py-3"
          onClick={() =>
            getSettlementListXlsx(
              trackNumber,
              iban,
              settlementStatus,
              settlementType,
              search
            )
          }
          loading={excelDownloadLoading}
        >
          <span className="text-xs">خروجی اکسل</span>
          <Image src={ExportIcon} alt="export" />
        </Button>
      </div>

      <Filter
        open={filterOpen}
        onClose={() => setFilterOpen(false)}
        trackNumber={trackNumber}
        onTrackNumberChange={(event) => setTrackNumber(event.target.value)}
        iban={iban}
        onIbanChange={(event) => setIban(event.target.value)}
        settlementStatus={settlementStatus}
        onSettlementStatusChange={(event) =>
          setSettlementStatus(event.target.value)
        }
        settlementType={settlementType}
        onSettlementTypeChange={(event) =>
          setSettlementType(event.target.value)
        }
        onSubmitClick={() => {
          setPage(1);
          getSettlementList(
            1,
            trackNumber,
            iban,
            settlementStatus,
            settlementType,
            search
          );
        }}
        onDeleteClick={deleteFilters}
      />

      <div className="mt-4">
        <Table
          heads={[
            "نوع تسویه",
            "نام فایل",
            "نام و نام خانوادگی",
            "کد پیگیری",
            "شماره شبا",
            "مبلغ کارمزد",
            "مبلغ کارمزد بانکی",
            "وضعیت",
            "عملیات",
          ]}
          data={data?.map((item) => ({
            settlement_type: getSettlementTypeTitle(item.settlement_type),
            batch_file_name: item.batch_file_name,
            name: `${item.first_name} ${item.last_name}`,
            track_number: item.track_number,
            iban: item.iban,
            wageAmount:
              item.wage_amount && rialSeparator(item.wage_amount.toString()),
            bankWageAmount:
              item.bank_wage_amount &&
              rialSeparator(item.bank_wage_amount.toString()),
            settlement_status: getSettlementStatusTitle(item.settlement_status),
            operation: (
              <MoreButton
                settlementId={item?.id}
                settlementStatus={item.settlement_status}
                onDoSettlementFinished={() =>
                  getSettlementList(
                    page,
                    trackNumber,
                    iban,
                    settlementStatus,
                    settlementType,
                    search
                  )
                }
              />
            ),
          }))}
          loading={loading}
        />
      </div>

      <div className="mt-4 flex justify-end">
        <Pagination
          page={page}
          count={Math.ceil(count / 10)}
          onChange={pageHandler}
        />
      </div>
    </PanelBox>
  );
}
