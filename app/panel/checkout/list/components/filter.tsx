import { ChangeEventHandler, MouseEventHandler, ReactNode } from "react";
import { SelectChangeEvent } from "@mui/material";
import FilterContainer from "@/components/filterContainer";
import MaterialTextField from "@/components/materialTextField";
import Select from "@/components/select";
import IBANInput from "./IBANInput";
import {
  FAILED_SETTLEMENT,
  SUCCESSFULL_SETTLEMENT,
  DELAY_PENDING_SETTLEMENT,
  QUEUE_PENDING_SETTLEMENT,
  USER_PENDING_SETTLEMENT,
  FAILED_SETTLEMENT_TITLE,
  SUCCESSFULL_SETTLEMENT_TITLE,
  DELAY_PENDING_SETTLEMENT_TITLE,
  QUEUE_PENDING_SETTLEMENT_TITLE,
  USER_PENDING_SETTLEMENT_TITLE,
} from "@/enums/settlementStatusEnums";
import {
  GROUP_SETTLEMENT,
  GROUP_SETTLEMENT_TITLE,
  SINGLE_SETTLEMENT,
  SINGLE_SETTLEMENT_TITLE,
} from "@/enums/settlementTypeEnums";

export default function Filter({
  open,
  onClose,
  onTrackNumberChange,
  trackNumber,
  onIbanChange,
  iban,
  onSettlementStatusChange,
  settlementStatus,
  onSettlementTypeChange,
  settlementType,
  onSubmitClick,
  onDeleteClick,
}: {
  open: boolean;
  onClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void;
  onTrackNumberChange?: ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  >;
  trackNumber?: any;
  onIbanChange?: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  iban?: any;
  onSettlementStatusChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  settlementStatus?: any;
  onSettlementTypeChange?: (
    event: SelectChangeEvent<any>,
    child: ReactNode
  ) => void;
  settlementType?: any;
  onSubmitClick?: MouseEventHandler<HTMLButtonElement>;
  onDeleteClick?: MouseEventHandler<HTMLButtonElement>;
}) {
  return (
    <FilterContainer
      open={open}
      onClose={onClose}
      onSubmitClick={onSubmitClick}
      onDeleteClick={onDeleteClick}
    >
      <div className="flex flex-col lg:flex-row gap-4">
        <MaterialTextField
          label="کد پیگیری"
          onChange={onTrackNumberChange}
          value={trackNumber}
        />

        <IBANInput label="شماره شبا" onChange={onIbanChange} value={iban} />

        <Select
          label="وضعیت"
          onChange={onSettlementStatusChange}
          value={settlementStatus}
          items={[
            {
              id: SUCCESSFULL_SETTLEMENT,
              name: SUCCESSFULL_SETTLEMENT_TITLE,
            },
            {
              id: FAILED_SETTLEMENT,
              name: FAILED_SETTLEMENT_TITLE,
            },
            {
              id: DELAY_PENDING_SETTLEMENT,
              name: DELAY_PENDING_SETTLEMENT_TITLE,
            },
            {
              id: QUEUE_PENDING_SETTLEMENT,
              name: QUEUE_PENDING_SETTLEMENT_TITLE,
            },
            {
              id: USER_PENDING_SETTLEMENT,
              name: USER_PENDING_SETTLEMENT_TITLE,
            },
          ]}
        />

        {/* TODO: Add file filter */}
        <Select
          label="نوع تسویه"
          onChange={onSettlementTypeChange}
          value={settlementType}
          items={[
            {
              id: SINGLE_SETTLEMENT,
              name: SINGLE_SETTLEMENT_TITLE,
            },
            {
              id: GROUP_SETTLEMENT,
              name: GROUP_SETTLEMENT_TITLE,
            },
          ]}
        />
      </div>
    </FilterContainer>
  );
}
