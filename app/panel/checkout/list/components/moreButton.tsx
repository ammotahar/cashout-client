"use client";

import { useState, useCallback } from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@/components/button";
import SettlementDetailsModal from "@/components/settlementDetailsModal";
import UpdateSettlementModal from "@/components/updateSettlementModal";
import MessageModal from "@/components/messageModal";
import {
  doSettlementByIdApi,
  deleteSettlementApi,
} from "@/services/settlement";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import {
  SUCCESSFULL_SETTLEMENT,
  FAILED_SETTLEMENT,
  DELAY_PENDING_SETTLEMENT,
  QUEUE_PENDING_SETTLEMENT,
  USER_PENDING_SETTLEMENT,
} from "@/enums/settlementStatusEnums";

export default function MoreButton({
  settlementId,
  settlementStatus,
  onDoSettlementFinished,
}: {
  settlementId: number;
  settlementStatus: number;
  onDoSettlementFinished?: () => void;
}) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [settlementDetailsModalOpen, setSettlementDetailsModalOpen] =
    useState(false);
  const [isRetrySettlement, setIsRetrySettlement] = useState(false);
  const [doSettlementLoading, setDoSettlementLoading] = useState(false);
  const [updateSettlementModalOpen, setUpdateSettlementModalOpen] =
    useState(false);
  const [deleteSettlementModalOpen, setDeleteSettlementModalOpen] =
    useState(false);
  const [deleteSettlementLoading, setDeleteSettlementLoading] = useState(false);

  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onDoSettlementClick = useCallback(async () => {
    setDoSettlementLoading(true);

    const res = await doSettlementByIdApi({ settlement_id: settlementId });

    if (res) {
      onDoSettlementFinished && onDoSettlementFinished();
    }

    setDoSettlementLoading(false);
  }, [onDoSettlementFinished, settlementId]);

  async function deleteSettlement() {
    setDeleteSettlementLoading(true);
    const res = await deleteSettlementApi(settlementId);
    setDeleteSettlementModalOpen(false);
    setDeleteSettlementLoading(false);
    onDoSettlementFinished && onDoSettlementFinished();
  }

  const renderOperationsButtons = useCallback(() => {
    switch (settlementStatus) {
      case SUCCESSFULL_SETTLEMENT:
        return (
          <MenuItem
            onClick={() => {
              setSettlementDetailsModalOpen(true);
              handleClose();
            }}
            className="!text-primary"
          >
            مشاهده جزئیات
          </MenuItem>
        );

      case FAILED_SETTLEMENT:
        return [
          <MenuItem
            key={4}
            onClick={() => {
              setIsRetrySettlement(true);
              setSettlementDetailsModalOpen(true);
              handleClose();
            }}
            className="!text-success"
          >
            درخواست مجدد
          </MenuItem>,
          <MenuItem
            key={5}
            onClick={() => {
              setIsRetrySettlement(false);
              setSettlementDetailsModalOpen(true);
              handleClose();
            }}
            className="!text-primary"
          >
            مشاهده جزئیات
          </MenuItem>,
        ];

      case DELAY_PENDING_SETTLEMENT:
        return (
          <MenuItem
            onClick={() => {
              setSettlementDetailsModalOpen(true);
              handleClose();
            }}
            className="!text-primary"
          >
            مشاهده جزئیات
          </MenuItem>
        );

      case QUEUE_PENDING_SETTLEMENT:
        return (
          <MenuItem
            onClick={() => {
              setSettlementDetailsModalOpen(true);
              handleClose();
            }}
            className="!text-primary"
          >
            مشاهده جزئیات
          </MenuItem>
        );

      case USER_PENDING_SETTLEMENT:
        return [
          <MenuItem
            key={0}
            onClick={onDoSettlementClick}
            className="!text-success"
          >
            {doSettlementLoading ? (
              <CircularProgress size={20} />
            ) : (
              "تسویه حساب"
            )}
          </MenuItem>,
          <MenuItem
            key={1}
            onClick={() => {
              setUpdateSettlementModalOpen(true);
              handleClose();
            }}
            className="!text-yellow"
          >
            ویرایش درخواست
          </MenuItem>,
          <MenuItem
            key={2}
            onClick={() => {
              setDeleteSettlementModalOpen(true);
              handleClose();
            }}
            className="!text-error"
          >
            حذف درخواست
          </MenuItem>,
          <MenuItem
            key={3}
            onClick={() => {
              setSettlementDetailsModalOpen(true);
              handleClose();
            }}
            className="!text-primary"
          >
            مشاهده جزئیات
          </MenuItem>,
        ];

      default:
        break;
    }
  }, [doSettlementLoading, onDoSettlementClick, settlementStatus]);

  return (
    <>
      <Button variant="icon" onClick={handleClick}>
        <MoreHorizIcon />
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        {renderOperationsButtons()}
      </Menu>

      <SettlementDetailsModal
        open={settlementDetailsModalOpen}
        settlementId={settlementId}
        isRetrySettlement={isRetrySettlement}
        onCancelClick={() => setSettlementDetailsModalOpen(false)}
        onClose={() => setSettlementDetailsModalOpen(false)}
      />

      <UpdateSettlementModal
        open={updateSettlementModalOpen}
        settlementId={settlementId}
        onFinishUpdateSettlement={() => {
          setUpdateSettlementModalOpen(false);
          onDoSettlementFinished && onDoSettlementFinished();
        }}
        onCancelClick={() => setUpdateSettlementModalOpen(false)}
        onClose={() => setUpdateSettlementModalOpen(false)}
      />

      <MessageModal
        open={deleteSettlementModalOpen}
        onClose={() => setDeleteSettlementModalOpen(false)}
        variant="error"
        title="حذف درخواست"
        message="آیا از حذف درخواست مورد نظر اطمینان دارید ؟"
        onSubmitClick={deleteSettlement}
        showCancelButton
        onCancelClick={() => setDeleteSettlementModalOpen(false)}
        loading={deleteSettlementLoading}
      />
    </>
  );
}
