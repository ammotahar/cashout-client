// @ts-nocheck
import MaterialTextField from "@/components/materialTextField";
import { TextFieldProps } from "@mui/material";
import React, { ReactNode } from "react";
import InputMask, { Props } from "react-input-mask";
const MaskedInput = (props: any) => {
  return (
    <InputMask
      mask="IR999999999999999999999999"
      value={props.value}
      maskChar=""
      onChange={props.onChange}
      label={props.label}
      placeholder={props.placeholder}
      helperText={props.helperText}
      error={props.error}
    >
      {(inputProps: Props & TextFieldProps): ReactNode => {
        return (
          <MaterialTextField
            className={`${inputProps.className} !w-80`}
            {...inputProps}
          />
        );
      }}
    </InputMask>
  );
};
export default MaskedInput;
