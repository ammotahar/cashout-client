"use client";

import { useRef, useState } from "react";
import Image from "next/image";
import FileNameModal from "@/components/fileNameModal";
import { uploadExcelBulkSettlementApi } from "@/services/settlement";
import UploadDocImage from "assets/images/upload-doc.png";
import { ErrorRecordsType } from "@/services/settlement/types";

export default function UploadExcel({
  onUploadComplete,
}: {
  onUploadComplete?: (
    fileId: number,
    hasError: boolean,
    errorCount: number,
    errorRecords?: ErrorRecordsType[],
    errorRecordsIndex?: number[]
  ) => void;
}) {
  const [dragActive, setDragActive] = useState(false);
  const [fileNameOpen, setFileNameOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState<File | undefined>();

  const inputRef = useRef<HTMLInputElement>(null);

  const handleDrag = function (e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.type === "dragenter" || e.type === "dragover") {
      setDragActive(true);
    } else if (e.type === "dragleave") {
      setDragActive(false);
    }
  };

  const handleDrop = function (e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      setSelectedFile(e.dataTransfer.files[0]);
      setFileNameOpen(true);
    }
  };

  const handleValue = (value: any) => {
    setSelectedFile(value.target.files[0]);
    setFileNameOpen(true);
  };

  async function uploadExcelBulkSettlement(fileName: string) {
    setLoading(true);

    const data = new FormData();
    selectedFile && data.append("file", selectedFile);
    data.append("file_name", fileName);

    const res = await uploadExcelBulkSettlementApi(data);

    if (res) {
      if (res?.data?.file_id > 0) {
        setFileNameOpen(false);
        onUploadComplete &&
          onUploadComplete(
            res?.data?.file_id,
            res?.data?.error_count > 0,
            res?.data?.error_count,
            res?.data?.error_count > 0 && res?.data?.error_records,
            undefined
          );
      } else if (res?.data?.error_count > 0 && res?.data?.error_records_index) {
        setFileNameOpen(false);
        onUploadComplete &&
          onUploadComplete(
            res?.data?.file_id,
            res?.data?.error_count > 0,
            res?.data?.error_count,
            undefined,
            res?.data?.error_records_index
          );
      }
    }

    setLoading(false);
  }

  return (
    <div
      className="flex flex-col items-center bg-form border border-gray rounded pb-4 lg:mx-36"
      onDragEnter={handleDrag}
      onDragLeave={handleDrag}
      onDragOver={handleDrag}
      onDrop={handleDrop}
    >
      <div className="my-4">
        <span className="text-black">آپلود فایل اکسل نمونه</span>
      </div>

      <input
        className="hidden"
        id="contained-button-file"
        name="file"
        accept="*"
        type="file"
        ref={inputRef}
        onChange={handleValue}
      />

      <label
        htmlFor="contained-button-file"
        className="w-11/12 lg:w-8/12 cursor-pointer"
      >
        <div
          className="flex flex-col items-center bg-white rounded border border-dashed border-gray w-full"
          // onClick={() => {
          //   inputRef.current?.click();
          // }}
        >
          <div className="my-4">
            <Image
              src={UploadDocImage}
              alt="upload-doc"
              className="w-20 h-20"
            />
          </div>

          <div className="text-center">
            {dragActive ? (
              <span className="text-black">رها کنید.</span>
            ) : (
              <span className="text-black">
                فایل خود را بکشید و در{" "}
                <span className="text-primary">اینجا</span> رها کنید.
              </span>
            )}
          </div>

          <div className="mt-6 flex items-center justify-between gap-4 w-11/12 mb-2">
            <div>
              <span className="text-xs text-subtitle">
                حداکثر حجم فایل برای آپلود: 17 مگابایت
              </span>
            </div>

            <div>
              <span className="text-xs text-subtitle">
                فرمت مجاز: XLS، XLSC
              </span>
            </div>
          </div>
        </div>
      </label>

      <FileNameModal
        open={fileNameOpen}
        onSubmit={(fileName) => uploadExcelBulkSettlement(fileName)}
        onCancelClick={() => setFileNameOpen(false)}
        onClose={() => setFileNameOpen(false)}
        loading={loading}
      />
    </div>
  );
}
