"use client";

import Image from "next/image";
import { downloadSampleExcelBulkSettlementApi } from "@/services/batchSettlement";
import DownloadDocImage from "assets/images/download-doc.png";

export default function DownloadExcel() {
  async function downloadSampleExcelBulkSettlement() {
    const res = await downloadSampleExcelBulkSettlementApi();

    if (res) {
      window.open(res?.data?.file_path);
    }
  }
  return (
    <div className="flex flex-col items-center bg-form border border-gray rounded pb-4 lg:mx-36">
      <div className="my-4">
        <span className="text-black">دانلود فایل اکسل نمونه</span>
      </div>

      <div className="flex flex-col items-center bg-white rounded border border-dashed border-gray w-11/12 lg:w-8/12">
        <div className="my-4">
          <Image src={DownloadDocImage} alt="download-doc" />
        </div>

        <div>
          <span className="text-black">
            فایل خود را از{" "}
            <span
              className="text-primary cursor-pointer"
              onClick={downloadSampleExcelBulkSettlement}
            >
              اینجا
            </span>{" "}
            دانلود کنید.
          </span>
        </div>

        <div className="mt-6 flex items-center justify-between w-11/12 mb-2">
          <div>
            <span className="text-xs text-subtitle">حجم فایل: 17 مگابایت</span>
          </div>

          <div>
            <span className="text-xs text-subtitle">فرمت فایل: XLS، XLSC</span>
          </div>
        </div>
      </div>
    </div>
  );
}
