"use client";

import { useState } from "react";
import Button from "@/components/button";
import SelectExcelModal from "@/components/selectExcelModal";
import FileNameModal from "@/components/fileNameModal";
import { uploadExcelBulkSettlementApi } from "@/services/settlement";
import { FileIcon } from "@/assets/icons/file";

export default function SelectExcel({
  onSelectClick,
}: {
  onSelectClick?: (data: number) => void;
}) {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState(0);
  const [fileNameModalOpen, setFileNameModalOpen] = useState(false);

  async function uploadExcelBulkSettlement(fileName: string) {
    setLoading(true);

    const data = new FormData();
    selected && data.append("file_id", selected.toString());
    data.append("file_name", fileName);

    const res = await uploadExcelBulkSettlementApi(data);

    if (res) {
      if (res?.data?.file_id > 0) {
        onSelectClick && onSelectClick(res?.data?.file_id);
        setFileNameModalOpen(false);
      }
    }

    setLoading(false);
  }

  return (
    <>
      <Button
        variant="text"
        className="!bg-orange !flex !items-center !justify-center !rounded-md !w-32 !h-9 !gap-2 !self-end lg:!ml-36 lg:!mt-10"
        onClick={() => setOpen(true)}
      >
        <span className="text-white text-sm">انتخاب فایل</span>
        <FileIcon />
      </Button>

      <SelectExcelModal
        open={open}
        onClose={() => setOpen(false)}
        onSelectClick={() => {
          setOpen(false);
          setFileNameModalOpen(true);
        }}
        onCancelClick={() => setOpen(false)}
        selected={selected}
        setSelected={(fileId) => setSelected(fileId)}
      />

      <FileNameModal
        open={fileNameModalOpen}
        onSubmit={(fileName) => uploadExcelBulkSettlement(fileName)}
        onCancelClick={() => setFileNameModalOpen(false)}
        onClose={() => setFileNameModalOpen(false)}
        loading={loading}
      />
    </>
  );
}
