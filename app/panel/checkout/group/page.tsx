"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";
import PanelBox from "@/components/panelBox";
import Button from "@/components/button";
import FormTitle from "@/components/formTitle";
import SelectExcel from "./components/selectExcel";
import DownloadExcel from "./components/downloadExcel";
import ExcelReviewModal from "@/components/excelReviewModal";
import SuccessModal from "@/components/successModal";

export default function CheckoutGroupPage() {
  const [excelReviewOpen, setExcelReviewOpen] = useState(false);
  const [successOpen, setSuccessOpen] = useState(false);
  const [fileId, setFileId] = useState(0);
  const [fileName, setFileName] = useState("");

  const router = useRouter();

  return (
    <PanelBox className="col-span-10 xl:col-span-7 flex flex-col gap-2 relative">
      <FormTitle title="تسویه حساب گروهی" />

      <SelectExcel
        onSelectClick={(fileId) => {
          setFileId(fileId);
          setExcelReviewOpen(true);
        }}
      />

      <DownloadExcel />

      <ExcelReviewModal
        open={excelReviewOpen}
        fileId={fileId}
        onCancelClick={() => setExcelReviewOpen(false)}
        onClose={() => setExcelReviewOpen(false)}
        onInstantSettlement={(fileId, fileName) => {
          setExcelReviewOpen(false);
          setFileId(fileId);
          setFileName(fileName);
          setSuccessOpen(true);
        }}
        onAddSettlementToList={(fileId, fileName) => {
          setExcelReviewOpen(false);
          setFileId(fileId);
          setFileName(fileName);
          setSuccessOpen(true);
        }}
      />

      <SuccessModal
        open={successOpen}
        title="عملیات موفق"
        onClose={() => setSuccessOpen(false)}
        body={
          <div className="text-center flex flex-col items-center justify-center gap-4">
            <div className="text-sm">عملیات شما با موفقیت انجام شد!</div>
            <div className="text-sm">
              برای مشاهده جزئیات به فرم لیست تسویه حساب ها مراجعه کنید.
            </div>
            <div className="text-sm text-blue">شماره فایل: {fileId}</div>
            <div className="text-sm text-blue">نام فایل: {fileName}</div>
          </div>
        }
        buttons={
          <div className="grid grid-cols-2 gap-4 mt-4">
            <Button
              fullRounded={false}
              className="!col-span-1 lg:!w-36 lg:!h-10"
              padding="!p-0"
              onClick={() => router.push("/panel/checkout/list")}
            >
              مشاهده جزئیات
            </Button>
            <Button
              fullRounded={false}
              variant="success"
              className="!col-span-1 lg:!w-36 lg:!h-10"
              onClick={() => setSuccessOpen(false)}
            >
              تایید
            </Button>
          </div>
        }
      />

      <div className="grid grid-cols-2 gap-4 mt-4 lg:flex lg:justify-end xl:absolute xl:bottom-4 xl:left-4">
        <Button fullRounded={false} className="!col-span-1 lg:!w-36 lg:!h-10">
          <Link href="/panel/checkout/group/upload-excel">مرحله بعد</Link>
        </Button>
      </div>
    </PanelBox>
  );
}
