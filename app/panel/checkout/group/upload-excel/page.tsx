"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";
import PanelBox from "@/components/panelBox";
import FormTitle from "@/components/formTitle";
import Button from "@/components/button";
import UploadExcel from "../components/uploadExcel";
import ExcelReviewModal from "@/components/excelReviewModal";
import SuccessModal from "@/components/successModal";
import ExcelErrorsModal from "@/components/excelErrorsModal";
import MessageModal from "@/components/messageModal";
import ErrorRecordsIndexModal from "@/components/errorRecordsIndexModal";
import { ErrorRecordsType } from "@/services/settlement/types";

export default function CheckoutGroupPage() {
  const [excelReviewOpen, setExcelReviewOpen] = useState(false);
  const [successOpen, setSuccessOpen] = useState(false);
  const [fileId, setFileId] = useState(0);
  const [fileName, setFileName] = useState("");
  const [excelErrorsOpen, setExcelErrorsOpen] = useState(false);
  const [errorRecords, setErrorRecords] = useState<ErrorRecordsType[]>([]);
  const [excelHasErrorModal, setExcelHasErrorModal] = useState(false);
  const [curroptedFileModal, setCurroptedFileModal] = useState(false);
  const [errorRecordsIndex, setErrorRecordsIndex] = useState<number[]>([]);
  const [errorRecordsIndexModal, setErrorRecordsIndexModal] = useState(false);
  const [errorCount, setErrorCount] = useState(0);

  const router = useRouter();

  return (
    <PanelBox className="col-span-10 xl:col-span-7 flex flex-col gap-2 relative">
      <FormTitle title="تسویه حساب گروهی" />

      <UploadExcel
        onUploadComplete={(
          fileId,
          hasError,
          errorCount: number,
          errorRecords?: ErrorRecordsType[],
          errorRecordsIndex?: number[]
        ) => {
          setFileId(fileId);
          setErrorCount(errorCount);
          if (hasError && errorRecords) {
            setErrorRecords(errorRecords);
            setExcelErrorsOpen(true);
          } else if (hasError && errorRecordsIndex) {
            setErrorRecordsIndex(errorRecordsIndex);
            setCurroptedFileModal(true);
          } else {
            setExcelReviewOpen(true);
          }
        }}
      />

      <ExcelReviewModal
        open={excelReviewOpen}
        fileId={fileId}
        onCancelClick={() => setExcelReviewOpen(false)}
        onClose={() => setExcelReviewOpen(false)}
        onInstantSettlement={(fileId, fileName) => {
          setExcelReviewOpen(false);
          setFileId(fileId);
          setFileName(fileName);
          setSuccessOpen(true);
        }}
        onAddSettlementToList={(fileId, fileName) => {
          setExcelReviewOpen(false);
          setFileId(fileId);
          setFileName(fileName);
          setSuccessOpen(true);
        }}
      />

      <ExcelErrorsModal
        open={excelErrorsOpen}
        errorRecords={errorRecords}
        fileId={fileId}
        onSubmitClick={() => {
          setExcelErrorsOpen(false);
          setExcelHasErrorModal(true);
        }}
        onClose={() => setExcelErrorsOpen(false)}
        onDeleteFinished={() => setExcelErrorsOpen(false)}
      />

      <ErrorRecordsIndexModal
        open={errorRecordsIndexModal}
        onClose={() => setErrorRecordsIndexModal(false)}
        errorRecordsIndex={errorRecordsIndex}
        onCloseClick={() => setErrorRecordsIndexModal(false)}
      />

      <MessageModal
        open={excelHasErrorModal}
        onClose={() => {
          setExcelHasErrorModal(false);
          setExcelErrorsOpen(true);
        }}
        variant="success"
        title="ارسال درخواست تسویه حساب گروهی"
        message="ردیف‌های دارای مشکل از فایل حذف میشوند!"
        content={
          <div className="text-center mt-2">
            <span className="text-sm text-primary">
              آیا مایل به ادامه هستید؟
            </span>
          </div>
        }
        onSubmitClick={() => {
          setExcelHasErrorModal(false);
          setExcelReviewOpen(true);
        }}
        showCancelButton
        onCancelClick={() => {
          setExcelHasErrorModal(false);
          setExcelErrorsOpen(true);
        }}
      />

      <MessageModal
        open={curroptedFileModal}
        onClose={() => setCurroptedFileModal(false)}
        variant="error"
        title="درخواست تسویه حساب / ناموفق"
        message={`تعداد رکوردها‌ی خطا ${errorCount} عدد می‌باشد!`}
        content={
          <div className="text-center mt-2">
            <span className="text-sm text-primary">
              لطفا فایل را اصلاح کنید و مجددا آن را بارگذاری کنید.
            </span>
          </div>
        }
        onSubmitClick={() => setCurroptedFileModal(false)}
        showCustomButton
        customButtonTitle="نمایش ردیف های دارای خطا"
        onCustomButtonClick={() => {
          setCurroptedFileModal(false);
          setErrorRecordsIndexModal(true);
        }}
      />

      <SuccessModal
        open={successOpen}
        title="عملیات موفق"
        onClose={() => setSuccessOpen(false)}
        body={
          <div className="text-center flex flex-col items-center justify-center gap-4">
            <div className="text-sm">عملیات شما با موفقیت انجام شد!</div>
            <div className="text-sm">
              برای مشاهده جزئیات به فرم لیست تسویه حساب ها مراجعه کنید.
            </div>
            <div className="text-sm text-blue">شماره فایل: {fileId}</div>
            <div className="text-sm text-blue">نام فایل: {fileName}</div>
          </div>
        }
        buttons={
          <div className="grid grid-cols-2 gap-4 mt-4">
            <Button
              fullRounded={false}
              className="!col-span-1 lg:!w-36 lg:!h-10"
              padding="!p-0"
              onClick={() => router.push("/panel/checkout/list")}
            >
              مشاهده جزئیات
            </Button>
            <Button
              fullRounded={false}
              variant="success"
              className="!col-span-1 lg:!w-36 lg:!h-10"
              onClick={() => setSuccessOpen(false)}
            >
              تایید
            </Button>
          </div>
        }
      />

      <div className="grid grid-cols-2 gap-4 mt-4 lg:flex lg:justify-end xl:absolute xl:bottom-4 xl:left-4">
        {/* <Button
          fullRounded={false}
          className="!col-span-1 lg:!w-36 lg:!h-10"
          onClick={() => setExcelReviewOpen(true)}
        >
          مرحله بعد
        </Button> */}
        <Button
          fullRounded={false}
          variant="cancel"
          className="!col-span-1 lg:!w-36 lg:!h-10"
        >
          <Link href="/panel/checkout/group">انصراف</Link>
        </Button>
      </div>
    </PanelBox>
  );
}
