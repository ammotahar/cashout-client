import CheckoutsStatusBox from "@/components/checkoutsStatusBox";
import CheckoutMenu from "./components/menu";
import CheckoutsStatusButton from "./components/checkoutsStatusButton";
import PanelBox from "@/components/panelBox";

export default function CheckoutLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div>
      <CheckoutMenu />

      <CheckoutsStatusButton />

      <div className="mt-4 lg:grid lg:grid-cols-10 lg:gap-4">
        {children}
        <PanelBox className="hidden lg:block col-span-10 xl:col-span-3">
          <CheckoutsStatusBox />
        </PanelBox>
      </div>
    </div>
  );
}
