"use client";

import { usePathname, useRouter } from "next/navigation";
import { ChevronLeft } from "@mui/icons-material";
import Button from "@/components/button";

// href="/panel/checkout/status"

export default function CheckoutsStatusButton() {
  const pathname = usePathname();
  const router = useRouter();

  return (
    <div className="flex items-center justify-center w-full h-11 text-white gap-4 bg-black rounded-lg mt-4 lg:hidden">
      <Button
        variant="icon"
        onClick={() => {
          if (pathname === "/panel/checkout/status") {
            router.back();
          } else {
            router.push("/panel/checkout/status");
          }
        }}
      >
        <span className="text-white">
          {pathname === "/panel/checkout/status"
            ? "بازگشت به تسویه حساب"
            : "نمایش کلی وضعیت تسویه‌ها"}
        </span>
        <ChevronLeft className="!text-white" />
      </Button>
    </div>
  );
}
