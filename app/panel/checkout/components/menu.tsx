"use client";

import { usePathname } from "next/navigation";
import Link from "next/link";
import Image from "next/image";
import SingleCheckoutIcon from "assets/icons/single-checkout.svg";
import GroupCheckoutIcon from "assets/icons/group-checkout.svg";
import ListCheckoutIcon from "assets/icons/list.svg";

export default function CheckoutMenu() {
  const pathname = usePathname();

  return (
    <div className="flex items-center justify-between gap-4 overflow-auto">
      <Link
        href="/panel/checkout/single"
        className={`flex items-center justify-center bg-white rounded-lg w-1/3 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/checkout/single")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={SingleCheckoutIcon}
          alt="single-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/checkout/single")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          تسویه حساب تکی
        </span>
      </Link>

      <Link
        href="/panel/checkout/group"
        className={`flex items-center justify-center bg-white rounded-lg w-1/3 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/checkout/group")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image
          src={GroupCheckoutIcon}
          alt="group-checkout"
          className="w-4 h-4"
        />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/checkout/group")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          تسویه حساب گروهی
        </span>
      </Link>

      <Link
        href="/panel/checkout/list"
        className={`flex items-center justify-center bg-white rounded-lg w-1/3 min-w-48 h-9 gap-2 border ${
          pathname.startsWith("/panel/checkout/list")
            ? "border-primary"
            : "border-gray"
        }`}
      >
        <Image src={ListCheckoutIcon} alt="list-checkout" className="w-4 h-4" />
        <span
          className={`text-xs ${
            pathname.startsWith("/panel/checkout/list")
              ? "text-primary"
              : "text-gray"
          }`}
        >
          لیست وضعیت تسویه حساب‌ها
        </span>
      </Link>
    </div>
  );
}
