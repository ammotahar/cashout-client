import PanelBox from "@/components/panelBox";
import CheckoutsStatusBox from "@/components/checkoutsStatusBox";

export default function StatusPage() {
  return (
    <PanelBox className="col-span-5 xl:col-span-2">
      <CheckoutsStatusBox />
    </PanelBox>
  );
}
