export interface SettlementListType {
  id: number;
  first_name: string;
  last_name: string;
  company_name: string;
  settlement_from: number;
  batch_file_name: string;
  settlement_type: number;
  track_number: number;
  iban: string;
  settlement_status: number;
  bank_name: string | null;
  response: string;
  account: number | null;
  wage_amount: number;
  bank_wage_amount: number;
}
