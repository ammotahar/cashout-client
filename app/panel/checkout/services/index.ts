import Api from "services";

export const doSingleSettlementApi = (data: {
  iban: string;
  amount: number;
  first_name: string;
  last_name: string;
  bank?: number;
  account?: number;
  // account_identity?: number;
  settlement_delay_time?: number | null;
  settlement_from: number | string | null;
  description?: string;
}) => Api.POST({ url: "settlement/do_single_settlement", data });

export const getSettlementListApi = (query?: {
  per_page?: number;
  page_number?: number;
  track_number?: string;
  iban?: string;
  settlement_status?: number;
  cut_batch_settlement?: string;
  settlement_type?: number;
  search?: string;
}) => Api.GET({ url: "settlement", query });

export const getSettlementListXlsxApi = (query?: {
  track_number?: string;
  iban?: string;
  settlement_status?: number;
  cut_batch_settlement?: string;
  search?: string;
}) =>
  Api.GET({
    url: "settlement/get_settlement_list_xlsx",
    query,
    isBinary: true,
  });
