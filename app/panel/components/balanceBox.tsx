"use client";

import { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import Skeleton from "@mui/material/Skeleton";
import Tabs from "@/components/tabs";
import PanelBox from "@/components/panelBox";
import PriceText from "@/components/priceText";
import Select from "@/components/select";
import Button from "@/components/button";
import { getBalanceAndMinDepositApi } from "@/services/company";
import { getAccountsListApi, getAccountBalanceApi } from "@/services/account";
import { AccountsListType } from "@/services/account/types";
import ActiveCardImage from "assets/images/active-card.svg";
import DeactiveCardImage from "assets/images/deactive-card.svg";
import PlusIcon from "@/assets/icons/blue/plus";

export default function BalanceBox() {
  const [value, setValue] = useState(0);
  const [balance, setBalance] = useState(0);
  const [balanceLoading, setBalanceLoading] = useState(false);
  const [accountList, setAccountList] = useState<AccountsListType[]>([]);
  const [selectedAccount, setSelectedAccount] = useState<number | string>("");
  const [accountBalance, setAccountBalance] = useState<number | undefined>(
    undefined
  );
  const [getAccountBalanceLoading, setGetAccountBalanceLoading] =
    useState(false);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  async function getBalanceAndMinDeposit() {
    setBalanceLoading(true);
    const res = await getBalanceAndMinDepositApi();

    if (res) {
      setBalance(res?.data?.balance);
    }

    setBalanceLoading(false);
  }

  async function getAccountsList() {
    const res = await getAccountsListApi({
      is_active: true,
      is_deleted: false,
    });
    if (res) {
      setAccountList(res?.data?.data);
    }
  }

  useEffect(() => {
    getBalanceAndMinDeposit();
    getAccountsList();
  }, []);

  async function getAccountBalance(accountId: number) {
    setGetAccountBalanceLoading(true);

    const res = await getAccountBalanceApi({ account_id: accountId });

    if (res) {
      setAccountBalance(res?.data?.balance);
    } else {
      setAccountBalance(undefined);
    }

    setGetAccountBalanceLoading(false);
  }

  function renderTabs() {
    switch (value) {
      case 0:
        return (
          <>
            <Link
              className="flex items-center justify-center bg-gray rounded-lg py-3"
              href="/panel/charge/receipt"
            >
              <PlusIcon />
              <span className="text-xs mr-2">افزودن موجودی</span>
            </Link>

            <div className="w-full mt-4 relative flex items-center justify-center">
              <PriceText
                price={balance.toString()}
                containerClassName="absolute"
              />
              <Image
                src={ActiveCardImage}
                alt="balance-card"
                className="w-full"
              />
            </div>
          </>
        );

      case 1:
        return (
          <>
            <Link
              className="flex items-center justify-center bg-gray rounded-lg py-3"
              href="/panel/account/create"
            >
              <PlusIcon />
              <span className="text-xs mr-2">افزودن حساب</span>
            </Link>

            <div className="mt-4">
              <Select
                label="انتخاب حساب بانک"
                onChange={(event) => {
                  setSelectedAccount(event.target.value);
                  setAccountBalance(undefined);
                }}
                value={selectedAccount}
                items={accountList.map((item) => {
                  return {
                    id: item.id,
                    name: `${item.account_name} (${item.account_number})`,
                  };
                })}
              />
            </div>

            <div className="w-full mt-4 relative flex items-center justify-center">
              {typeof selectedAccount !== "string" && (
                <div className="absolute text-white text-sm flex flex-col gap-4 w-72">
                  <div className="flex items-center justify-between">
                    <div>
                      <span>شماره حساب :</span>
                    </div>

                    <div>
                      <span>
                        {
                          accountList.filter(
                            (item) => item.id === selectedAccount
                          )[0].account_number
                        }
                      </span>
                    </div>
                  </div>

                  <div className="flex items-center justify-between">
                    <div>
                      <span>شماره شبا :</span>
                    </div>

                    <div>
                      <span>
                        {
                          accountList.filter(
                            (item) => item.id === selectedAccount
                          )[0].iban
                        }
                      </span>
                    </div>
                  </div>

                  <div className="self-end">
                    {accountBalance ? (
                      <PriceText price={accountBalance.toString()} />
                    ) : (
                      <Button
                        onClick={() => getAccountBalance(selectedAccount)}
                        loading={getAccountBalanceLoading}
                        variant="secondary"
                        className="!rounded-full !text-white !border-white"
                      >
                        دریافت موجودی
                      </Button>
                    )}
                  </div>
                </div>
              )}

              <Image
                src={
                  typeof selectedAccount !== "string"
                    ? ActiveCardImage
                    : DeactiveCardImage
                }
                alt="balance-card"
                className="w-full"
              />
            </div>
          </>
        );

      default:
        break;
    }
  }

  return (
    <PanelBox>
      <div className="w-full h-full">
        <Tabs
          value={value}
          onChange={handleChange}
          tabs={["کیف پول", "حساب های بانکی"]}
        />

        <div className="w-full md:w-1/2 lg:w-4/5 xl:w-3/5 mx-auto mt-4">
          {balanceLoading ? (
            <Skeleton
              variant="rounded"
              className="!w-full !h-32 !rounded-2xl !mt-4"
            />
          ) : (
            renderTabs()
          )}
        </div>
      </div>
    </PanelBox>
  );
}
