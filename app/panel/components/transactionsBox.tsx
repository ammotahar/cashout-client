"use client";

import { useState, useEffect, useCallback } from "react";
import Skeleton from "@mui/material/Skeleton";
import { ExpandMore, ChevronLeft } from "@mui/icons-material";
import PanelBox from "@/components/panelBox";
import PieChart from "@/components/charts/pieChart";
import Table from "@/components/table";
import Pagination from "@/components/pagination";
import { getSettlementTransactionApi } from "@/services/report";
import { GetSettlementTransactionType } from "@/services/report/getSettlementTransactionTypes";
import {
  FAILED_SETTLEMENT,
  SUCCESSFULL_SETTLEMENT,
  DELAY_PENDING_SETTLEMENT,
  QUEUE_PENDING_SETTLEMENT,
  USER_PENDING_SETTLEMENT,
  FAILED_SETTLEMENT_TITLE,
  SUCCESSFULL_SETTLEMENT_TITLE,
  DELAY_PENDING_SETTLEMENT_TITLE,
  QUEUE_PENDING_SETTLEMENT_TITLE,
  USER_PENDING_SETTLEMENT_TITLE,
} from "@/enums/settlementStatusEnums";

export default function TransactionsBox() {
  const [data, setData] = useState<GetSettlementTransactionType | null>(null);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState<number>(170);
  const [page, setPage] = useState<number>(1);

  const getSettlementTransaction = useCallback(async () => {
    setLoading(true);

    const res = await getSettlementTransactionApi({
      per_page: 10,
      page_number: page,
    });

    if (res) {
      setData(res?.data);
      setCount(res?.data?.count);
    }

    setLoading(false);
  }, [page]);

  useEffect(() => {
    getSettlementTransaction();
  }, [getSettlementTransaction]);

  function pageHandler(event: any, page: number) {
    setPage(page);
  }

  function getSettlementStatusTitle(status: number) {
    switch (status) {
      case SUCCESSFULL_SETTLEMENT:
        return (
          <span className="text-xs text-success">
            {SUCCESSFULL_SETTLEMENT_TITLE}
          </span>
        );

      case FAILED_SETTLEMENT:
        return (
          <span className="text-xs text-error">{FAILED_SETTLEMENT_TITLE}</span>
        );

      case DELAY_PENDING_SETTLEMENT:
        return (
          <span className="text-xs text-secondary">
            {DELAY_PENDING_SETTLEMENT_TITLE}
          </span>
        );

      case QUEUE_PENDING_SETTLEMENT:
        return (
          <span className="text-xs text-secondary">
            {QUEUE_PENDING_SETTLEMENT_TITLE}
          </span>
        );

      case USER_PENDING_SETTLEMENT:
        return (
          <span className="text-xs text-secondary">
            {USER_PENDING_SETTLEMENT_TITLE}
          </span>
        );

      default:
        break;
    }
  }

  return (
    <PanelBox>
      {loading || !data ? (
        <Skeleton variant="rounded" className="!w-full !h-80 lg:!h-full" />
      ) : (
        <div className="w-full h-full">
          {/* <div className="flex items-center justify-between bg-gray rounded-2xl py-1 px-2 w-24">
            <span className="text-primary text-xs">هفته گذشته</span>
            <ExpandMore className="!text-primary !text-base" />
          </div> */}

          <div className="flex flex-col xl:flex-row-reverse">
            <div className="flex flex-col items-center w-full lg:w-2/5">
              <PieChart
                data={{
                  successful_transactions_percent: data
                    ? data?.successful_transactions_percent
                    : 0,
                  pending_transactions_percent: data
                    ? data?.pending_transactions_percent
                    : 0,
                  failed_transactions_percent: data
                    ? data?.failed_transactions_percent
                    : 0,
                  checking_queue_transactions_percent: data
                    ? data?.checking_queue_transactions_percent
                    : 0,
                  pending_user_transactions_percent: data
                    ? data?.pending_user_transactions_percent
                    : 0,
                }}
              />
            </div>

            <div className="grid grid-cols-10 text-xs mt-4 w-full xl:w-3/5">
              <div className="col-span-5 grid grid-rows-5 gap-4 text-black">
                <div className="row-span-1 flex items-center gap-2">
                  <div className="bg-success min-w-3 w-3 h-3 rounded" />
                  <span>تراکنش‌های موفق</span>
                </div>

                <div className="row-span-1 flex items-center gap-2">
                  <div className="bg-yellow min-w-3 w-3 h-3 rounded" />
                  <span>تراکنش‌های در انتظار تسویه</span>
                </div>

                <div className="row-span-1 flex items-center gap-2">
                  <div className="bg-error min-w-3 w-3 h-3 rounded" />
                  <span>تراکنش‌های تسویه نشده</span>
                </div>

                <div className="row-span-1 flex items-center gap-2">
                  <div className="bg-purple min-w-3 w-3 h-3 rounded" />
                  <span>تراکنش‌های در انتظار تایید کاربر</span>
                </div>

                <div className="row-span-1 flex items-center gap-2">
                  <div className="bg-blue min-w-3 w-3 h-3 rounded" />
                  <span>تراکنش‌های در صف</span>
                </div>
              </div>

              <div className="col-span-5 grid grid-rows-5 gap-4 xl:col-span-4">
                <div className="row-span-1 flex items-center justify-end">
                  <span className="text-success">{`${data?.successful_transactions_count} نفر`}</span>
                </div>

                <div className="row-span-1 flex items-center justify-end">
                  <span className="text-yellow">{`${data?.pending_transactions_count} نفر`}</span>
                </div>

                <div className="row-span-1 flex items-center justify-end">
                  <span className="text-error">{`${data?.failed_transactions_count} نفر`}</span>
                </div>

                <div className="row-span-1 flex items-center justify-end">
                  <span className="text-purple">{`${data?.pending_user_transactions_count} نفر`}</span>
                </div>

                <div className="row-span-1 flex items-center justify-end">
                  <span className="text-primary">{`${data?.checking_queue_transactions_count} نفر`}</span>
                </div>
              </div>

              {/* <div className="col-span-3 grid grid-rows-5 gap-4 text-primary">
                <div className="flex items-center justify-end row-span-1">
                  <span>نمایش لیست</span>
                  <ChevronLeft className="!text-base" />
                </div>

                <div className="flex items-center justify-end row-span-1">
                  <span>نمایش لیست</span>
                  <ChevronLeft className="!text-base" />
                </div>

                <div className="flex items-center justify-end row-span-1">
                  <span>نمایش لیست</span>
                  <ChevronLeft className="!text-base" />
                </div>

                <div className="flex items-center justify-end row-span-1">
                  <span>نمایش لیست</span>
                  <ChevronLeft className="!text-base" />
                </div>

                <div className="flex items-center justify-end row-span-1">
                  <span>نمایش لیست</span>
                  <ChevronLeft className="!text-base" />
                </div>
              </div> */}
            </div>
          </div>

          <div className="flex items-center justify-between mt-8">
            <div>
              <span className="text-black text-sm">
                لیست تراکنش‌های موفق در هفته گذشته
              </span>
            </div>

            <div className="hidden lg:block">
              <Pagination
                page={page}
                count={Math.ceil(count / 10)}
                onChange={pageHandler}
              />
            </div>
          </div>

          <div className="mt-4">
            <Table
              heads={["نام و نام خانوادگی", "کد پیگیری", "شماره شبا", "وضعیت"]}
              data={data?.data.map((item) => {
                return {
                  fullname: `${item?.first_name} ${item?.last_name}`,
                  track_number: item?.track_number,
                  iban: item?.iban,
                  settlement_status_title: getSettlementStatusTitle(
                    item?.settlement_status
                  ),
                };
              })}
            />
          </div>

          <div className="lg:hidden mt-4 flex justify-end">
            <Pagination
              page={page}
              count={Math.ceil(count / 10)}
              onChange={pageHandler}
            />
          </div>
        </div>
      )}
    </PanelBox>
  );
}
